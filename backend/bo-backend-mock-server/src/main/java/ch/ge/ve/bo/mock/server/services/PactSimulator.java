/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server.services;

import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialCreationStage.BUILD_PUBLIC_CREDENTIALS;
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialCreationStage.INITIALIZE_PARAMETERS;
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialCreationStage.SEND_VOTERS;
import static java.time.LocalDateTime.now;

import ch.ge.ve.bo.mock.server.MockServer;
import ch.ge.ve.bo.mock.server.database.DatabaseService;
import ch.ge.ve.bo.mock.server.pact.PactVotingMaterialStatusController;
import ch.ge.ve.bo.repository.entity.WorkflowStep;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.chvote.pactback.contract.operation.status.DeployedConfigurationStatusVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.InTestConfigurationStatusVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.OperationConfigurationStatusVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.ProtocolStageProgressVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.TallyArchiveStatusVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialCreationStage;
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo;
import com.google.common.collect.ImmutableMap;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

@Service
public class PactSimulator {
  private static final String                                    PACT_URL                 = "/pact";
  private static final String                                    VR_URL                   = "/vr";
  public static final  String                                    INVALIDATION_PAGE_URL    = PACT_URL + "/invalidate";
  private static final String                                    TEST_FILE_LOCATION;
  public static final  ImmutableMap<String, String>              TEST_FILE_LOCATIONS;
  public final         DatabaseService                           databaseService;
  private              Map<Long, OperationConfigurationStatusVo> configurationStatusById  = new HashMap<>();
  private              Map<Long, VotingMaterialsStatusVo>        votingMaterialStatusById = new HashMap<>();
  private              Map<Long, VotingPeriodStatusVo>           votingPeriodStatusById   = new HashMap<>();
  private              Map<Long, TallyArchiveStatusVo>           tallyArchiveStatusById   = new HashMap<>();

  static {
    try {
      Path tempFile = Files.createTempFile("test-file", ".zip");
      Files.write(tempFile, IOUtils.toByteArray(PactSimulator.class.getResourceAsStream("/test-file.zip")));
      TEST_FILE_LOCATION = tempFile.toString();
      TEST_FILE_LOCATIONS = ImmutableMap.of("Printer4", TEST_FILE_LOCATION, "boh-vprt", TEST_FILE_LOCATION);
    } catch (IOException e) {
      throw new TechnicalException(e);
    }
  }

  public PactSimulator(DatabaseService databaseService) {
    this.databaseService = databaseService;
  }


  public void markConfigurationAsUploaded(long operationId) {
    databaseService.setWorkflowStep(WorkflowStep.VOTING_MATERIAL_CONF_NOT_SENT, operationId);
    configurationStatusById.compute(operationId, (id, status) -> new OperationConfigurationStatusVo(
        new InTestConfigurationStatusVo(
            MockServer.MOCK_SERVER_USER,
            now(),
            InTestConfigurationStatusVo.State.TEST_SITE_IN_DEPLOYMENT,
            null,
            PACT_URL,
            TEST_FILE_LOCATION,
            new ArrayList<>(),
            VR_URL
        ),
        status != null ? status.getDeployed() : null));
  }


  public OperationConfigurationStatusVo getConfigurationStatus(long operationId) {
    return configurationStatusById.get(operationId);
  }

  public void setNewTestStatus(long operationId, InTestConfigurationStatusVo.State newTestStatus, String comment,
                               String username) {

    databaseService.setWorkflowStep(WorkflowStep.VOTING_MATERIAL_CONF_NOT_SENT, operationId);
    InTestConfigurationStatusVo inTest =
        new InTestConfigurationStatusVo(username, now(), newTestStatus, comment, PACT_URL,
                                        TEST_FILE_LOCATION, new ArrayList<>(), VR_URL);
    configurationStatusById.put(operationId, new OperationConfigurationStatusVo(inTest, null));
  }

  public void deployConfiguration(long operationId) {
    databaseService.setWorkflowStep(WorkflowStep.VOTING_MATERIAL_CONF_NOT_SENT, operationId);
    OperationConfigurationStatusVo status = new OperationConfigurationStatusVo(
        null,
        new DeployedConfigurationStatusVo(MockServer.MOCK_SERVER_USER, now(), VR_URL)
    );

    configurationStatusById.put(operationId, status);
  }

  public void resetConfiguration() {
    databaseService.setWorkflowStep(WorkflowStep.CONFIGURATION_NOT_SENT);
    configurationStatusById.clear();
  }

  public void markVotingMaterialAsUploaded(long operationId) {
    databaseService.setWorkflowStep(WorkflowStep.VOTING_PERIOD_CONF_NOT_SENT);
    setVotingMaterialStatus(operationId, VotingMaterialsStatusVo.State.AVAILABLE_FOR_CREATION);
  }

  public void resetVotingMaterial() {
    databaseService.setWorkflowStep(WorkflowStep.VOTING_MATERIAL_CONF_NOT_SENT);
    votingMaterialStatusById.clear();
  }

  public VotingMaterialsStatusVo getVotingMaterialStatus(long operationId) {
    return votingMaterialStatusById.get(operationId);
  }

  public void setVotingMaterialStatus(long operationId, VotingMaterialsStatusVo.State state) {
    setVotingMaterialStatus(operationId, state, null,
                            (state == VotingMaterialsStatusVo.State.VALIDATED ||
                             state == VotingMaterialsStatusVo.State.CREATED ||
                             state == VotingMaterialsStatusVo.State.INVALIDATION_REJECTED) ?
                                TEST_FILE_LOCATIONS : null);
  }

  public void setVotingMaterialStatus(long operationId, VotingMaterialsStatusVo.State state,
                                      String reason, Map<String, String> votingCardLocations) {
    databaseService.setWorkflowStep(WorkflowStep.VOTING_PERIOD_CONF_NOT_SENT);
    votingMaterialStatusById.put(operationId, new VotingMaterialsStatusVo(
        "MOCK-SERVER", now(), state, reason, PACT_URL, INVALIDATION_PAGE_URL, votingCardLocations, new ArrayList<>()));
  }

  public void setVotingMaterialStatusInProgress(long operationId, PactVotingMaterialStatusController.ProgressLevel
      progressLevel) {
    databaseService.setWorkflowStep(WorkflowStep.VOTING_PERIOD_CONF_NOT_SENT);
    List<ProtocolStageProgressVo<VotingMaterialCreationStage>> protocolProgress = new ArrayList<>();
    switch (progressLevel) {

      case ONE_STEP_DONE:
        protocolProgress.add(
            new ProtocolStageProgressVo<>(INITIALIZE_PARAMETERS, -1, 1d, now().minusMinutes(5), now().minusMinutes(3)));
        protocolProgress
            .add(new ProtocolStageProgressVo<>(SEND_VOTERS, 0, 0.5d, now().minusMinutes(2), now().minusSeconds(5)));
        protocolProgress
            .add(new ProtocolStageProgressVo<>(SEND_VOTERS, 1, 0.3d, now().minusMinutes(2), now().minusSeconds(5)));
        break;
      case TWO_STEPS_DONE:
        protocolProgress.add(
            new ProtocolStageProgressVo<>(INITIALIZE_PARAMETERS, -1, 1d, now().minusMinutes(7), now().minusMinutes(6)));
        protocolProgress
            .add(new ProtocolStageProgressVo<>(SEND_VOTERS, 0, 1d, now().minusMinutes(6), now().minusMinutes(5)));
        protocolProgress
            .add(new ProtocolStageProgressVo<>(SEND_VOTERS, 1, 1d, now().minusMinutes(6), now().minusMinutes(5)));
        protocolProgress.add(new ProtocolStageProgressVo<>(BUILD_PUBLIC_CREDENTIALS, 0, 0.9d, now().minusMinutes(5),
                                                           now().minusMinutes(1)));
        protocolProgress.add(new ProtocolStageProgressVo<>(BUILD_PUBLIC_CREDENTIALS, 1, 0.9d, now().minusMinutes(5),
                                                           now().minusMinutes(1)));
        break;
      case STUCK:
        protocolProgress.add(new ProtocolStageProgressVo<>(INITIALIZE_PARAMETERS, -1, 1d, now().minusMinutes(10),
                                                           now().minusMinutes(8)));
        protocolProgress
            .add(new ProtocolStageProgressVo<>(SEND_VOTERS, 0, 0.6d, now().minusMinutes(8), now().minusSeconds(5)));
        protocolProgress
            .add(new ProtocolStageProgressVo<>(SEND_VOTERS, 1, 0.1d, now().minusMinutes(8), now().minusMinutes(7)));
        break;
    }

    votingMaterialStatusById.put(operationId, new VotingMaterialsStatusVo(
        "MOCK-SERVER", now(), VotingMaterialsStatusVo.State.CREATION_IN_PROGRESS, null, PACT_URL,
        INVALIDATION_PAGE_URL, null, protocolProgress));
  }

  public void setVotingMaterialStatus(long operationId, VotingMaterialsStatusVo.State state, String userName) {
    databaseService.setWorkflowStep(WorkflowStep.VOTING_PERIOD_CONF_NOT_SENT);
    votingMaterialStatusById.put(operationId, new VotingMaterialsStatusVo(
        userName, now(), state, null, PACT_URL, INVALIDATION_PAGE_URL, TEST_FILE_LOCATIONS, new ArrayList<>()));
  }

  public void markVotingPeriodAsUploaded(long operationId) {
    databaseService.setWorkflowStep(WorkflowStep.VOTING_PERIOD_CONF_SENT);
    setVotingPeriodStatus(operationId, VotingPeriodStatusVo.State.AVAILABLE_FOR_INITIALIZATION, null);
  }

  public void setVotingPeriodStatus(long operationId, VotingPeriodStatusVo.State state, String reason) {
    databaseService.setWorkflowStep(WorkflowStep.VOTING_PERIOD_CONF_SENT);
    votingPeriodStatusById
        .put(operationId, new VotingPeriodStatusVo("MOCK-SERVER", now(), state, reason, PACT_URL));
  }

  public VotingPeriodStatusVo getVotingPeriodStatus(long operationId) {
    databaseService.setWorkflowStep(WorkflowStep.VOTING_PERIOD_CONF_SENT);
    return votingPeriodStatusById.get(operationId);
  }

  public void resetVotingPeriod() {
    databaseService.setWorkflowStep(WorkflowStep.VOTING_PERIOD_CONF_NOT_SENT);
    votingPeriodStatusById.clear();
  }

  public void setTallyArchiveStatus(long operationId, TallyArchiveStatusVo.State state) {
    databaseService.setWorkflowStep(WorkflowStep.VOTING_PERIOD_CONF_SENT);
    this.tallyArchiveStatusById.put(
        operationId,
        new TallyArchiveStatusVo(state, state == TallyArchiveStatusVo.State.CREATED ? TEST_FILE_LOCATION : null));
  }

  public TallyArchiveStatusVo getTallyArchiveStatus(long operationId) {
    databaseService.setWorkflowStep(WorkflowStep.VOTING_PERIOD_CONF_SENT);
    return tallyArchiveStatusById
        .getOrDefault(operationId, new TallyArchiveStatusVo(TallyArchiveStatusVo.State.NOT_REQUESTED, null));
  }

}
