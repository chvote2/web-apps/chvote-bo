/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server;

import ch.ge.ve.bo.mock.server.database.DatabaseController;
import ch.ge.ve.bo.mock.server.database.OperationOption;
import ch.ge.ve.bo.mock.server.database.RegisterType;
import ch.ge.ve.bo.mock.server.workflow.ManagementEntity;
import ch.ge.ve.bo.mock.server.workflow.WorkflowController;
import ch.ge.ve.bo.repository.entity.DeploymentTarget;
import ch.ge.ve.bo.service.conf.Canton;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional
public class UseCaseController {
  private static final Logger             logger = LoggerFactory.getLogger(UseCaseController.class);
  private final        DatabaseController databaseController;
  private final        WorkflowController workflowController;


  public UseCaseController(DatabaseController databaseController,
                           WorkflowController workflowController) {
    this.databaseController = databaseController;
    this.workflowController = workflowController;
  }


  /**
   * create a new test case
   */
  @RequestMapping(path = "use-case/create",
                  method = RequestMethod.GET)
  public long createUseCase(@RequestParam(value = "shortName", defaultValue = "OPE") String shortName,
                            @RequestParam(value = "longName", defaultValue = "Operation") String longName,
                            @RequestParam(value = "target",
                                          defaultValue = "SIMULATION") DeploymentTarget deploymentTarget,
                            @RequestParam(value = "managementEntity",
                                          defaultValue = "GE_CANTON_DE_GENEVE") ManagementEntity managementEntity,
                            @RequestParam(value = "votation", defaultValue = "true") boolean withVotation,
                            @RequestParam(value = "votation-incpqs", defaultValue = "false") boolean withVotationINCPQS,
                            @RequestParam(value = "votation-election", defaultValue = "false") boolean withElection,
                            @RequestParam(value = "register", defaultValue = "SR_SE") RegisterType registerType,
                            @RequestParam(value = "send-to-vote-receiver") boolean toVoteReceiver
  ) {

    List<OperationOption> operationOptions = new ArrayList<>();
    operationOptions.add(OperationOption.ADD_MILESTONE);
    operationOptions.add(OperationOption.ADD_DOMAIN_INFLUENCE);
    operationOptions.add(OperationOption.ADD_ELECTORAL_AUTHORITY_KEYS);
    operationOptions.add(OperationOption.ADD_DOCUMENT);
    operationOptions.add(OperationOption.ADD_TESTING_CARDS);
    operationOptions.add(OperationOption.ADD_CARD_TITLE);
    operationOptions.add(OperationOption.ADD_PRINTER_TEMPLATE);
    operationOptions.add(OperationOption.ADD_SIMULATION_SITE_PERIOD);
    operationOptions.add(OperationOption.DATE_ON_CARD_TITLE);
    operationOptions.add(OperationOption.ADD_VOTING_SITE_CONFIGURATION);

    if (withElection) {
      operationOptions.add(OperationOption.ADD_REPOSITORY_ELECTION);
      operationOptions.add(OperationOption.ADD_ELECTION_PAGE_PROPERTIES);
    }

    if (withVotation) {
      operationOptions.add(OperationOption.ADD_REPOSITORY_VOTATION);
    }

    if (withVotationINCPQS) {
      operationOptions.add(OperationOption.ADD_REPOSITORY_VOTATION_INCPQS);
    }

    if (EnumSet.of(RegisterType.SE, RegisterType.SR_SE).contains(registerType)) {
      operationOptions.add(OperationOption.ADD_REGISTER_SE);
    }

    if (EnumSet.of(RegisterType.SR, RegisterType.SR_SE).contains(registerType)) {
      operationOptions.add(OperationOption.ADD_REGISTER_SR);
    }

    long operationId = databaseController
        .createOperation(shortName, longName, managementEntity, operationOptions.toArray(new OperationOption[0]));

    if (toVoteReceiver) {
      workflowController
          .fastForward(operationId, Canton.valueOf(managementEntity.getDefaultUser().name()), deploymentTarget);
    }

    return operationId;
  }


}
