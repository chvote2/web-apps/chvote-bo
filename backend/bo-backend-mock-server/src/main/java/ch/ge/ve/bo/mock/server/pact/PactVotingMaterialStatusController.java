/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server.pact;

import ch.ge.ve.bo.mock.server.NotFound;
import ch.ge.ve.bo.mock.server.services.PactSimulator;
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile({"development", "integration"})
public class PactVotingMaterialStatusController {

  public enum ProgressLevel {
    ONE_STEP_DONE,
    TWO_STEPS_DONE,
    STUCK,
  }

  private final PactSimulator pactSimulator;

  public PactVotingMaterialStatusController(PactSimulator pactSimulator) {
    this.pactSimulator = pactSimulator;
  }


  @RequestMapping(path = "pact/operation/{operationId}/voting-materials/status", method = RequestMethod.GET)
  @ResponseBody
  public VotingMaterialsStatusVo getVotingMaterialStatus(@PathVariable("operationId") long operationId) {
    VotingMaterialsStatusVo status = pactSimulator.getVotingMaterialStatus(operationId);
    if (status == null) {
      throw new NotFound();
    }
    return status;
  }

  @RequestMapping(path = "pact/operation/{operationId}/voting-materials/validate", method = RequestMethod.POST)
  @ResponseBody
  public void validateTestSite(@PathVariable("operationId") long operationId, @RequestParam("user") String userName) {
    pactSimulator.setVotingMaterialStatus(operationId, VotingMaterialsStatusVo.State.VALIDATED, userName);
  }

  @RequestMapping(path = "pact/operation/{operationId}/reject-voting-material-creation", method = RequestMethod.GET)
  @ResponseBody
  public void rejectVotingMaterialCreation(@PathVariable("operationId") long operationId) {
    pactSimulator.setVotingMaterialStatus(operationId, VotingMaterialsStatusVo.State.CREATION_REJECTED,
                                          "Rejected as asked by mock server", null);
  }


  @RequestMapping(path = "pact/operation/{operationId}/request-voting-material-creation", method = RequestMethod.GET)
  @ResponseBody
  public void requestVotingMaterialCreation(@PathVariable("operationId") long operationId) {
    pactSimulator.setVotingMaterialStatus(operationId, VotingMaterialsStatusVo.State.CREATION_REQUESTED);
  }

  @RequestMapping(path = "pact/operation/{operationId}/set-voting-material-creation-to-in-progress/{progressLevel}",
                  method = RequestMethod.GET)
  @ResponseBody
  public void setVotingMaterialCreationToInProgress(@PathVariable("operationId") long operationId,
                                                    @PathVariable("progressLevel") ProgressLevel progressLevel) {

    pactSimulator.setVotingMaterialStatusInProgress(operationId, progressLevel);
  }


  @RequestMapping(path = "pact/operation/{operationId}/set-voting-material-creation-to-failed",
                  method = RequestMethod.GET)
  @ResponseBody
  public void setVotingMaterialCreationToFailed(@PathVariable("operationId") long operationId) {
    pactSimulator.setVotingMaterialStatus(operationId, VotingMaterialsStatusVo.State.CREATION_FAILED);
  }

  @RequestMapping(path = "pact/operation/{operationId}/set-voting-material-to-created", method = RequestMethod.GET)
  @ResponseBody
  public void setVotingMaterialToCreated(@PathVariable("operationId") long operationId) {
    pactSimulator.setVotingMaterialStatus(operationId, VotingMaterialsStatusVo.State.CREATED);
  }

  @RequestMapping(path = "pact/operation/{operationId}/request-invalidation-voting-material",
                  method = RequestMethod.GET)
  @ResponseBody
  public void requestInvalidationOfVotingMaterial(@PathVariable("operationId") long operationId) {
    pactSimulator.setVotingMaterialStatus(operationId, VotingMaterialsStatusVo.State.INVALIDATION_REQUESTED);
  }

  @GetMapping(path = "pact/operation/{operationId}/reject-invalidation-voting-material")
  @ResponseBody
  public void rejectInvalidationOfVotingMaterial(@PathVariable("operationId") long operationId) {
    pactSimulator.setVotingMaterialStatus(operationId, VotingMaterialsStatusVo.State.INVALIDATION_REJECTED);
  }

  @RequestMapping(path = "pact/operation/{operationId}/invalidate-voting-material", method = RequestMethod.GET)
  @ResponseBody
  public void invalidateVotingMaterial(@PathVariable("operationId") long operationId) {
    pactSimulator.setVotingMaterialStatus(operationId, VotingMaterialsStatusVo.State.INVALIDATED);
  }

}
