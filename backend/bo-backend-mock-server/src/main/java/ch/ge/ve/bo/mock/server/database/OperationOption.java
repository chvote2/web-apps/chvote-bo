/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server.database;

public enum OperationOption {
  ADD_ALL,
  ADD_MILESTONE,
  ADD_DOMAIN_INFLUENCE,
  ADD_REPOSITORY_VOTATION,
  ADD_REPOSITORY_VOTATION_INCPQS,
  ADD_REPOSITORY_ELECTION,
  ADD_ELECTORAL_AUTHORITY_KEYS,
  ADD_DOCUMENT,
  ADD_TESTING_CARDS,
  ADD_ELECTION_PAGE_PROPERTIES,
  ADD_REGISTER_SE,
  ADD_REGISTER_SR,
  ADD_CARD_TITLE,
  ADD_PRINTER_TEMPLATE,
  ADD_SIMULATION_SITE_PERIOD,
  ADD_VOTING_SITE_CONFIGURATION,
  DATE_ON_CARD_TITLE,
  REMOVE_ELECTION_PAGE_PROPERTIES,
  REMOVE_REPOSITORY_ELECTION,
  REMOVE_BALLOT_DOCUMENTATION,
  FOR_REAL,
  FOR_SIMULATION,
  WITH_VOTING_MATERIAL_FINALIZED,
}
