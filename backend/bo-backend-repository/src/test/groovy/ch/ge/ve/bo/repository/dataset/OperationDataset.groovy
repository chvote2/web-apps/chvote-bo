/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.dataset

import static ch.ge.ve.bo.MilestoneType.BALLOT_BOX_DECRYPT
import static ch.ge.ve.bo.MilestoneType.BALLOT_BOX_INIT
import static ch.ge.ve.bo.MilestoneType.CERTIFICATION
import static ch.ge.ve.bo.MilestoneType.DATA_DESTRUCTION
import static ch.ge.ve.bo.MilestoneType.PRINTER_FILES
import static ch.ge.ve.bo.MilestoneType.RESULT_VALIDATION
import static ch.ge.ve.bo.MilestoneType.SITE_CLOSE
import static ch.ge.ve.bo.MilestoneType.SITE_OPEN
import static ch.ge.ve.bo.MilestoneType.SITE_VALIDATION
import static ch.ge.ve.bo.repository.dataset.DatasetTools.entity
import static ch.ge.ve.bo.repository.dataset.DatasetTools.localDateTime

import ch.ge.ve.bo.repository.entity.DeploymentTarget
import ch.ge.ve.bo.repository.entity.Operation
import java.time.LocalDateTime

/**
 * Dataset used for testing {@link Operation} entities and repository.
 */
class OperationDataset {
  static final LocalDateTime LAST_CONFIGURATION_UPDATE_DATE = localDateTime('01.01.2017 00:10:00')
  static final LocalDateTime LAST_VOTING_MATERIAL_UPDATE_DATE = localDateTime('02.01.2017 00:10:00')
  static final LocalDateTime LAST_VOTING_PERIOD_CONFIG_UPDATE_DATE = localDateTime('03.01.2017 00:10:00')
  static final DEFAULT_MANAGEMENT_ENTITY = "Canton de Genève"
  static final DEFAULT_CANTON = "GE"
  public static final String PRINTER_TEMPLATE = "printerTemplate"

  static Operation electoralOperation(Options options = new Options()) {
    entity().operation(
            id: options.get(Options.id, 1),
            shortLabel: 'EL042050',
            longLabel: 'Élections communales du 19 avril 2050',
            date: options.get(Options.operationDate, localDateTime('19.04.2050 00:00:00')),
            gracePeriod: options.get(Options.withMilestone, true) ? 9 : null,
            login: 'test',
            saveDate: localDateTime('13.12.2051 00:00:00'),
            milestones: options.get(Options.withMilestone, true) ? [
                    entity().milestone(id: 1, type: SITE_VALIDATION, date: localDateTime('11.02.2050 00:00:00'), login: 'test', saveDate: localDateTime('19.04.2050 00:00:00')),
                    entity().milestone(id: 2, type: CERTIFICATION, date: localDateTime('27.02.2050 00:00:00'), login: 'test', saveDate: localDateTime('19.04.2050 00:00:00')),
                    entity().milestone(id: 3, type: PRINTER_FILES, date: localDateTime('04.03.2050 00:00:00'), login: 'test', saveDate: localDateTime('19.04.2050 00:00:00')),
                    entity().milestone(id: 4, type: BALLOT_BOX_INIT, date: localDateTime('20.03.2050 00:00:00'), login: 'test', saveDate: localDateTime('19.04.2050 00:00:00')),
                    entity().milestone(id: 5, type: SITE_OPEN, date: localDateTime('23.03.2050 12:00:00'), login: 'test', saveDate: localDateTime('19.04.2050 00:00:00')),
                    entity().milestone(id: 6, type: SITE_CLOSE, date: localDateTime('18.04.2050 12:00:00'), login: 'test', saveDate: localDateTime('19.04.2050 00:00:00')),
                    entity().milestone(id: 7, type: BALLOT_BOX_DECRYPT, date: localDateTime('19.04.2050 00:00:00'), login: 'test', saveDate: localDateTime('19.04.2050 00:00:00')),
                    entity().milestone(id: 8, type: RESULT_VALIDATION, date: localDateTime('21.04.2050 00:00:00'), login: 'test', saveDate: localDateTime('19.04.2050 00:00:00')),
                    entity().milestone(id: 9, type: DATA_DESTRUCTION, date: localDateTime('18.07.2050 00:00:00'), login: 'test', saveDate: localDateTime('19.04.2050 00:00:00'))
            ] : [],
            inModification: options.get(Options.inModification, false),
            deploymentTarget: options.get(Options.deploymentTarget, DeploymentTarget.NOT_DEFINED),
            simulationName: options.get(Options.simulationName, null),
            votingCardTitle: options.get(Options.votingCardTitle, null),
            printerTemplate: options.get(Options.printerTemplate, PRINTER_TEMPLATE),
            lastConfigurationUpdateDate: LAST_CONFIGURATION_UPDATE_DATE,
            lastVotingMaterialUpdateDate: LAST_VOTING_MATERIAL_UPDATE_DATE,
            lastVotingPeriodConfigUpdateDate: LAST_VOTING_PERIOD_CONFIG_UPDATE_DATE,
            managementEntity: DEFAULT_MANAGEMENT_ENTITY,
            canton: DEFAULT_CANTON
    )
  }

  static Operation votingOperation(Options options = new Options()) {
    entity().operation(
            id: options.get(Options.id, 2),
            shortLabel: 'VP052055',
            longLabel: 'Votation Populaire du 21 mai 2055',
            date: options.get(Options.operationDate, localDateTime('21.05.2055 00:00:00')),
            gracePeriod: 7,
            login: 'test',
            saveDate: localDateTime('10.04.2055 00:00:00'),
            milestones: options.get(Options.withMilestone, true) ? [
                    entity().milestone(id: 10, type: SITE_VALIDATION, date: localDateTime('02.05.2055 00:00:00'), login: 'test', saveDate: localDateTime('19.04.2050 00:00:00')),
                    entity().milestone(id: 11, type: CERTIFICATION, date: localDateTime('02.05.2055 00:00:00'), login: 'test', saveDate: localDateTime('19.04.2050 00:00:00')),
                    entity().milestone(id: 12, type: PRINTER_FILES, date: localDateTime('03.05.2055 00:00:00'), login: 'test', saveDate: localDateTime('19.04.2050 00:00:00')),
                    entity().milestone(id: 13, type: BALLOT_BOX_INIT, date: localDateTime('08.05.2055 00:00:00'), login: 'test', saveDate: localDateTime('19.04.2050 00:00:00')),
                    entity().milestone(id: 14, type: SITE_OPEN, date: localDateTime('08.05.2055 12:00:00'), login: 'test', saveDate: localDateTime('19.04.2050 00:00:00')),
                    entity().milestone(id: 15, type: SITE_CLOSE, date: localDateTime('20.05.2055 12:00:00'), login: 'test', saveDate: localDateTime('19.04.2050 00:00:00')),
                    entity().milestone(id: 16, type: BALLOT_BOX_DECRYPT, date: localDateTime('21.05.2055 00:00:00'), login: 'test', saveDate: localDateTime('19.04.2050 00:00:00')),
                    entity().milestone(id: 17, type: RESULT_VALIDATION, date: localDateTime('23.05.2055 00:00:00'), login: 'test', saveDate: localDateTime('19.04.2050 00:00:00')),
                    entity().milestone(id: 18, type: DATA_DESTRUCTION, date: localDateTime('19.08.2055 00:00:00'), login: 'test', saveDate: localDateTime('19.04.2050 00:00:00'))
            ] : [],
            inModification: options.get(Options.inModification, false),
            deploymentTarget: options.get(Options.deploymentTarget, DeploymentTarget.NOT_DEFINED),
            simulationName: options.get(Options.simulationName, null),
            votingCardTitle: options.get(Options.votingCardTitle, null),
            printerTemplate: options.get(Options.printerTemplate, PRINTER_TEMPLATE),
            lastConfigurationUpdateDate: LAST_CONFIGURATION_UPDATE_DATE,
            lastVotingMaterialUpdateDate: LAST_VOTING_MATERIAL_UPDATE_DATE,
            managementEntity: DEFAULT_MANAGEMENT_ENTITY,
            canton: DEFAULT_CANTON
    )
  }


  static class Options {
    static def id = new PossibleOption<Long>() {}
    static def withMilestone = new PossibleOption<Boolean>() {}
    static def operationDate = new PossibleOption<LocalDateTime>() {}
    static def deploymentTarget = new PossibleOption<DeploymentTarget>() {}
    static def simulationName = new PossibleOption<String>() {}
    static def inModification = new PossibleOption<Boolean>() {}
    static def votingCardTitle = new PossibleOption<String>() {}
    static def printerTemplate = new PossibleOption<String>() {}

    final Map<PossibleOption, Object> values = new HashMap<>()


    def <T> T get(PossibleOption<T> option, T defaultValue) {
      return (T) values.getOrDefault(option, defaultValue)
    }

    def <T> Options with(PossibleOption<T> option, T value) {
      values.put(option, value)
      return this
    }

    static abstract class PossibleOption<T> {

    }
  }


}
