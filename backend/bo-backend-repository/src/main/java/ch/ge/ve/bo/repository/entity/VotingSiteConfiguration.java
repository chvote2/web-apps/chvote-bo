/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.entity;

import static ch.ge.ve.bo.repository.entity.EntityConstants.PROPERTY_ID;
import static ch.ge.ve.bo.repository.entity.EntityConstants.PROPERTY_LOGIN;
import static ch.ge.ve.bo.repository.entity.EntityConstants.PROPERTY_SAVE;
import static ch.ge.ve.bo.repository.entity.EntityConstants.SEQUENCE_GENERATOR;

import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.repository.security.OperationHolder;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * Entity representing a voting site specific configuration.
 */
@Entity
@Table(name = "BO_T_VOTING_SITE_CONFIG")
@SequenceGenerator(name = SEQUENCE_GENERATOR, allocationSize = 1, sequenceName = "BO_S_VOTING_SITE_CONFIG")
@AttributeOverrides({
                        @AttributeOverride(name = PROPERTY_ID, column = @Column(name = "VSC_N_ID")),
                        @AttributeOverride(name = PROPERTY_LOGIN, column = @Column(name = "VSC_C_LOGIN")),
                        @AttributeOverride(name = PROPERTY_SAVE, column = @Column(name = "VSC_D_SAVE"))
                    })
public class VotingSiteConfiguration extends BaseEntity implements OperationHolder {

  @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
  @JoinColumn(name = "VSC_N_OPE_ID", nullable = false)
  private Operation operation;


  @Column(name = "VSC_C_DEFAULT_LANG")
  @Enumerated(EnumType.STRING)
  private Language defaultLanguage;


  @ElementCollection
  @CollectionTable(name = "BO_T_VOTING_SITE_CONFIG_LANG", joinColumns = {@JoinColumn(name = "VSCL_N_CONFIG_ID")})
  @Column(name = "VSCL_C_LANGUAGE")
  @Enumerated(EnumType.STRING)
  private Set<Language> allLanguages = new HashSet<>();

  public void setOperation(Operation operation) {
    this.operation = operation;
  }

  public Language getDefaultLanguage() {
    return defaultLanguage;
  }

  public void setDefaultLanguage(Language defaultLanguage) {
    this.defaultLanguage = defaultLanguage;
  }

  public void setAllLanguages(Set<Language> allLanguages) {
    this.allLanguages = allLanguages;
  }

  public Set<Language> getAllLanguages() {
    return allLanguages;
  }

  @Override
  public Operation getOperation() {
    return operation;
  }

  @Override
  public boolean onlyManagementEntityInChargeCanCreate() {
    return true;
  }

  @Override
  public OperationParameterType getOperationParameterType() {
    return OperationParameterType.CONFIGURATION;
  }
}
