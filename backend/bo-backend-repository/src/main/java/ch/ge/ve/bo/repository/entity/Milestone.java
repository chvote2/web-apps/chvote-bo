/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.entity;

import ch.ge.ve.bo.MilestoneType;
import ch.ge.ve.bo.repository.security.OperationHolder;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity representing a milestone in the operation calendar.
 */
@Entity
@Table(name = "BO_T_MILESTONE")
@SequenceGenerator(name = EntityConstants.SEQUENCE_GENERATOR, allocationSize = 1, sequenceName = "BO_S_MILESTONE")
@AttributeOverrides({
                        @AttributeOverride(name = EntityConstants.PROPERTY_ID, column = @Column(name = "MIL_N_ID")),
                        @AttributeOverride(name = EntityConstants.PROPERTY_LOGIN,
                                           column = @Column(name = "MIL_C_LOGIN")),
                        @AttributeOverride(name = EntityConstants.PROPERTY_SAVE, column = @Column(name = "MIL_D_SAVE"))
                    })
public class Milestone extends BaseEntity implements OperationHolder {

  @ManyToOne
  @JoinColumn(name = "MIL_OPE_N_ID", referencedColumnName = "OPE_N_ID")
  private Operation operation;

  @Column(name = "MIL_C_TYPE")
  @Enumerated(EnumType.STRING)
  private MilestoneType type;

  @Column(name = "MIL_D_DATE")
  @SuppressWarnings("squid:S3437") // suppress Sonar check on serialized value-based objects
  private LocalDateTime date;

  @Override
  public Operation getOperation() {
    return operation;
  }

  @Override
  public boolean onlyManagementEntityInChargeCanCreate() {
    return true;
  }

  @Override
  public OperationParameterType getOperationParameterType() {
    return OperationParameterType.CONFIGURATION;
  }

  public void setOperation(Operation operation) {
    this.operation = operation;
  }

  public MilestoneType getType() {
    return type;
  }

  public void setType(MilestoneType milestoneType) {
    this.type = milestoneType;
  }

  public LocalDateTime getDate() {
    return date;
  }

  public void setDate(LocalDateTime date) {
    this.date = date;
  }

  @Override
  public int hashCode() {
    return Objects.hash(operation, type, date);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    final Milestone other = (Milestone) obj;
    return Objects.equals(this.operation, other.operation)
           && Objects.equals(this.type, other.type)
           && Objects.equals(this.date, other.date);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Milestone{");
    sb.append("id=").append(getId());
    sb.append(", milestoneType=").append(type);
    sb.append(", date=").append(date != null ? date.format(DATE_FORMATTER) : null);
    sb.append(", login=").append(getLogin());
    sb.append(", saveDate=").append(getSaveDate() != null ? getSaveDate().format(DATE_FORMATTER) : null);
    sb.append('}');
    return sb.toString();
  }
}
