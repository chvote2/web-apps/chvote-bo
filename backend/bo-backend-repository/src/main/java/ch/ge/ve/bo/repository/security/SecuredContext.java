/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.security;

import java.util.function.Supplier;

/**
 * Allow modification in a secured context by bypassing OperationSecurityRepositoryFactoryBean
 */
public class SecuredContext {
  private ThreadLocal<Integer> isInSecureContext = new ThreadLocal<>();

  /**
   * Do an operation in secured context
   */
  public <T> T doInSecureContext(Supplier<T> supplier) {
    try {
      isInSecureContext.set(isInSecureContext.get() == null ? 1 : 1 + isInSecureContext.get());
      return supplier.get();
    } finally {
      isInSecureContext.set(isInSecureContext.get() - 1);
    }
  }

  public boolean isInSecureContext() {
    Integer value = isInSecureContext.get();
    return value != null && value > 0;
  }

}
