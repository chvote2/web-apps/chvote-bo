/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.entity;

import ch.ge.ve.bo.repository.security.OperationHolder;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity representing the place holder of localized questions linked to an operation
 */
@Entity
@Table(name = "BO_T_HIGHLIGHTED_QUESTION")
@SequenceGenerator(name = EntityConstants.SEQUENCE_GENERATOR, allocationSize = 1,
                   sequenceName = "BO_S_HIGHLIGHTED_QUESTION")
@AttributeOverrides({
                        @AttributeOverride(name = EntityConstants.PROPERTY_ID, column = @Column(name = "HQU_N_ID")),
                        @AttributeOverride(name = EntityConstants.PROPERTY_LOGIN,
                                           column = @Column(name = "HQU_C_LOGIN")),
                        @AttributeOverride(name = EntityConstants.PROPERTY_SAVE, column = @Column(name = "HQU_D_SAVE"))
                    })
public class HighlightedQuestion extends BaseEntity implements OperationHolder{

  @ManyToOne
  @JoinColumn(name = "HQU_OPE_N_ID", referencedColumnName = "OPE_N_ID")
  private Operation operation;

  @OneToMany(mappedBy = "question", orphanRemoval = true, cascade = CascadeType.ALL)
  private List<LocalizedHighlightedQuestion> localizedQuestions;

  @Override
  public Operation getOperation() {
    return operation;
  }

  @Override
  public boolean onlyManagementEntityInChargeCanCreate() {
    return true;
  }

  @Override
  public OperationParameterType getOperationParameterType() {
    return OperationParameterType.CONFIGURATION;
  }

  public void setOperation(Operation operation) {
    this.operation = operation;
  }

  public List<LocalizedHighlightedQuestion> getLocalizedQuestions() {
    return localizedQuestions;
  }

  public void setLocalizedQuestions(List<LocalizedHighlightedQuestion> localizedQuestions) {
    this.localizedQuestions = localizedQuestions;
  }

}
