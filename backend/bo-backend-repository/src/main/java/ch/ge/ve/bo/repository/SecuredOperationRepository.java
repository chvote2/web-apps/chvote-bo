/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository;

import ch.ge.ve.bo.repository.entity.Operation;
import java.util.List;

/**
 * Operation repository that
 * - insure that connected user is in correct management entity
 * - insure that change dates are correctly updated
 * - insure that consistency checks are done
 */
public interface SecuredOperationRepository {

  /**
   * Register hash is used to create a lock to avoid concurent acces to repository import
   */
  void updateRegisterHash(Long operationId, String newHash);

  /**
   * Mark a configuration as updated in order to schedule a consistency check
   */
  void markConfigurationAsUpdated(long operationId, ConsistencyCheckCallback consistencyCheckCallback);

  /**
   * Mark a voting material as updated in order to schedule a consistency check
   */
  void markVotingMaterialAsUpdated(long operationId, ConsistencyCheckCallback consistencyCheckCallback);

  /**
   * find all operation for a given management entity
   */
  List<Operation> findAllByManagementEntity(String managementEntity);


  /**
   * Save an operation
   */
  Operation save(Operation operation);

  /**
   * find one operation and insure that user's management entity is authorized to see this operation
   */
  Operation findOne(Long operationId, boolean restrictedToOperationManagementEntity);

  /**
   * Method used to update register
   *
   * @return 0 if no update in register has been done since user validated it.
   */
  int getRegisterFilesLock(Long operationId, String preRegisterHash, String postRegisterHash);

  /**
   * Mark a voting period as updated (change date)
   */
  void markVotingPeriodConfigAsUpdated(Long operationId);
}
