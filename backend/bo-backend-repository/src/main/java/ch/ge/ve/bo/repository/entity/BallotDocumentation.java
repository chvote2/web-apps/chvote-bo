/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.entity;


import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.repository.security.ManagementEntityHolder;
import ch.ge.ve.bo.repository.security.OperationHolder;
import javax.persistence.AttributeOverride;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity representing a ballot documentation
 */
@Entity
@Table(name = "BO_T_BALLOT_DOCUMENTATION")
@SequenceGenerator(name = EntityConstants.SEQUENCE_GENERATOR, allocationSize = 1, sequenceName = "BO_S_BALLOT_DOCUMENTATION")
@AttributeOverride(name = EntityConstants.PROPERTY_ID, column = @Column(name = "BDOC_N_ID"))
@AttributeOverride(name = EntityConstants.PROPERTY_LOGIN, column = @Column(name = "BDOC_C_LOGIN"))
@AttributeOverride(name = EntityConstants.PROPERTY_SAVE, column = @Column(name = "BDOC_D_SAVE"))

public class BallotDocumentation extends BaseEntity implements OperationHolder, ManagementEntityHolder {

  @Lob
  @Column(name = "BDOC_X_CONTENT")
  @Basic(fetch = FetchType.LAZY)
  private byte[] fileContent;

  @Column(name = "BDOC_C_FILENAME")
  private String fileName;

  @Column(name = "BDOC_C_LANGUAGE")
  @Enumerated(EnumType.STRING)
  private Language language;

  @Column(name = "BDOC_C_LOCALIZED_LABEL")
  private String localizedLabel;

  @Column(name = "BDOC_C_BALLOT_ID")
  private String ballotId;

  @Column(name = "BDOC_C_MANAGMENT_ENTITY")
  private String managementEntity;

  @ManyToOne
  @JoinColumn(name = "BDOC_OPE_N_ID", referencedColumnName = "OPE_N_ID")
  private Operation operation;

  @Override
  public String getManagementEntity() {
    return managementEntity;
  }

  public void setManagementEntity(String managementEntity) {
    this.managementEntity = managementEntity;
  }

  public byte[] getFileContent() {
    return fileContent;
  }

  public void setFileContent(byte[] fileContent) {
    this.fileContent = fileContent;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public Language getLanguage() {
    return language;
  }

  public void setLanguage(Language language) {
    this.language = language;
  }

  public String getLocalizedLabel() {
    return localizedLabel;
  }

  public void setLocalizedLabel(String localizedLabel) {
    this.localizedLabel = localizedLabel;
  }

  public String getBallotId() {
    return ballotId;
  }

  public void setBallotId(String ballotId) {
    this.ballotId = ballotId;
  }


  @Override
  public Operation getOperation() {
    return operation;
  }

  @Override
  public boolean onlyManagementEntityInChargeCanCreate() {
    return false;
  }

  @Override
  public OperationParameterType getOperationParameterType() {
    return OperationParameterType.CONFIGURATION;
  }

  public void setOperation(Operation operation) {
    this.operation = operation;
  }
}
