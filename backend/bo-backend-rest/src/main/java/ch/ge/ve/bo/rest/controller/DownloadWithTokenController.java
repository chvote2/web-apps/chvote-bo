/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller;

import ch.ge.ve.bo.repository.DownloadTokenRepository;
import ch.ge.ve.bo.repository.entity.DownloadToken;
import ch.ge.ve.bo.rest.DynamicResourceHttpRequestHandler;
import ch.ge.ve.bo.service.exception.TechnicalException;
import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for download token
 */
@RestController
@RequestMapping("download-with-token")
public class DownloadWithTokenController {
  private static final Logger logger = LoggerFactory.getLogger(DownloadWithTokenController.class);


  private final DownloadTokenRepository           tokenRepository;
  private final DynamicResourceHttpRequestHandler dynamicResourceHttpRequestHandler;

  /**
   * Default constructor
   */
  public DownloadWithTokenController(DownloadTokenRepository tokenRepository,
                                     DynamicResourceHttpRequestHandler dynamicResourceHttpRequestHandler) {
    this.tokenRepository = tokenRepository;
    this.dynamicResourceHttpRequestHandler = dynamicResourceHttpRequestHandler;
  }

  /**
   * return file attached to the token
   */
  @GetMapping(value = "/{token}")
  @SuppressWarnings("anonymous_user")
  public void get(@PathVariable("token") String token, HttpServletResponse response, HttpServletRequest request) {
    DownloadToken downloadToken =
        tokenRepository.findById(token).orElseThrow(() -> new EntityNotFoundException("token: " + token));
    String downloadTokenPath = downloadToken.getPath();
    logger.info("User {} request file {}", downloadToken.getLogin(), downloadTokenPath);

    File file = new File(downloadTokenPath);

    response.setContentType(Optional.ofNullable(URLConnection.guessContentTypeFromName(file.getName()))
                                    .orElse("application/octet-stream"));

    response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));

    request.setAttribute(DynamicResourceHttpRequestHandler.ATTR_FILE, downloadTokenPath);
    try {
      dynamicResourceHttpRequestHandler.handleRequest(request, response);
    } catch (ServletException | IOException e) {
      throw new TechnicalException(e);
    }
  }


}
