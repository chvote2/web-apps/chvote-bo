/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller.operation;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.operation.exception.PactRequestException;
import ch.ge.ve.bo.service.operation.model.OperationStatusAndConsistencyVo;
import ch.ge.ve.bo.service.operation.status.OperationStatusAndConsistencyService;
import ch.ge.ve.bo.service.operation.status.OperationWorkflowService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


/**
 * Controller used to change operation status workflow
 */
@SuppressWarnings("ALL")
@RestController
@RequestMapping(value = "/operation/{operationId}", method = GET)
public class OperationStatusController {
  private final OperationStatusAndConsistencyService operationStatusService;
  private final OperationWorkflowService             operationWorkflowService;

  /**
   * default constructor
   */
  public OperationStatusController(OperationStatusAndConsistencyService operationStatusService,
                                   OperationWorkflowService operationWorkflowService) {
    this.operationStatusService = operationStatusService;
    this.operationWorkflowService = operationWorkflowService;
  }


  /**
   * Switch an operation in configuration modification mode
   */
  @PutMapping(value = "/modify")
  @PreAuthorize("hasRole('MODIFY_CONFIGURATION')")
  public void modifyConfiguration(@PathVariable long operationId) throws BusinessException {
    operationWorkflowService.switchToModificationMode(operationId);
  }


  /**
   * Retrieve operation status
   */
  @ResponseBody
  @RequestMapping("/status")
  @PreAuthorize("hasRole('USER')")
  public OperationStatusAndConsistencyVo getOperationStatus(@PathVariable long operationId)
      throws PactRequestException {
    return operationStatusService.getOperationStatusAndConsistency(operationId);
  }

  /**
   * invalidate a test site
   */
  @RequestMapping(value = "/invalidate-test-site")
  @PreAuthorize("hasRole('INVALIDATE_TEST_SITE')")
  public boolean invalidate(@PathVariable long operationId) throws BusinessException {
    operationWorkflowService.invalidateTestSite(operationId);
    return true;
  }

  /**
   * validate a test site
   */
  @RequestMapping(value = "/validate-test-site")
  @PreAuthorize("hasRole('VALIDATE_TEST_SITE')")
  public boolean validate(@PathVariable long operationId) throws BusinessException {
    operationWorkflowService.validateTestSite(operationId);
    return true;
  }

  /**
   * validate voting material
   */
  @RequestMapping(value = "/validate-voting-material")
  @PreAuthorize("hasRole('VALIDATE_VOTING_MATERIAL')")
  public boolean validateVotingMaterial(@PathVariable long operationId) throws BusinessException {
    operationWorkflowService.validateVotingMaterial(operationId);
    return true;
  }

  /**
   * close a voting period
   */
  @RequestMapping(path = "/close-voting-period")
  @PreAuthorize("hasRole('CLOSE_VOTING_PERIOD_IN_SIMULATION')")
  public void closeVotingPeriod(@PathVariable("operationId") long operationId) throws BusinessException {
    operationWorkflowService.closeVotingPeriod(operationId);
  }

}
