/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller.operation.parameters;

import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.rest.controller.FileControllerUtils;
import ch.ge.ve.bo.rest.controller.TransferFileType;
import ch.ge.ve.bo.rest.services.FileUploadService;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.model.ContentAndFileName;
import ch.ge.ve.bo.service.operation.model.HighlightedQuestionVo;
import ch.ge.ve.bo.service.operation.parameters.document.HighlightedQuestionService;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Highlighted Question Controller
 */
@RestController
@RequestMapping("operation/{operationId}/highlighted-question")
public class HighlightedQuestionController {
  private final HighlightedQuestionService questionService;
  private final FileUploadService          uploadService;

  /**
   * Default constructor
   */
  public HighlightedQuestionController(HighlightedQuestionService questionService,
                                       FileUploadService uploadService) {
    this.questionService = questionService;
    this.uploadService = uploadService;
  }


  /**
   * @return all Highlighted Question to an operation
   */
  @GetMapping
  @PreAuthorize("hasRole('USER')")
  public List<HighlightedQuestionVo> findByOperation(@PathVariable("operationId") Long operationId) {
    return questionService.findByOperation(operationId);
  }

  /**
   * download am Highlighted Question content
   */
  @GetMapping(value = "/download/{localizedQuestionId}")
  @PreAuthorize("hasRole('USER')")
  public void download(@PathVariable("localizedQuestionId") long localizedQuestionId,
                       HttpServletResponse response) throws IOException {
    ContentAndFileName contentAndFileName = questionService.getFileContent(localizedQuestionId);
    FileControllerUtils.sendFile(response,
                                 contentAndFileName.getFileName(),
                                 contentAndFileName.getContent(),
                                 TransferFileType.PDF);
  }

  /**
   * Add an Highlighted Question to an operation
   */
  @PutMapping
  @PreAuthorize("hasRole('ADD_HIGHLIGHTED_QUESTION')")
  public HighlightedQuestionVo addQuestion(@PathVariable("operationId") Long operationId) {
    return questionService.addQuestion(operationId);
  }

  /**
   * Add an Highlighted Question localized content to an operation
   */
  @PostMapping(path = "{questionId}")
  @PreAuthorize("hasRole('ADD_LOCALIZED_HIGHLIGHTED_QUESTION')")
  public HighlightedQuestionVo addLocalizedQuestion(@PathVariable("questionId") Long highlightedQuestionId,
                                                    @RequestParam("localizedQuestion") String localizedQuestion,
                                                    @RequestParam("language") Language language,
                                                    HttpServletRequest request) throws BusinessException {
    return uploadService.processFileUpload(
        request,
        "file",
        (fileName, inputStream) -> {
          try {
            return questionService.addLocalizedQuestion(
                highlightedQuestionId,
                fileName,
                localizedQuestion,
                language,
                IOUtils.toByteArray(inputStream));
          } catch (IOException e) {
            throw new TechnicalException(e);
          }
        }
    );
  }

  /**
   * delete a Highlighted Question localized content
   */
  @DeleteMapping(path = "{questionId}/{localizedQuestionId}")
  @PreAuthorize("hasRole('DELETE_LOCALIZED_HIGHLIGHTED_QUESTION')")
  public HighlightedQuestionVo deleteLocalizedQuestion(@PathVariable("questionId") Long questionId,
                                                       @PathVariable("localizedQuestionId") Long localizedQuestionId) {
    return questionService.deleteLocalizedQuestion(questionId, localizedQuestionId);
  }

  /**
   * delete a Highlighted Question and all related contents
   */
  @DeleteMapping(path = "{questionId}")
  @PreAuthorize("hasRole('DELETE_HIGHLIGHTED_QUESTION')")
  public void deleteQuestion(@PathVariable("questionId") Long questionId) {
    questionService.deleteQuestion(questionId);
  }


}
