/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller;

import ch.ge.ve.bo.rest.services.FileUploadService;
import ch.ge.ve.bo.service.admin.ElectoralAuthorityKeyService;
import ch.ge.ve.bo.service.electoralAuthorityKey.model.ElectoralAuthorityKeyVo;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * Controller for electoral authority key request
 */
@RestController
@RequestMapping("electoral-authority-key")
public class ElectoralAuthorityKeyController {
  private static final Logger logger = LoggerFactory.getLogger(ElectoralAuthorityKeyController.class);

  private static final BusinessException INVALID_FORMAT_EXCEPTION =
      new BusinessException("admin.electoral-authority-key.error.invalid-format");


  private final ElectoralAuthorityKeyService service;
  private final FileUploadService            uploadService;
  private final ObjectMapper                 objectMapper;

  /**
   * Default constructor
   */
  public ElectoralAuthorityKeyController(ElectoralAuthorityKeyService service,
                                         ObjectMapper objectMapper,
                                         FileUploadService uploadService) {
    this.service = service;
    this.objectMapper = objectMapper;
    this.uploadService = uploadService;
  }

  /**
   * @return all {@link ElectoralAuthorityKeyVo} for user realm
   */
  @GetMapping
  @PreAuthorize("hasRole('USER')")
  public List<ElectoralAuthorityKeyVo> findAll() {
    return service.findAll();
  }

  /**
   * add an {@link ElectoralAuthorityKeyVo} in the user realm
   */
  @PostMapping
  @PreAuthorize("hasRole('ADD_ELECTORAL_AUTHORITY_KEY')")
  public ElectoralAuthorityKeyVo addElectoralAuthorityKey(
      @RequestParam("label") String label, HttpServletRequest request) throws BusinessException {
    return uploadService.processFileUpload(request, "file",
                                           (fileName, inputStream) -> service
                                               .create(label, getStreamOfPublicKey(inputStream))
    );
  }

  private byte[] getStreamOfPublicKey(InputStream inputStream) throws BusinessException {
    try {
      return objectMapper.writeValueAsBytes(getPublicKey(inputStream));
    } catch (JsonProcessingException e) {
      logger.error("Error during mapping public key: {}", e);
      throw INVALID_FORMAT_EXCEPTION;
    }
  }

  /**
   * Read, with format validation, a public electoral authority key from request's stream
   *
   * @param inputStream input to be validated
   *
   * @return a BigInteger
   *
   * @throws BusinessException if electoral authority key are in invalid format
   */
  private EncryptionPublicKey getPublicKey(InputStream inputStream) throws BusinessException {
    try {
      EncryptionPublicKey encryptionPublicKey = objectMapper.readValue(inputStream, EncryptionPublicKey.class);
      if (encryptionPublicKey == null || encryptionPublicKey.getPublicKey() == null) {
        throw INVALID_FORMAT_EXCEPTION;
      }
      return encryptionPublicKey;
    } catch (IOException e) {
      logger.error("Error when getting public key : {}", e);
      throw INVALID_FORMAT_EXCEPTION;
    }
  }


  /**
   * @return update a given {@link ElectoralAuthorityKeyVo} label
   */
  @PutMapping
  @PreAuthorize("hasRole('ADD_ELECTORAL_AUTHORITY_KEY')")
  public ElectoralAuthorityKeyVo addElectoralAuthorityKey(@RequestBody ElectoralAuthorityKeyUpdate update) {
    return service.updateLabel(update.id, update.label);
  }

  static class ElectoralAuthorityKeyUpdate {
    private String label;
    private long   id;

    @JsonCreator
    public ElectoralAuthorityKeyUpdate(@JsonProperty("label") String label, @JsonProperty("id") long id) {
      this.label = label;
      this.id = id;
    }

    public void setId(long id) {
      this.id = id;
    }

    public void setLabel(String label) {
      this.label = label;
    }
  }
}
