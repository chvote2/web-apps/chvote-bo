/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller.operation.voting.period;

import ch.ge.ve.bo.service.electoralAuthorityKey.model.ElectoralAuthorityKeyVo;
import ch.ge.ve.bo.service.operation.voting.period.ElectoralAuthorityKeyConfigurationService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * controller used to link {@link ch.ge.ve.bo.repository.entity.ElectoralAuthorityKey} to operation
 */
@RestController
@RequestMapping("/operation/{operationId}/electoral-authority-key")
public class ElectoralAuthorityKeyConfigurationController {
  private final ElectoralAuthorityKeyConfigurationService service;

  /**
   * Default constructor
   */
  public ElectoralAuthorityKeyConfigurationController(ElectoralAuthorityKeyConfigurationService service) {
    this.service = service;
  }

  /**
   * link {@link ch.ge.ve.bo.repository.entity.ElectoralAuthorityKey} to operation
   */
  @PostMapping
  @PreAuthorize("hasRole('SELECT_ELECTORAL_AUTHORITY_KEY')")
  public ElectoralAuthorityKeyVo saveOrUpdate(@PathVariable("operationId") long operationId,
                                              @RequestBody ElectoralAuthorityKeyChange change) {
    return service.saveOrUpdate(operationId, change.keyId);
  }

  /**
   * get the link from {@link ch.ge.ve.bo.repository.entity.ElectoralAuthorityKey} to operation
   */
  @GetMapping
  @PreAuthorize("hasRole('USER')")
  public ElectoralAuthorityKeyVo findForOperation(@PathVariable("operationId") long operationId) {
    return service.findForOperation(operationId).orElse(null);
  }


  public static class ElectoralAuthorityKeyChange {
    private long keyId;

    public void setKeyId(long keyId) {
      this.keyId = keyId;
    }
  }
}
