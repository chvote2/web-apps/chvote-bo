/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller.operation.parameters;

import ch.ge.ve.bo.service.operation.model.ElectionPagePropertiesModelVo;
import ch.ge.ve.bo.service.operation.parameters.election.ElectionPagePropertiesService;
import java.util.List;
import java.util.Map;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * controller used to defined election page properties
 */
@RestController
@RequestMapping("operation/{operationId}/election-page-properties")
public class ElectionPagePropertiesController {
  private final ElectionPagePropertiesService service;


  /**
   * Deefault constructor
   */
  public ElectionPagePropertiesController(ElectionPagePropertiesService electionPagePropertiesService) {
    this.service = electionPagePropertiesService;
  }

  /**
   * Return all available {@link ElectionPagePropertiesModelVo} for an operation
   */
  @GetMapping(path = "/model")
  @PreAuthorize("hasRole('USER')")
  public List<ElectionPagePropertiesModelVo> getModelsForOperation(@PathVariable("operationId") Long operationId) {
    return service.getModelsForOperation(operationId);
  }

  /**
   * Return a{@link ElectionPagePropertiesModelVo} by it's id
   */
  @GetMapping(path = "/model/{modelId}")
  @PreAuthorize("hasRole('USER')")
  public ElectionPagePropertiesModelVo getModelById(@PathVariable("modelId") Long modelId) {
    return service.getModel(modelId);
  }

  /**
   * save a {@link ElectionPagePropertiesModelVo} for an operation
   */
  @PostMapping(path = "/model")
  @PreAuthorize("hasRole('CREATE_ELECTION_PAGE_PROPERTIES_MODEL')")
  public ElectionPagePropertiesModelVo saveOrUpdateModel(
      @PathVariable("operationId") Long operationId, @RequestBody ElectionPagePropertiesModelVo model) {
    return service.saveOrUpdateModel(operationId, model);
  }

  /**
   * delete a {@link ElectionPagePropertiesModelVo} for an operation
   */
  @DeleteMapping(path = "/model/{modelId}")
  @PreAuthorize("hasRole('DELETE_ELECTION_PAGE_PROPERTIES_MODEL')")
  public void deleteModel(@PathVariable("modelId") long modelId) {
    service.deleteModel(modelId);
  }

  /**
   * get all mappings from {@link ElectionPagePropertiesModelVo} to ballot
   */
  @GetMapping(path = "/mapping")
  @PreAuthorize("hasRole('USER')")
  public Map<String, Long> getBallotToModelMapping(@PathVariable("operationId") Long operationId) {
    return service.getBallotToModelIdMapping(operationId);
  }

  /**
   * Map a {@link ElectionPagePropertiesModelVo} to a ballot
   */
  @PutMapping(path = "/mapping")
  @PreAuthorize("hasRole('MAP_ELECTION_PAGE_PROPERTIES_MODEL_TO_BALLOT')")
  public void associateModelToBallot(
      @PathVariable("operationId") Long operationId, @RequestBody BallotToModelMapping mapping) {
    service.associateModelToBallots(operationId, mapping.modelId, mapping.ballotIds);
  }

  /**
   * Remove a mapping between a {@link ElectionPagePropertiesModelVo} and a ballot
   */
  @DeleteMapping(path = "/mapping")
  @PreAuthorize("hasRole('MAP_ELECTION_PAGE_PROPERTIES_MODEL_TO_BALLOT')")
  public void removeMapping(@PathVariable("operationId") Long operationId, @RequestParam("ballotId") String ballot) {
    service.removeMapping(operationId, ballot);
  }


  private static class BallotToModelMapping {
    private long     modelId;
    private String[] ballotIds;

    public void setModelId(long modelId) {
      this.modelId = modelId;
    }

    public void setBallotIds(String[] ballotIds) {
      this.ballotIds = ballotIds;
    }
  }


}
