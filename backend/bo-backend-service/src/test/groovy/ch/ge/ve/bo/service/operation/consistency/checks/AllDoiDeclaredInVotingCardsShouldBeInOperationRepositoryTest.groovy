/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.consistency.checks

import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.repository.entity.CardType
import ch.ge.ve.bo.repository.entity.Signature
import ch.ge.ve.bo.service.model.VoterTestingCardsLotVo
import ch.ge.ve.bo.service.operation.consistency.AbstractConsistencyChecker
import ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceVo
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryService
import ch.ge.ve.bo.service.testing.card.VoterTestingCardsLotService
import spock.lang.Specification

class AllDoiDeclaredInVotingCardsShouldBeInOperationRepositoryTest extends Specification {
    def repositoryService = Mock(OperationRepositoryService)
    def voterTestingCardsLotService = Mock(VoterTestingCardsLotService)
    def check = new AllDoiDeclaredInVotingCardsShouldBeInOperationRepository(
            AbstractConsistencyChecker.CheckType.CONFIGURATION,
            repositoryService,
            voterTestingCardsLotService
    )

    def "CheckForOperation"() {
        given: "an operation"
        def operation = OperationDataset.electoralOperation()

        and: "A repository has been uploaded with one doi"
        repositoryService.getAllDomainOfInfluenceForOperation(operation.id) >> [
                new DomainOfInfluenceVo("CT", "known", "known doi", "known doi")
        ].toSet()

        and: "A testing card lot has been defined on absent doi"
        def lot = new VoterTestingCardsLotVo(1, "lotName", CardType.PRINTER_TESTING_CARD, 100,
                Signature.Ms, "firstName", "lastName", null, null, null,
                null, null, null, null, null, false,
                ["known", "unknown"])

        voterTestingCardsLotService.findAll(operation.id, true) >> [lot]


        when:
        def result = check.checkForOperation(operation)

        then:
        [ConsistencyErrorVo.ConsistencyErrorType.CONF_UNKNOWN_DOI_FOR_TESTING_CARD_LOT] == result.consistencyErrors.errorType
        "[[unknownDoi:unknown, lotName:lotName]]" == result.consistencyErrors.errorsParameters.toString()
    }


}
