/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.download.token

import ch.ge.ve.bo.repository.ConnectedUser
import ch.ge.ve.bo.repository.ConnectedUserTestUtils
import ch.ge.ve.bo.repository.DownloadTokenRepository
import ch.ge.ve.bo.repository.entity.DownloadToken
import ch.ge.ve.bo.repository.entity.Operation
import spock.lang.Specification

class DownloadTokenServiceImplTest extends Specification {
  def repository = Mock(DownloadTokenRepository)
  def service = new DownloadTokenServiceImpl(repository)

  void cleanup() {
    ConnectedUserTestUtils.disconnectUser()
  }

  def 'findByPrinterManagementEntity Should reuse token if already stored'() {
    given:
    def operation = new Operation()
    def path = "/test/path"
    ConnectedUserTestUtils.connectUser()

    DownloadToken token = new DownloadToken()
    repository.findByPathAndLogin(path, ConnectedUser.get().login) >> token

    when:
    def returnedToken = service.getOrCreateDownloadToken(operation, path)

    then:
    returnedToken == token
  }


  def 'findByPrinterManagementEntity Should create a new token if not present'() {
    given:
    def operation = new Operation()
    def path = "/test/path"
    ConnectedUserTestUtils.connectUser()

    when:
    def returnedToken = service.getOrCreateDownloadToken(operation, path)

    then:
    returnedToken.id.matches("[a-zA-Z0-9]+")
    returnedToken.operation == operation
    returnedToken.login == ConnectedUser.get().login
    returnedToken.path == path
  }


}
