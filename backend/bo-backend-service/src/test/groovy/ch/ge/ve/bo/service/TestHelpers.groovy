/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service

import ch.ge.ve.bo.repository.security.OperationHolder
import ch.ge.ve.bo.service.operation.SecuredOperationHolderService
import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Deserializer
import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Serializer
import ch.ge.ve.jacksonserializer.JSDates
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import com.google.common.io.CharStreams
import java.time.LocalDateTime
import java.util.function.Consumer
import java.util.function.Function
import java.util.function.Supplier
import java.util.regex.Matcher
import org.apache.commons.io.IOUtils
import org.springframework.util.StringUtils

class TestHelpers {
  static private InputStream stream


  static InputStream getResourceStream(Class<?> testClass, String filename) {
    String resourcePath = testClass.getName()
            .replaceAll('\\.[^.]+$', "")
            .replaceAll('\\.', Matcher.quoteReplacement(File.separator)) +
            File.separator +
            filename
    stream = testClass.getClassLoader().getResourceAsStream(resourcePath)
    if (stream == null) {
      throw new FileNotFoundException(resourcePath)
    }
    return stream
  };

  static String getResourceString(Class<?> testClass, String filename, boolean cleanCrlf = true) {
    String toReturn = IOUtils.toString(getResourceStream(testClass, filename), "UTF-8")
    return cleanCrlf ? toReturn.replaceAll("\r", "") : cleanCrlf
  }

  static byte[] getResourceBytes(String resourcePath) {
    return IOUtils.toByteArray(TestHelpers.getClassLoader().getResourceAsStream(resourcePath))
  }

  static String getResourceString(String resourcePath, boolean cleanCrlf = true) {
    String toReturn = IOUtils.toString(TestHelpers.getClassLoader().getResourceAsStream(resourcePath), "UTF-8")
    return cleanCrlf ? toReturn.replaceAll("\r", "") : cleanCrlf
  }

  static ObjectMapper getObjectMapper() {
    ObjectMapper objectMapper = new ObjectMapper()

    SimpleModule module = new SimpleModule()
    module.addSerializer(LocalDateTime.class, JSDates.SERIALIZER)
    module.addDeserializer(LocalDateTime.class, JSDates.DESERIALIZER)
    module.addSerializer(BigInteger.class, new BigIntegerAsBase64Serializer())
    module.addDeserializer(BigInteger.class, new BigIntegerAsBase64Deserializer())
    objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
    objectMapper.registerModule(module)
  }

  /**
   * Reads a public key from a classpath resource.
   *
   * @param sourcePath path to the public key resource
   *
   * @return the public key value
   */
  @SuppressWarnings("UnstableApiUsage")
  static BigInteger readPublicKeyFromClasspath(String sourcePath) {
    def reader = getResourceStream(TestHelpers,sourcePath).newReader("UTF-8")
    return new BigInteger(Base64.getDecoder().decode(CharStreams.toString(reader)))
  }

  /**
   * Not perfect method to parse a multipart request
   */
  static ParsedHttpRequestParts[] parseMultiPartRequest(String request) {
    return request.split("(\n|^)--[^\n]+\n")
            .findAll { !StringUtils.isEmpty(it) }
            .collect {
      def part = new ParsedHttpRequestParts()
      def headersAndBody = it.replaceAll("\\r\\n", "\n").split("\\n\\n")
      part.content = headersAndBody[1]
      headersAndBody[0].split("\\n")
              .collect { part.headers.put(it.split(":", 2)[0], it.split(":", 2)[1].trim()) }
      part
    }
  }

  /**
   * Provide a SecuredOperationHolderService that accept all operations
   */
  static SecuredOperationHolderService passThroughSecuredOperationHolderService() {
    return new SecuredOperationHolderService() {
      @Override
      <T extends OperationHolder> Optional<T> safeRead(Supplier<Optional<T>> operationHolderSupplier) {
        return operationHolderSupplier.get()
      }

      @Override
      <U extends OperationHolder> void doUpdateAndForget(Supplier<Optional<U>> operationHolderSupplier, Consumer<U> updater) {
        operationHolderSupplier.get().ifPresent(updater)
      }

      @Override
      <U extends OperationHolder> void doUpdateOperationHolderAndForget(U operationHolder, Consumer<U> updater) {
        updater.accept(operationHolder)
      }

      @Override
      <U extends OperationHolder, T extends Collection<U>> T safeReadAll(Supplier<T> operationHolderSupplier) {
        return operationHolderSupplier.get()
      }

      @Override
      <U extends OperationHolder> void doNotOptionalUpdateAndForget(Supplier<U> operationHolderSupplier, Consumer<U> updater) {
        updater.accept(operationHolderSupplier.get())
      }

      @Override
      <U extends OperationHolder, T> Optional<T> doTechnicalUpdate(Supplier<Optional<U>> operationHolderSupplier, Function<U, T> updater) {
        return operationHolderSupplier.get().map(updater)
      }

      @Override
      <U extends OperationHolder, T> T doTechnicalNotOptionalUpdate(Supplier<U> operationHolderSupplier, Function<U, T> updater) {
        return updater.apply(operationHolderSupplier.get())
      }

      @Override
      <U extends OperationHolder> void doTechnicalUpdateAndForget(Supplier<Optional<U>> operationHolderSupplier, Consumer<U> updater) {
        operationHolderSupplier.get().ifPresent(updater)
      }

      @Override
      <U extends OperationHolder> void doTechnicalNotOptionalUpdateAndForget(Supplier<U> operationHolderSupplier, Consumer<U> updater) {
        updater.accept(operationHolderSupplier.get())
      }

      @Override
      <U extends OperationHolder, T> Optional<T> doUpdate(Supplier<Optional<U>> operationHolderSupplier, Function<U, T> updater) {
        return operationHolderSupplier.get().map(updater)
      }

      @Override
      <U extends OperationHolder, T> T doUpdateOperationHolder(U operationHolder, Function<U, T> updater) {
        return  updater.apply(operationHolder)
      }

      @Override
      <U extends OperationHolder, T> T doNotOptionalUpdate(Supplier<U> operationHolderSupplier, Function<U, T> updater) {
        return updater.apply(operationHolderSupplier.get())
      }
    }
  }


}
