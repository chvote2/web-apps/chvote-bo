/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation

import ch.ge.ve.bo.repository.ConnectedUserTestUtils
import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.repository.entity.DeploymentTarget
import ch.ge.ve.bo.repository.entity.Operation
import ch.ge.ve.bo.repository.entity.OperationParameterType
import ch.ge.ve.bo.repository.security.ManagementEntityHolder
import ch.ge.ve.bo.repository.security.OperationHolder
import ch.ge.ve.bo.repository.security.SecuredContext
import ch.ge.ve.bo.service.exception.TechnicalException
import ch.ge.ve.bo.service.operation.consistency.ConsistencyScheduleService
import ch.ge.ve.bo.service.operation.model.ConfigurationStatusVo
import ch.ge.ve.bo.service.operation.model.OperationStatusVo
import ch.ge.ve.bo.service.operation.model.TallyArchiveStatusVo
import ch.ge.ve.bo.service.operation.model.VotingMaterialStatusVo
import ch.ge.ve.bo.service.operation.status.OperationStatusService
import java.time.LocalDateTime
import java.util.function.Function
import spock.lang.Specification

class SecuredOperationHolderServiceImplTest extends Specification {
  static OperationStatusVo READ_WRITE_STATUS = new OperationStatusVo(
          new ConfigurationStatusVo.Builder()
                  .setState(ConfigurationStatusVo.State.INCOMPLETE, LocalDateTime.now(), "user")
                  .setCompletedSections(["test": false])
                  .build(),
          new VotingMaterialStatusVo(
                  VotingMaterialStatusVo.State.CREATION_FAILED, LocalDateTime.now(), ["test": false], null),
          null, TallyArchiveStatusVo.notRequested(), null, DeploymentTarget.NOT_DEFINED)

  static final OperationStatusVo READ_ONLY_STATUS = new OperationStatusVo(
          new ConfigurationStatusVo.Builder()
                  .setState(ConfigurationStatusVo.State.TEST_SITE_IN_DEPLOYMENT, LocalDateTime.now(), "user")
                  .setCompletedSections([:])
                  .build(),
          null, null, TallyArchiveStatusVo.notRequested(), null, DeploymentTarget.NOT_DEFINED)


  def operationUnderTest = new Operation()
  ConsistencyScheduleService consistencyScheduleService = Mock(ConsistencyScheduleService)
  SecuredOperationRepository operationRepository = Mock(SecuredOperationRepository)
  OperationStatusService operationStatusService = Mock(OperationStatusService)
  SecuredContext securedContext = new SecuredContext()
  def service = new SecuredOperationHolderServiceImpl(consistencyScheduleService, operationRepository, securedContext, operationStatusService)


  class StupidUpdater<U> implements Function<U, String> {
    def hasBeenCalled = false

    @Override
    String apply(U u) {
      hasBeenCalled = true
      return "ok"
    }
  }


  void setup() {
    operationUnderTest.id = 1
    operationUnderTest.managementEntity == "ge"
  }

  void cleanup() {
    ConnectedUserTestUtils.disconnectUser()
  }

  def "Read only is no restricted to operation management entity (every guest Management entity can read)"() {
    when:
    service.safeRead({ Optional.of(operationHolder(onlyOperationManagementEntityCanCreate)()) })

    then:
    1 * operationRepository.findOne(1, false)

    where:
    _ | onlyOperationManagementEntityCanCreate
    _ | true
    _ | false
  }

  def "safe read return null without any control if operationHolder is not retrieved - ie: is null"() {
    when:
    def value = service.safeRead({Optional.empty()})

    then:
    !value.isPresent()
  }


  def "SafeReadAll"() {
    when:
    service.safeReadAll({ [operationHolder(true).call()] })

    then:
    1 * operationRepository.findOne(1, false)
  }

  def "doTechnicalUpdate should update operation even if status is in read only but should not mark operation as updated"() {
    given:
    operationStatusService.isConfigurationInReadonly(1) >> true

    when:
    def objectUnderModification = operationHolder(true)
    def value = service.doTechnicalNotOptionalUpdate(objectUnderModification, new StupidUpdater())

    then:
    0 * operationRepository.markConfigurationAsUpdated(1, _)
    "ok" == value
  }


  def "If object has no management entity, write restriction is given only by the object under modification"() {
    given:
    operationStatusService.getStatus(1) >> READ_WRITE_STATUS

    when:
    def objectUnderModification = operationHolder(onlyOperationManagementEntityCanCreate)
    def value = service.doNotOptionalUpdate(objectUnderModification, new StupidUpdater())

    then:
    1 * operationRepository.findOne(1, findOneIsRestricted)
    1 * operationRepository.markConfigurationAsUpdated(1, _)
    "ok" == value

    where:
    onlyOperationManagementEntityCanCreate | findOneIsRestricted
    true                                   | true
    false                                  | false
  }

  def "if the status of the configuration is in readonly update cannot be done"() {
    given:
    operationStatusService.getStatus(1) >> READ_ONLY_STATUS
    def updater = new StupidUpdater()

    when:
    def objectUnderModification = operationHolder(true)
    service.doNotOptionalUpdate(objectUnderModification, updater)

    then:
    0 * operationRepository.markConfigurationAsUpdated(1, _)
    !updater.hasBeenCalled
    def te = thrown TechnicalException
    "try to modify a read only property" == te.message
  }

  def "if the status of the voting material is in readonly update cannot be done"() {
    given:
    operationStatusService.getStatus(1) >> READ_ONLY_STATUS
    def updater = new StupidUpdater()

    when:
    def objectUnderModification = operationHolder(true, OperationParameterType.VOTING_MATERIAL)
    service.doNotOptionalUpdateAndForget(objectUnderModification, { updater.apply(it) })

    then:
    0 * operationRepository.markVotingMaterialAsUpdated(1, _)
    !updater.hasBeenCalled
    def te = thrown TechnicalException
    "try to modify a read only property" == te.message
  }

  def "if the status of the voting period is in readonly update cannot be done"() {
    given:
    operationStatusService.getStatus(1) >> READ_ONLY_STATUS
    def updater = new StupidUpdater()

    when:
    def objectUnderModification = operationHolder(true, OperationParameterType.VOTING_PERIOD)
    service.doNotOptionalUpdateAndForget(objectUnderModification, { updater.apply(it) })

    then:
    0 * operationRepository.markVotingMaterialAsUpdated(1, _)
    !updater.hasBeenCalled
    def te = thrown TechnicalException
    "try to modify a read only property" == te.message
  }


  def "if object has management entity, user belonging to this management entity can still write it"() {
    given:
    operationStatusService.getStatus(1) >> READ_WRITE_STATUS

    when:
    ConnectedUserTestUtils.connectUser("user", "ge", userManagementEntity)
    def objectUnderModification = managementEntityHolder("objectManagementEntity")
    def value = service.doNotOptionalUpdate({ objectUnderModification }, new StupidUpdater())

    then:
    1 * operationRepository.findOne(1, findOneIsRestrictedToOperationManagementEntity)
    1 * operationRepository.markVotingMaterialAsUpdated(1, _)
    "ok" == value

    where:
    userManagementEntity     | findOneIsRestrictedToOperationManagementEntity
    "objectManagementEntity" | false
    "other"                  | true
  }

  ManagementEntityHolder managementEntityHolder(String managementEntity) {
    return new ManagementEntityHolder() {
      @Override
      String getManagementEntity() {
        return managementEntity
      }

      @Override
      Operation getOperation() {
        return operationUnderTest
      }

      @Override
      boolean onlyManagementEntityInChargeCanCreate() {
        return true
      }

      @Override
      OperationParameterType getOperationParameterType() {
        return OperationParameterType.VOTING_MATERIAL
      }
    }
  }

  def operationHolder(boolean onlyOperationManagementEntityCanCreate,
                      OperationParameterType operationParameterType = OperationParameterType.CONFIGURATION) {
    return {
      new OperationHolder() {
        Operation getOperation() {
          return operationUnderTest
        }

        boolean onlyManagementEntityInChargeCanCreate() {
          return onlyOperationManagementEntityCanCreate
        }

        OperationParameterType getOperationParameterType() {
          return operationParameterType
        }
      }
    }

  }

}
