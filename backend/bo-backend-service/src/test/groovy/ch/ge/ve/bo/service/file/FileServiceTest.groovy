/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.file

import static ch.ge.ve.bo.repository.dataset.FileDataset.Options
import static ch.ge.ve.bo.repository.dataset.FileDataset.Options.getFileType
import static ch.ge.ve.bo.repository.dataset.FileDataset.Options.getOperationId
import static ch.ge.ve.bo.repository.dataset.FileDataset.file
import static ch.ge.ve.bo.repository.dataset.OperationDataset.votingOperation
import static ch.ge.ve.bo.service.dataset.FileVoDataset.votationOperationRepositoryVo

import ch.ge.ve.bo.FileType
import ch.ge.ve.bo.Language
import ch.ge.ve.bo.repository.ConnectedUserTestUtils
import ch.ge.ve.bo.repository.FileRepository
import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.repository.dataset.FileDataset
import ch.ge.ve.bo.service.TestHelpers
import ch.ge.ve.bo.service.file.exception.FileAlreadyImportedException
import ch.ge.ve.bo.service.file.exception.InvalidFileException
import spock.lang.Specification

/**
 * Unit tests for the application file services ({@link FileService}).
 */
class FileServiceTest extends Specification {
  def fileRepositoryMock = Mock(FileRepository)
  def operationRepositoryMock = Mock(SecuredOperationRepository)

  def fileService = new FileServiceImpl(fileRepositoryMock, operationRepositoryMock, TestHelpers.passThroughSecuredOperationHolderService())

  void setup() {
    ConnectedUserTestUtils.connectUserWithAllRoles()
  }

  void cleanup() {
    ConnectedUserTestUtils.disconnectUser()
  }

  def "business key should be composed of realm, sender ID and message ID"() {
    when: 'the business key is requested'
    def key = fileService.buildApplicationBusinessKey('vota3://all', '6c6f4b14b6d349ef97703f2af84ffbc6')

    then: 'all the elements of the key are created'
    key == 'GE|vota3://all|6c6f4b14b6d349ef97703f2af84ffbc6'
  }


  def "business key without sender ID should raise an error"() {
    when: 'the business key is requested'
    fileService.buildApplicationBusinessKey(null, '6c6f4b14b6d349ef97703f2af84ffbc6')

    then: 'an error should be raised'
    thrown(InvalidFileException)
  }

  def "business key without message ID should raise an error"() {
    when: 'the business key is requested'
    fileService.buildApplicationBusinessKey('vota3://all', null)

    then: 'an error should be raised'
    thrown(InvalidFileException)
  }

  def "business key for document should be composed of the ream the contest ID, sender ID and message ID"() {
    when: 'the business key is requested'
    def key = fileService.buildDocumentBusinessKey(1234, FileType.DOCUMENT_FAQ, Language.FR)

    then: 'all the elements of the key are created'
    key == 'GE' + '|' + '1234' + '|' + 'DOCUMENT_FAQ' + '|' + 'FR'
  }

  def "business key for document without operation ID should raise an error"() {
    when: 'the business key is requested'
    fileService.buildDocumentBusinessKey(0, FileType.DOCUMENT_FAQ, Language.FR)

    then: 'an error should be raised'
    thrown(IllegalArgumentException)
  }

  def "business key for document with fileType null should raise an error"() {
    when: 'the business key is requested'
    fileService.buildDocumentBusinessKey(1234, null, Language.FR)

    then: 'an error should be raised'
    thrown(IllegalArgumentException)
  }

  def "business key for document with language null should raise an error"() {
    when: 'the business key is requested'
    fileService.buildDocumentBusinessKey(1234, FileType.DOCUMENT_FAQ, null)

    then: 'an error should be raised'
    thrown(IllegalArgumentException)
  }

  def "force import without file already in database should store the application file and return its ID"() {
    given: 'an application file'
    def fileVo = votationOperationRepositoryVo()

    when: 'calling the importFile service'
    def applicationFileId = fileService.importFile(fileVo, true)

    then: 'the file should be saved in the repository'
    1 * fileRepositoryMock.findByBusinessKey('202012SGAvota3://all6c6f4b14b6d349ef97703f2af84ffbc6') >> Optional.empty()
    1 * operationRepositoryMock.findOne(2, false) >> votingOperation()
    0 * fileRepositoryMock.delete(_)
    1 * fileRepositoryMock.save(_) >> file()

    and: 'the file\'s ID should be returned'
    applicationFileId.businessKey == votationOperationRepositoryVo().businessKey
  }

  def "force importFile with file already in database for the same operation should overwrite"() {
    given: 'an application file'
    def fileVo = votationOperationRepositoryVo(new Options().with(operationId, 2))
    fileRepositoryMock.findByBusinessKey('202012SGAvota3://all6c6f4b14b6d349ef97703f2af84ffbc6') >>
            Optional.of(file(new Options().with(operationId, 2)))
    operationRepositoryMock.findOne(2, false) >> votingOperation()

    when: 'calling the importFile service'
    def applicationFileId = fileService.importFile(fileVo, true)

    then: 'the file should be saved in the repository'
    1 * fileRepositoryMock.delete(_)
    1 * fileRepositoryMock.save(_) >> file()

    and: 'the file\'s ID should be returned'
    applicationFileId.businessKey == votationOperationRepositoryVo().businessKey
  }

  def "force importFile with file already in database for a different operation should throw exception"() {
    given: 'an application file'
    def fileVo = votationOperationRepositoryVo()
    fileRepositoryMock.findByBusinessKey('202012SGAvota3://all6c6f4b14b6d349ef97703f2af84ffbc6') >>
            Optional.of(file(new Options().with(operationId, 50)))
    operationRepositoryMock.findOne(2) >> votingOperation()

    when: 'calling the importFile service'
    def applicationFileId = fileService.importFile(fileVo, true)

    then:
    0 * fileRepositoryMock.delete(_)
    0 * fileRepositoryMock.save(_) >> file()
    thrown(FileAlreadyImportedException.class)
  }


  def "importFile should store the application file"() {
    given: 'an application file'
    def fileVo = votationOperationRepositoryVo(new Options().with(fileType, fType))
    operationRepositoryMock.findOne(2, false) >> votingOperation()
    fileRepositoryMock.findByBusinessKey(_ as String) >> Optional.empty()


    when: 'calling the importFile service'
    def applicationFileId = fileService.importFile(fileVo, false)

    then: 'the file should be saved in the repository'

    1 * fileRepositoryMock.save(_) >> file()

    and: 'the file\'s ID should be returned'
    applicationFileId.businessKey == votationOperationRepositoryVo().businessKey

    where:
    fType                        | _
    FileType.REGISTER            | _
    FileType.VOTATION_REPOSITORY | _
  }


  def 'expect FileAlreadyImportedException when importing a file already imported'() {
    given: 'a repository already imported'
    fileRepositoryMock.findByBusinessKey('202012SGAvota3://all6c6f4b14b6d349ef97703f2af84ffbc6') >>
            Optional.of(file())

    when: 'calling the import service'
    fileService.importFile(votationOperationRepositoryVo(), false)

    then: 'an error should be raised'
    thrown(FileAlreadyImportedException.class)
  }

  def "getFiles(operationId, type) should return the corresponding files"() {
    when:
    def files = fileService.getFiles(1L, false, Collections.singleton(FileType.VOTATION_REPOSITORY))

    then:
    1 * fileRepositoryMock.findByOperationIdAndTypeIn(1l, Collections.singleton(FileType.VOTATION_REPOSITORY)) >> [file()]
    files.collect {it.businessKey} == [votationOperationRepositoryVo().businessKey]
  }


  def "getFiles(operationId, type, null) should return the corresponding files"() {
    when:
    def files = fileService.getFiles(1L, FileType.VOTATION_REPOSITORY, null)

    then:
    1 * fileRepositoryMock.findByOperationIdAndType(1L, FileType.VOTATION_REPOSITORY) >> [file()]
    files.collect {it.businessKey} == [votationOperationRepositoryVo().businessKey]
  }

  def "getFiles(operationId, type, language) should return the corresponding files"() {
    when:
    def files = fileService.getFiles(1L, FileType.VOTATION_REPOSITORY, Language.FR)

    then:
    1 * fileRepositoryMock.findByOperationIdAndTypeAndLanguage(1L, FileType.VOTATION_REPOSITORY, Language.FR) >> [file()]
    files.collect {it.businessKey} == [votationOperationRepositoryVo().businessKey]
  }

  def "getFile should return the seeked file"() {
    when:
    def file = fileService.getFile(1L)

    then:
    1 * fileRepositoryMock.findById(1L) >> Optional.of(FileDataset.file())
    file.businessKey == votationOperationRepositoryVo().businessKey
  }

  def "deleteRepositoryFile should call the correct repository's method"() {
    given:
    def file = file()
    fileRepositoryMock.findById(1L) >> Optional.of(file)

    when:
    fileService.deleteFile(1L)

    then:
    1 * fileRepositoryMock.delete(file)
  }


}
