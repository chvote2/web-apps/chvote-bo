/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.repository

import static ch.ge.ve.bo.FileType.ELECTION_REPOSITORY
import static ch.ge.ve.bo.FileType.VOTATION_REPOSITORY
import static ch.ge.ve.bo.repository.dataset.DatasetTools.localDateTime
import static ch.ge.ve.bo.repository.dataset.FileDataset.Options
import static ch.ge.ve.bo.repository.dataset.OperationDataset.electoralOperation
import static ch.ge.ve.bo.repository.dataset.OperationDataset.votingOperation
import static ch.ge.ve.bo.service.TestHelpers.getResourceBytes
import static ch.ge.ve.bo.service.dataset.FileVoDataset.electionOperationRepositoryVo
import static ch.ge.ve.bo.service.dataset.FileVoDataset.votationOperationRepositoryVo

import ch.ge.ve.bo.FileType
import ch.ge.ve.bo.repository.ConnectedUser
import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.service.file.FileService
import ch.ge.ve.bo.service.file.model.FileVo
import ch.ge.ve.bo.service.operation.parameters.doi.OperationDomainInfluenceService
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceFileVo
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceVo
import ch.ge.ve.bo.service.operation.parameters.repository.exception.OperationRepositoryDateMismatchException
import ch.ge.ve.bo.service.operation.parameters.repository.model.VotationRepositoryFileVo
import ch.ge.ve.bo.service.utils.xml.XSDValidator
import ch.ge.ve.interfaces.ech.eCH0157.v4.Delivery
import ch.ge.ve.interfaces.ech.service.JAXBEchCodecImpl
import java.time.LocalDateTime
import org.apache.commons.io.IOUtils
import spock.lang.Specification

class OperationRepositoryServiceImplTest extends Specification {

  public static final Options withContent = new Options().with(Options.withoutContent, false)
  public static final EnumSet<FileType> REPO_FILE_TYPES =
          EnumSet.of(ELECTION_REPOSITORY, VOTATION_REPOSITORY)


  def operationRepositoryMock = Mock(SecuredOperationRepository)
  def fileServiceMock = Mock(FileService)
  def operationDomainInfluenceServiceMock = Mock(OperationDomainInfluenceService)
  def votationValidator = XSDValidator.of("xsd/eCH-0159-4-0.xsd", "catalog.cat")
  def electionValidator = XSDValidator.of("xsd/eCH-0157-4-0.xsd", "catalog.cat")

  def votationHandler = new OperationRepositoryServiceVotationHandler(votationValidator, {
    new JAXBEchCodecImpl<>(ch.ge.ve.interfaces.ech.eCH0159.v4.Delivery).deserialize(it, false)
  }, fileServiceMock)
  def electionHandler = new OperationRepositoryServiceElectionHandler(electionValidator, {
    new JAXBEchCodecImpl<>(Delivery).deserialize(it, false)
  }, fileServiceMock)


  def operationRepositoryService = new OperationRepositoryServiceImpl(
          operationRepositoryMock,
          operationDomainInfluenceServiceMock,
          fileServiceMock,
          votationHandler,
          electionHandler)


  def setupSpec() {
    ConnectedUser.technical()
  }

  def "getAllDomainOfInfluenceForOperation should return all known doi"() {
    given:

    operationDomainInfluenceServiceMock.getFile(1, false) >>
            new DomainOfInfluenceFileVo(2, "", LocalDateTime.now(), "businessKey", 1,
                    FileType.DOMAIN_OF_INFLUENCE, null, "fileName", "Canton de Genève", null,
                    Collections.singletonList(new DomainOfInfluenceVo("type", "6624", "test", "test")))

    fileServiceMock.getFiles(1, true, REPO_FILE_TYPES) >>
            Collections.singletonList(
                    new FileVo("fileName", VOTATION_REPOSITORY, "Gy", IOUtils.toByteArray(this.getClass().getResourceAsStream
                            ("/repositoryVotation.xml")))
            )

    when:
    def doi = operationRepositoryService.getAllDomainOfInfluenceForOperation(1)

    then:
    1 == doi.size()
    "test" == doi[0].name
  }


  def "getAllBallotForOperation should return all ballot"() {
    given:
    fileServiceMock.getFiles(1, true, REPO_FILE_TYPES) >>
            [new FileVo("ele", ELECTION_REPOSITORY, "Gy", getResourceBytes("repositoryElection.xml")),
             new FileVo("vot", VOTATION_REPOSITORY, "Gy", getResourceBytes("repositoryVotation.xml"))]

    when:
    def opeBallots = operationRepositoryService.getAllBallotsForOperation(1, false, true)
    def elecBallots = operationRepositoryService.getAllBallotsForOperation(1, true, false)
    def allBallots = operationRepositoryService.getAllBallotsForOperation(1, true, true)

    then:
    ["202012SGA-COM-6624"] as Set == opeBallots
    ["Conseil municipal 12 juin 2057 - Aire-la-Ville", "Conseil municipal 12 juin 2057 - Anières"] as Set == elecBallots
    ["Conseil municipal 12 juin 2057 - Aire-la-Ville", "Conseil municipal 12 juin 2057 - Anières", "202012SGA-COM-6624"] as Set == allBallots
  }


  def "getAllDomainOfInfluenceForOperation should create virtual unknown doi return all known doi"() {
    given:
    fileServiceMock.getFiles(1, true, REPO_FILE_TYPES) >>
            [new FileVo("ele", ELECTION_REPOSITORY, "Gy", getResourceBytes("repositoryElection.xml")),
             new FileVo("vot", VOTATION_REPOSITORY, "Gy", getResourceBytes("repositoryVotation.xml"))]

    when:
    def doi = operationRepositoryService.getAllDomainOfInfluenceForOperation(1)

    then:
    ["Unknown id=6602", "Unknown id=6624", "Unknown id=6601"] as Set == doi.collect { it.name } as Set
  }


  def "import a repository votation should save the file in database"() {
    given: 'a valid xml input stream and a file name'
    def fileName = "/repositoryVotation.xml"
    InputStream xmlFileStream = this.getClass().getResourceAsStream(fileName)

    def operation = votingOperation()
    operation.setDate(localDateTime('01.12.2020 00:00:00'))

    when: 'calling repository import'
    def importResult = operationRepositoryService.importFile(1, fileName, xmlFileStream)

    then: 'corresponding services are called'
    1 * operationRepositoryMock.findOne(1, false) >> operation
    1 * fileServiceMock.importFile(_, false) >> votationOperationRepositoryVo(new Options().with(Options.withoutBusinessKey, true))

    and: 'no error is detected'
    importResult.validationErrors.size() == 0

    and: 'the imported file information is returned'
    importResult.repositoryFile.contestIdentification == "202012SGA"
    importResult.repositoryFile.fileContent == null
  }

  def "import a incpqs repository votation should save the file in database"() {
    given: 'a valid xml input stream and a file name'
    def fileName = "/repositoryVotation-incpqs.xml"
    InputStream xmlFileStream = this.getClass().getResourceAsStream(fileName)

    def operation = votingOperation()
    operation.setDate(localDateTime('12.06.2057 00:00:00'))

    when: 'calling repository import'
    def importResult = operationRepositoryService.importFile(1, fileName, xmlFileStream)
    def repositoryFile = (VotationRepositoryFileVo) importResult.repositoryFile

    then: 'corresponding services are called'
    1 * operationRepositoryMock.findOne(1, false) >> operation
    1 * fileServiceMock.importFile(_, false) >> votationOperationRepositoryVo(new Options().with(Options.withoutBusinessKey, true))

    and: 'no error is detected'
    importResult.validationErrors.size() == 0

    and: 'the imported file information is returned'
    repositoryFile.contestIdentification == "201903VP"
    ["FED_CH_Q1", "CAN_GE_Q1", "COM_6608_Q1.a", "COM_6608_Q1.b", "COM_6608_Q1.c"] == repositoryFile.details.ballotIdentification
    ["parameters.repository.edit.detail-grid.answer-type-string.2",
     "parameters.repository.edit.detail-grid.answer-type-string.2",
     "parameters.repository.edit.detail-grid.answer-type-string.2",
     "parameters.repository.edit.detail-grid.answer-type-string.2",
     "parameters.repository.edit.detail-grid.answer-type-string.5"] == repositoryFile.details.acceptedAnswers
    [1, 1, 1, 2, 3] == repositoryFile.details.ballotPosition
    ["CH", "GE", "6608", "6608", "6608"] == repositoryFile.details.domainOfInfluence
    ["201903VP-FED-CH", "201903VP-CAN-GE", "201903VP-COM-6608", "201903VP-COM-6608", "201903VP-COM-6608"] == repositoryFile.details.voteIdentification
    importResult.repositoryFile.fileContent == null
  }


  def "import a repository election should save the file in database"() {
    given: 'a valid xml input stream and a file name'
    def fileName = "/repositoryElection.xml"
    InputStream xmlFileStream = this.getClass().getResourceAsStream(fileName)

    def operation = electoralOperation()
    operation.setDate(localDateTime('12.06.2057 00:00:00'))

    when: 'calling repository import'
    def importResult = operationRepositoryService.importFile(1, fileName, xmlFileStream)

    then: 'corresponding services are called'
    1 * operationRepositoryMock.findOne(1, false) >> operation
    1 * fileServiceMock.importFile(_, false) >> electionOperationRepositoryVo(new Options().with(Options.withoutBusinessKey, true))

    and: 'no error is detected'
    importResult.validationErrors.size() == 0

    and: 'the imported file information is returned'
    importResult.repositoryFile.contestIdentification == "205706EC"
    importResult.repositoryFile.fileContent == null
  }

  def 'importing repository with wrong operation year should throw OperationRepositoryDateMismatchException'() {
    given:
    def fileName = "/repositoryVotation.xml"
    InputStream xmlFileStream = this.getClass().getResourceAsStream(fileName)

    def operation = votingOperation()
    operation.setDate(localDateTime('01.12.2018 00:00:00'))
    operationRepositoryMock.findOne(1, false) >> operation

    when: 'calling the import service'
    operationRepositoryService.importFile(1, fileName, xmlFileStream)

    then: 'OperationRepositoryDateMismatchException should be thrown'
    thrown(OperationRepositoryDateMismatchException.class)
  }

  def 'importing repository with wrong operation month should throw OperationRepositoryDateMismatchException'() {
    given:
    def fileName = "/repositoryVotation.xml"
    InputStream xmlFileStream = this.getClass().getResourceAsStream(fileName)

    def operation = votingOperation()
    operation.setDate(localDateTime('01.11.2020 00:00:00'))
    operationRepositoryMock.findOne(1, false) >> operation

    when: 'calling the import service'
    operationRepositoryService.importFile(1, fileName, xmlFileStream)

    then: 'OperationRepositoryDateMismatchException should be thrown'
    thrown(OperationRepositoryDateMismatchException.class)
  }

  def 'importing repository with wrong operation day should throw OperationRepositoryDateMismatchException'() {
    given:
    def fileName = "/repositoryVotation.xml"
    InputStream xmlFileStream = this.getClass().getResourceAsStream(fileName)

    def operation = votingOperation()
    operation.setDate(localDateTime('21.12.2020 00:00:00'))
    operationRepositoryMock.findOne(1, false) >> operation

    when: 'calling the import service'
    operationRepositoryService.importFile(1, fileName, xmlFileStream)

    then: 'OperationRepositoryDateMismatchException should be thrown'
    thrown(OperationRepositoryDateMismatchException.class)
  }


  def "validation errors while importing should be reported"() {
    given: 'an invalid xml input stream'
    def fileName = "/invalidRepository.xml"
    InputStream xmlFileStream = this.getClass().getResourceAsStream(fileName)

    when: 'calling repository import'
    def importResult = operationRepositoryService.importFile(1, fileName, xmlFileStream)

    then: 'the imported file information is empty'
    importResult.repositoryFile == null

    and: 'errors are returned'
    importResult.validationErrors.size() == 1
    importResult.validationErrors[0].message == 'cvc-complex-type.2.4.a: Invalid content was found starting with element \'ns1:contestDescription\'. One of \'{"http://www.ech.ch/xmlns/eCH-0155/4":language}\' is expected.'
  }


  def 'getFiles should return the repository files linked to the given operation'() {
    when: 'calling the getFiles service'
    def files = operationRepositoryService.getFiles(1L)

    then: 'corresponding repository files are returned'
    1 * fileServiceMock.getFiles(1L, true, REPO_FILE_TYPES) >> [votationOperationRepositoryVo(withContent)]
    1 == files.size()
    files[0].contestIdentification == "202012SGA"
  }

  def "getFile should return the full repository file with its content"() {
    when:
    def repositoryFile = operationRepositoryService.getFile(5L)

    then:
    1 * fileServiceMock.getFile(5L) >> votationOperationRepositoryVo(withContent)
    repositoryFile.contestIdentification == "202012SGA"
    repositoryFile.fileContent != null
  }

}
