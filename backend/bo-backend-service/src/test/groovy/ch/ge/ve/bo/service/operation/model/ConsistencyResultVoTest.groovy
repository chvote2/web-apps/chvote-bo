/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model

import static ch.ge.ve.bo.service.TestHelpers.objectMapper

import org.skyscreamer.jsonassert.JSONAssert
import spock.lang.Specification

class ConsistencyResultVoTest extends Specification {

    def "Should support json marshalling and unmarshalling"() {
        given:
        def results = new ConsistencyResultVo()
        results.addError(ConsistencyErrorVo.ConsistencyErrorType.VM_NO_PRINTER_ASSOCIATED_TO_MUNICIPALITY,
                ConsistencyResultVo.param("key1", "param1"),
                ConsistencyResultVo.param("key2", "param2"))

        when:
        def resultAsJson = objectMapper
                .writeValueAsString(objectMapper
                .readValue(objectMapper
                .writeValueAsString(results), ConsistencyResultVo))

        then:
        JSONAssert.assertEquals(resultAsJson, """
{
  "consistencyErrors":[
   {
     "errorType":"VM_NO_PRINTER_ASSOCIATED_TO_MUNICIPALITY",
     "errorsParameters":{"key1":"param1","key2":"param2"},
     "scope":"VOTING_MATERIAL",
     "errorKey":"consistency.votingMaterial.error.noPrinterAssociatedToMunicipality"
   }
  ]
}
""", true)
    }

}

