/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.document

import static ch.ge.ve.bo.repository.dataset.FileDataset.Options
import static ch.ge.ve.bo.service.dataset.FileVoDataset.doiApplicationFileVo
import static ch.ge.ve.bo.service.dataset.FileVoDataset.pdfDocumentApplicationFileVo
import static ch.ge.ve.bo.service.dataset.FileVoDataset.termsDocumentFileVo

import ch.ge.ve.bo.FileType
import ch.ge.ve.bo.Language
import ch.ge.ve.bo.repository.ConnectedUser
import ch.ge.ve.bo.service.exception.TechnicalException
import ch.ge.ve.bo.service.file.FileService
import ch.ge.ve.bo.service.file.exception.InvalidFileException
import spock.lang.Specification

class OperationDocumentServiceTest extends Specification {
    def withoutContent = new Options().with(Options.withoutContent, true)
    def withContent = new Options().with(Options.withoutContent, false)

    def fileServiceMock = Mock(FileService)

    def operationDocumentService = new OperationDocumentServiceImpl(fileServiceMock)

    def setupSpec() {
        Locale.setDefault(Locale.ENGLISH)
        ConnectedUser.technical()
    }

    def "import a document should save the file in database"() {
        given: 'a valid document input stream and file name'
        def fileName = "/example.pdf"
        InputStream pdfFileIS = this.getClass().getResourceAsStream(fileName)

        when: 'calling import service'
        def importedFile = operationDocumentService.importFile(1, FileType.DOCUMENT_TERMS, Language.DE, fileName, pdfFileIS)

        then: 'corresponding services are called'
        1 * fileServiceMock.importFile(_, false) >> termsDocumentFileVo()

        and: 'the imported file information is returned'
        importedFile.businessKey == termsDocumentFileVo().businessKey
    }

    def "import a document which is not a PDF file should raise an exception"() {
        when: 'calling import service'
        operationDocumentService.importFile(1, FileType.DOCUMENT_TERMS, Language.DE, "document.xml", null)

        then: 'an InvalidFileException should be thrown'
        0 * fileServiceMock.importFile(_, false)
        thrown(InvalidFileException)
    }

    def "expect IllegalArgumentException when importing document file with invalid file type"() {
        given: 'a valid document input stream and file name'
        def fileName = "/example.pdf"
        InputStream xmlFileIS = this.getClass().getResourceAsStream(fileName)

        when: 'calling import service'
        operationDocumentService.importFile(1, FileType.DOMAIN_OF_INFLUENCE, Language.DE, fileName, xmlFileIS)

        then:
        thrown(IllegalArgumentException)
    }

    def "an IOException should be wrapped in a TechnicalException"() {
        given: 'a valid document input stream and file name'
        def fileName = "/example.pdf"
        InputStream xmlFileIS = this.getClass().getResourceAsStream(fileName)
        xmlFileIS.close()

        when: 'calling import service'
        operationDocumentService.importFile(1, FileType.DOCUMENT_TERMS, Language.DE, fileName, xmlFileIS)

        then:
        thrown(TechnicalException)
    }

    def "getFile(id) should return the document file with content if present"() {
        when:
        def fileVo = operationDocumentService.getFile(5L)

        then:
        1 * fileServiceMock.getFile(5L) >> termsDocumentFileVo(withContent)
        fileVo.businessKey == termsDocumentFileVo(withContent).businessKey
    }

    def "getFiles(operationId) should return the document file without content if present"() {
        given :
        when:
        def fileVos = operationDocumentService.getFiles(5L)

        then:
        1 * fileServiceMock.getFiles(5L, false, [FileType.DOCUMENT_FAQ, FileType.DOCUMENT_TERMS, FileType.DOCUMENT_CERTIFICATE]) >>
                [termsDocumentFileVo(withContent),                        pdfDocumentApplicationFileVo(withContent)]
        fileVos.businessKey == [termsDocumentFileVo(withoutContent), pdfDocumentApplicationFileVo(withoutContent)].businessKey
    }

    def "getFiles(operationId) should return only document files"() {
        when:
        def fileVos = operationDocumentService.getFiles(5L)

        then:
        1 * fileServiceMock.getFiles(5L, false, [FileType.DOCUMENT_FAQ, FileType.DOCUMENT_TERMS, FileType.DOCUMENT_CERTIFICATE]) >>
                [termsDocumentFileVo(withContent), doiApplicationFileVo()]
        fileVos.businessKey == [termsDocumentFileVo(withoutContent)].businessKey
    }
}
