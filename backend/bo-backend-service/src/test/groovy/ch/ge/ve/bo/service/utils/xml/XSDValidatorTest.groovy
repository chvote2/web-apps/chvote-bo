/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.utils.xml

import static ch.ge.ve.bo.service.TestHelpers.getResourceStream

import ch.ge.ve.bo.service.utils.exception.SchemaValidationException
import spock.lang.Specification

class XSDValidatorTest extends Specification {
  XSDValidator validator = XSDValidator.of("xsd/eCH-0045-4-0.xsd", "catalog.cat")

  def "Validate with an invalid file thrown a SchemaValidationException"() {
    given: "an invalid XML file"
    def is = getResourceStream(this.getClass(), "invalid.xml")

    when: "xml validator is started"
    validator.validate(is, XSDValidator.RETHROW_ERROR_HANDLER)

    then:
    thrown SchemaValidationException
  }

  def "Validate with an valid file shouldn't thrown a SchemaValidationException"() {
    given: "an invalid XML file"
    def is = getResourceStream(this.getClass(), "valid.xml")

    when: "xml validator is started"
    validator.validate(is, XSDValidator.RETHROW_ERROR_HANDLER)

    then:
    notThrown SchemaValidationException
  }


}
