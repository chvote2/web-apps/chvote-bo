/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.dataset

import static ch.ge.ve.bo.MilestoneType.BALLOT_BOX_DECRYPT
import static ch.ge.ve.bo.MilestoneType.BALLOT_BOX_INIT
import static ch.ge.ve.bo.MilestoneType.CERTIFICATION
import static ch.ge.ve.bo.MilestoneType.DATA_DESTRUCTION
import static ch.ge.ve.bo.MilestoneType.PRINTER_FILES
import static ch.ge.ve.bo.MilestoneType.RESULT_VALIDATION
import static ch.ge.ve.bo.MilestoneType.SITE_CLOSE
import static ch.ge.ve.bo.MilestoneType.SITE_OPEN
import static ch.ge.ve.bo.MilestoneType.SITE_VALIDATION
import static ch.ge.ve.bo.repository.dataset.DatasetTools.localDateTime
import static ch.ge.ve.bo.repository.dataset.OperationDataset.Options

import ch.ge.ve.bo.repository.entity.DeploymentTarget
import ch.ge.ve.bo.service.operation.model.MilestoneVo
import ch.ge.ve.bo.service.operation.model.OperationVo
import java.time.LocalDateTime

/**
 * Dataset used for testing {@link OperationVo} model and service.
 */
class OperationVoDataset {

    static final LocalDateTime lastModificationDate = localDateTime('01.01.2017 00:00:00')
    static final LocalDateTime lastConfigurationUpdateDate = localDateTime('01.01.2017 00:10:00')


    static OperationVo electoralOperationVo(Options options = new Options()) {
        new OperationVo(
                options.get(Options.id, 1),
                'test',
                localDateTime('13.12.2051 00:00:00'),
                'EL042050',
                'Élections communales du 19 avril 2050',
                options.get(Options.operationDate, localDateTime('19.04.2050 00:00:00')),
                9,
                options.get(Options.withMilestone, true) ? [
                        new MilestoneVo(1, 'test', localDateTime('19.04.2050 00:00:00'), SITE_VALIDATION, localDateTime('11.02.2050 00:00:00')),
                        new MilestoneVo(2, 'test', localDateTime('19.04.2050 00:00:00'), CERTIFICATION, localDateTime('27.02.2050 00:00:00')),
                        new MilestoneVo(3, 'test', localDateTime('19.04.2050 00:00:00'), PRINTER_FILES, localDateTime('04.03.2050 00:00:00')),
                        new MilestoneVo(4, 'test', localDateTime('19.04.2050 00:00:00'), BALLOT_BOX_INIT, localDateTime('20.03.2050 00:00:00')),
                        new MilestoneVo(5, 'test', localDateTime('19.04.2050 00:00:00'), SITE_OPEN, localDateTime('23.03.2050 12:00:00')),
                        new MilestoneVo(6, 'test', localDateTime('19.04.2050 00:00:00'), SITE_CLOSE, localDateTime('18.04.2050 12:00:00')),
                        new MilestoneVo(7, 'test', localDateTime('19.04.2050 00:00:00'), BALLOT_BOX_DECRYPT, localDateTime('19.04.2050 00:00:00')),
                        new MilestoneVo(8, 'test', localDateTime('19.04.2050 00:00:00'), RESULT_VALIDATION, localDateTime('21.04.2050 00:00:00')),
                        new MilestoneVo(9, 'test', localDateTime('19.04.2050 00:00:00'), DATA_DESTRUCTION, localDateTime('18.07.2050 00:00:00'))
                ] : [],
                lastModificationDate,
                options.get(Options.simulationName, null),
                options.get(Options.deploymentTarget, DeploymentTarget.NOT_DEFINED),
                lastConfigurationUpdateDate,
                options.get(Options.votingCardTitle, "votingCardTitle"),
                options.get(Options.printerTemplate, "printerTemplate"),
                "Canton de Genève",
                [].toSet()
        )
    }


    static OperationVo votingOperationVo(Options options = new Options()) {
        new OperationVo(
                options.get(Options.id, 2),
                'test',
                localDateTime('10.04.2055 00:00:00'),
                'VP052055',
                'Votation Populaire du 21 mai 2055',
                options.get(Options.operationDate, localDateTime('21.05.2055 00:00:00')),
                7,
                options.get(Options.withMilestone, true) ? [
                        new MilestoneVo(10, 'test', localDateTime('19.04.2059 00:00:00'), SITE_VALIDATION, localDateTime('02.05.2055 00:00:00')),
                        new MilestoneVo(11, 'test', localDateTime('19.04.2059 00:00:00'), CERTIFICATION, localDateTime('02.05.2055 00:00:00')),
                        new MilestoneVo(12, 'test', localDateTime('19.04.2059 00:00:00'), PRINTER_FILES, localDateTime('03.05.2055 00:00:00')),
                        new MilestoneVo(13, 'test', localDateTime('19.04.2059 00:00:00'), BALLOT_BOX_INIT, localDateTime('08.05.2055 00:00:00')),
                        new MilestoneVo(14, 'test', localDateTime('19.04.2059 00:00:00'), SITE_OPEN, localDateTime('08.05.2055 12:00:00')),
                        new MilestoneVo(15, 'test', localDateTime('19.04.2059 00:00:00'), SITE_CLOSE, localDateTime('20.05.2055 12:00:00')),
                        new MilestoneVo(16, 'test', localDateTime('19.04.2059 00:00:00'), BALLOT_BOX_DECRYPT, localDateTime('21.05.2055 00:00:00')),
                        new MilestoneVo(17, 'test', localDateTime('19.04.2059 00:00:00'), RESULT_VALIDATION, localDateTime('23.05.2055 00:00:00')),
                        new MilestoneVo(18, 'test', localDateTime('19.04.2059 00:00:00'), DATA_DESTRUCTION, localDateTime('19.08.2055 00:00:00'))
                ] : [],
                lastModificationDate,
                options.get(Options.simulationName, null),
                options.get(Options.deploymentTarget, DeploymentTarget.NOT_DEFINED),
                lastConfigurationUpdateDate,
                options.get(Options.votingCardTitle, "votingCardTitle"),
                options.get(Options.printerTemplate, "printerTemplate"),
                "Canton de Genève",
                [].toSet()
        )
    }


}
