/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.dataset

import static ch.ge.ve.bo.repository.dataset.DatasetTools.localDateTime
import static ch.ge.ve.bo.repository.dataset.FileDataset.Options

import ch.ge.ve.bo.FileType
import ch.ge.ve.bo.Language
import ch.ge.ve.bo.service.TestHelpers
import ch.ge.ve.bo.service.file.model.FileVo
import java.nio.charset.StandardCharsets

class FileVoDataset {

  static FileVo votationOperationRepositoryVo(Options options = new Options()) {
    new FileVo(
            options.get(Options.id, 1),
            'admin',
            localDateTime('13.12.2016 00:00:00'),
            options.get(Options.withoutBusinessKey, false) ? null : '202012SGAvota3://all6c6f4b14b6d349ef97703f2af84ffbc6',
            options.get(Options.operationId, 2),
            options.get(Options.fileType, FileType.VOTATION_REPOSITORY),
            null,
            'repositoryVotation.xml',
            'Gy',
            options.get(Options.withoutContent, false) ? null : TestHelpers.getResourceBytes("repositoryVotation.xml")
    )
  }

  static FileVo electionOperationRepositoryVo(Options options = new Options()) {
    new FileVo(
            options.get(Options.id, 1),
            'admin',
            localDateTime('13.12.2016 00:00:00'),
            options.get(Options.withoutBusinessKey, false) ? null : '202012SGAvota3://all6c6f4b14b6d349ef97703f2af84ffbc6',
            options.get(Options.operationId, 2),
            options.get(Options.fileType, FileType.ELECTION_REPOSITORY),
            null,
            'repositoryElection.xml',
            'Gy',
            options.get(Options.withoutContent, false) ? null : TestHelpers.getResourceBytes("repositoryElection.xml")
    )
  }

  /**
   * Just a semantic shortcut to a doi application file
   * Use this if you don't really care about file content
   */
  static FileVo someFileVo(Options options = new Options()) {
    return doiApplicationFileVo(options)
  }

  static FileVo doiApplicationFileVo(Options options = new Options()) {
    new FileVo(
            options.get(Options.id, 2),
            'test',
            localDateTime('13.12.2016 14:30:00'),
            options.get(Options.withoutBusinessKey, false) ? null : '201709VPbo://TEST70dc0e5d947d44bdbbef2092ee473f99',
            options.get(Options.operationId, 1),
            options.get(Options.fileType, FileType.DOMAIN_OF_INFLUENCE),
            null,
            'valid-doi.xml',
            options.get(Options.managementEntity, "Canton de Genève"),
            options.get(Options.withoutContent, false) ? null : TestHelpers.getResourceBytes("doi.xml")
    )
  }

  static FileVo termsDocumentFileVo(Options options = new Options()) {
    new FileVo(
            options.get(Options.id, 3),
            'test',
            localDateTime('13.12.2016 14:30:00'),
            options.get(Options.withoutBusinessKey, false) ? null : '201709VP|OP_DOC_TERMS|DE',
            options.get(Options.operationId, 1),
            options.get(Options.fileType, FileType.DOCUMENT_TERMS),
            Language.DE,
            'example.pdf',
            'Canton de Genève',
            options.with(Options.withoutContent, false) ? null : "%PDF-1.4 CONTENT %%EOF\n".getBytes(StandardCharsets.UTF_8)
    )
  }

  static FileVo pdfDocumentApplicationFileVo(Options options = new Options()) {
    new FileVo(
            options.get(Options.id, 4),
            'admin',
            localDateTime('13.12.2016 00:00:00'),
            options.get(Options.withoutBusinessKey, false) ? null : '201709VP|OP_DOC_CERTIFICATE|FR',
            options.get(Options.operationId, 1),

            options.get(Options.fileType, FileType.DOCUMENT_CERTIFICATE),
            null,
            'certificate.pdf',
            'Canton de Genève',
            options.with(Options.withoutContent, false) ? null : "%PDF-1.4 CONTENT %%EOF\n".getBytes(StandardCharsets.UTF_8)
    )
  }


}
