/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.utils

import static ch.ge.ve.bo.service.operation.model.VotingMaterialStatusVo.State.COMPLETE
import static ch.ge.ve.bo.service.operation.model.VotingMaterialStatusVo.State.CREATED
import static ch.ge.ve.bo.service.operation.model.VotingMaterialStatusVo.State.INCOMPLETE

import ch.ge.ve.bo.service.operation.model.ConfigurationStatusVo
import ch.ge.ve.bo.service.operation.model.ModificationMode
import ch.ge.ve.bo.service.operation.model.OperationStatusVo
import ch.ge.ve.bo.service.operation.model.TallyArchiveStatusVo
import ch.ge.ve.bo.service.operation.model.VotingMaterialStatusVo
import java.time.LocalDateTime

class OperationStatusBuilder {


  def votingMaterialComplete = ["section": false]
  def configurationComplete = ["section": false]
  boolean votingMaterialInReadonly
  boolean configurationInReadonly

  def withConfigurationInReadonly() {
    this.configurationInReadonly = true
    return this
  }

  def withVotingMaterialInReadonly() {
    this.votingMaterialInReadonly = true
    return this
  }


  def withConfigurationComplete() {
    configurationComplete = ["section": true]
    return this
  }

  def withVotingMaterialComplete() {
    this.votingMaterialComplete = ["section": true]
    return this
  }


  def build() {
    VotingMaterialStatusVo votingMaterialStatus = new VotingMaterialStatusVo(
            votingMaterialInReadonly ? CREATED : (votingMaterialComplete["section"] ? COMPLETE : INCOMPLETE),
            LocalDateTime.now(),
            votingMaterialComplete, null)

    def state = OperationStatusVo.isComplete(configurationComplete) ?
            ConfigurationStatusVo.State.COMPLETE : ConfigurationStatusVo.State.INCOMPLETE
    if (configurationInReadonly) {
      state = ConfigurationStatusVo.State.TEST_SITE_IN_DEPLOYMENT
    }
    return new OperationStatusVo(
            new ConfigurationStatusVo.Builder()
                    .setState(state, LocalDateTime.now(), "user")
                    .setCompletedSections(configurationComplete)
                    .build(),
            votingMaterialStatus,
            null, TallyArchiveStatusVo.notRequested(), null, null)
  }


}
