/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.status.complete

import ch.ge.ve.bo.FileType
import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.service.file.FileService
import ch.ge.ve.bo.service.file.model.FileVo
import spock.lang.Specification

class RepositoryCompleteTest extends Specification {
  FileService fileService = Mock(FileService)
  def service = new RepositoryComplete(fileService)

  def "IsSectionComplete is true when there is a repository file"() {
    given:
    def operation = OperationDataset.electoralOperation()
    fileService.getFiles(operation.getId(), false,
            EnumSet.of(FileType.VOTATION_REPOSITORY, FileType.ELECTION_REPOSITORY)) >> [
            new FileVo("test", FileType.VOTATION_REPOSITORY, "test", null)
    ]

    expect:
    service.isSectionComplete(operation)
  }


  def "IsSectionComplete is false when there is no repository file"() {
    given:
    def operation = OperationDataset.electoralOperation()
    fileService.getFiles(operation.getId(), false,
            EnumSet.of(FileType.VOTATION_REPOSITORY, FileType.ELECTION_REPOSITORY)) >> []

    expect:
    !service.isSectionComplete(operation)
  }

  def "sectionName is repository"() {
    expect:
    service.sectionName == "repository"
  }


}
