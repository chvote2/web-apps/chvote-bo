/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.consistency.checks

import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo
import ch.ge.ve.bo.service.operation.parameters.election.ElectionPagePropertiesService
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryService
import spock.lang.Specification

class AllBallotsInElectionPagePropertiesShouldBeInOperationRepositoryTest extends Specification {
  def repositoryService = Mock(OperationRepositoryService)
  def electionPagePropertyService = Mock(ElectionPagePropertiesService)
  def check = new AllBallotsInElectionPagePropertiesShouldBeInOperationRepository(
          repositoryService,
          electionPagePropertyService
  )

  def "CheckForOperation"() {
    given: "an operation"
    def operation = OperationDataset.electoralOperation()

    and: "A repository has been uploaded with one ballot"
    repositoryService.getAllBallotsForOperation(operation.id, true, false) >> ["known"].toSet()

    and: "An election page properties mapping has been defined on absent ballot"
    electionPagePropertyService.getBallotToModelIdMapping(operation.id) >> ["known": 1, "unknown": 2]

    when:
    def result = check.checkForOperation(operation)

    then:
    [ConsistencyErrorVo.ConsistencyErrorType.BALLOT_IN_ELECTION_PAGE_PROPERTIES_NOT_IN_OPERATION_REPOSITORY] == result.consistencyErrors.errorType
    "[[ballot:unknown]]" == result.consistencyErrors.errorsParameters.toString()
  }
}
