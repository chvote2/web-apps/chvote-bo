/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.document;

import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.service.model.ContentAndFileName;
import ch.ge.ve.bo.service.operation.model.HighlightedQuestionVo;
import java.util.List;

/**
 * Service responsible of managing {@link ch.ge.ve.bo.repository.entity.HighlightedQuestion} and {@link ch.ge.ve.bo.repository.entity.LocalizedHighlightedQuestion}
 */
public interface HighlightedQuestionService {
  /**
   * Add an Highlighted Question to an operation
   */
  HighlightedQuestionVo addQuestion(Long operationId);

  /**
   * Add an Highlighted Question localized content to an operation
   */
  HighlightedQuestionVo addLocalizedQuestion(
      Long highlightedQuestionId,
      String filename,
      String localizedQuestion,
      Language language,
      byte[] fileContent);

  /**
   * delete a Highlighted Question localized content
   */
  HighlightedQuestionVo deleteLocalizedQuestion(Long questionId, Long localizedQuestionId);

  /**
   * delete a Highlighted Question and all related contents
   */
  void deleteQuestion(Long questionId);

  /**
   * @return all Highlighted Question to an operation
   */
  List<HighlightedQuestionVo> findByOperation(Long operationId);

  /**
   * @return download information for a Highlighted Question
   */
  ContentAndFileName getFileContent(long localizedQuestionId);


}
