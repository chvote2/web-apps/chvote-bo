/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.exception;

import ch.ge.ve.bo.service.exception.BusinessException;

/**
 * Exception indicating that an operation cannot be close prematurely
 */
public class CloseVotingPeriodException extends BusinessException {
  private CloseVotingPeriodException(String messageKey, String... parameters) {
    super(messageKey, parameters);
  }

  /**
   * Cannot close because operation is not in simulation
   */
  public static  CloseVotingPeriodException operationIsNotInSimulation(){
    return new CloseVotingPeriodException("tally.archive.errors.operation-is-not-in-simulation");
  }

  /**
   * Cannot close because tally archive already in generation
   */
  public static CloseVotingPeriodException tallyArchiveGenerationAlreadyRequested() {
    return new CloseVotingPeriodException("tally.archive.errors.generation-already-requested");
  }

  /**
   * Cannot close because voting period is not yet initialized
   */
  public static CloseVotingPeriodException votingPeriodNotInitialized() {
    return new CloseVotingPeriodException("tally.archive.errors.voting-period-not-initialized");
  }

  /**
   * Cannot close because tally archive already created
   */
  public static CloseVotingPeriodException tallyArchiveGenerationAlreadyDone() {
    return new CloseVotingPeriodException("tally.archive.errors.generation-already-done");
  }
}
