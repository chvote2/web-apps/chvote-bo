/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.repository;

import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.file.FileService;
import ch.ge.ve.bo.service.file.model.FileVo;
import ch.ge.ve.bo.service.lambda.FunctionWithBusinessException;
import ch.ge.ve.bo.service.operation.parameters.repository.exception.OperationRepositoryDateMismatchException;
import ch.ge.ve.bo.service.operation.parameters.repository.model.RepositoryFileVo;
import ch.ge.ve.bo.service.operation.parameters.repository.model.RepositoryImportResult;
import ch.ge.ve.bo.service.utils.xml.SimpleErrorHandler;
import ch.ge.ve.bo.service.utils.xml.XSDValidator;
import ch.ge.ve.interfaces.ech.eCH0058.v5.HeaderType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.ContestType;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Set;
import java.util.function.Function;
import org.springframework.transaction.annotation.Transactional;

abstract class OperationRepositoryServiceHandler<D, T extends RepositoryFileVo> {
  private final XSDValidator             xsdValidator;
  private final Function<InputStream, D> deliveryBuilder;
  private final FileService              fileService;

  OperationRepositoryServiceHandler(XSDValidator xsdValidator,
                                    Function<InputStream, D> deliveryBuilder,
                                    FileService fileService) {
    this.xsdValidator = xsdValidator;
    this.deliveryBuilder = deliveryBuilder;
    this.fileService = fileService;
  }

  D fromXml(InputStream is) {
    return deliveryBuilder.apply(is);
  }


  abstract HeaderType getHeader(D delivery);

  abstract ContestType getContest(D delivery);


  public RepositoryImportResult importFile(String fileName, byte[] fileContent, Operation
      operation) throws BusinessException {
    return doWithNewRepositoryFile(
        fileName, fileContent, operation,
        document -> fileService.importFile(
            new FileVo(fileService.buildApplicationBusinessKey(getHeader(document).getSenderId(),
                                                               getHeader(document).getMessageId()),
                       operation.getId(), getFileType(), null, fileName, ConnectedUser.get().managementEntity,
                       fileContent), false)
    );
  }

  private RepositoryImportResult doWithNewRepositoryFile(String fileName,
                                                         byte[] fileContent,
                                                         Operation operation,
                                                         FunctionWithBusinessException<D, FileVo> documentFunction)
      throws BusinessException {
    SimpleErrorHandler errorHandler = new SimpleErrorHandler();
    xsdValidator.validate(new ByteArrayInputStream(fileContent), errorHandler);
    if (!errorHandler.getErrors().isEmpty()) {
      return new RepositoryImportResult(null, errorHandler.getErrors());
    }

    D deliveryDocument = fromXml(new ByteArrayInputStream(fileContent));

    // check operation's date
    LocalDate contestDate = getContest(deliveryDocument).getContestDate();
    if (contestDate.getDayOfMonth() != operation.getDate().getDayOfMonth() ||
        contestDate.getMonthValue() != operation.getDate().getMonthValue() ||
        contestDate.getYear() != operation.getDate().getYear()) {
      throw new OperationRepositoryDateMismatchException(fileName);
    }

    FileVo applicationFileVo = documentFunction.apply(deliveryDocument);

    return new RepositoryImportResult(buildFrom(deliveryDocument, applicationFileVo, false), Collections.emptyList());
  }


  abstract T buildFrom(D document, FileVo file, boolean withContent);

  abstract T buildFrom(FileVo applicationFileVo, boolean withContent);

  abstract FileType getFileType();

  @SuppressWarnings("unchecked")
  Set<String> getAllDoiIdentification(RepositoryFileVo file) {
    return getAllDoiIdentificationInternal((T) file);
  }

  abstract Set<String> getAllDoiIdentificationInternal(T file);


  @SuppressWarnings("unchecked")
  Set<String> getAllBallots(RepositoryFileVo file) {
    return getAllBallotsInternal((T) file);
  }

  abstract Set<String> getAllBallotsInternal(T file);


  @Transactional
  public RepositoryImportResult replaceFile(String fileName, long oldId, byte[] fileContent, Operation
      operation) throws BusinessException {
    return doWithNewRepositoryFile(
        fileName, fileContent, operation,
        document ->
            fileService.replaceFile(
                oldId,
                new FileVo(
                    fileService.buildApplicationBusinessKey(getHeader(document).getSenderId(),
                                                            getHeader(document).getMessageId()),
                    operation.getId(), getFileType(), null, fileName, ConnectedUser.get().managementEntity,
                    fileContent)));
  }
}
