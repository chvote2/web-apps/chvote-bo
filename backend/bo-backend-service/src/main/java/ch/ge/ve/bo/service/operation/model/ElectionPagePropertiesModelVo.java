/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model;

import ch.ge.ve.bo.repository.entity.ElectionPagePropertiesModel;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.util.StringUtils;

/**
 * Value object for {@link ElectionPagePropertiesModel}
 */
public class ElectionPagePropertiesModelVo {
  private final long     id;
  private final String   modelName;
  private final boolean  displayCandidateSearchForm;
  private final boolean  allowChangeOfElectoralList;
  private final boolean  displayEmptyPosition;
  private final boolean  displayCandidatePositionOnACompactBallotPaper;
  private final boolean  displayCandidatePositionOnAModifiedBallotPaper;
  private final boolean  displaySuffrageCount;
  private final boolean  displayListVerificationCode;
  private final String   candidateInformationDisplayModel;
  private final boolean  allowOpenCandidature;
  private final boolean  allowMultipleMandates;
  private final boolean  displayVoidOnEmptyBallotPaper;
  private final String[] displayedColumnsOnVerificationTable;
  private final String[] columnsOrderOnVerificationTable;


  /**
   * Default constructor
   */
  @JsonCreator
  public ElectionPagePropertiesModelVo(
      @JsonProperty("id") long id,
      @JsonProperty("modelName") String modelName,
      @JsonProperty("displayCandidateSearchForm") boolean displayCandidateSearchForm,
      @JsonProperty("allowChangeOfElectoralList") boolean allowChangeOfElectoralList,
      @JsonProperty("displayEmptyPosition") boolean displayEmptyPosition,
      @JsonProperty("displayCandidatePositionOnACompactBallotPaper")
          boolean displayCandidatePositionOnACompactBallotPaper,
      @JsonProperty("displayCandidatePositionOnAModifiedBallotPaper")
          boolean displayCandidatePositionOnAModifiedBallotPaper,
      @JsonProperty("displaySuffrageCount") boolean displaySuffrageCount,
      @JsonProperty("displayListVerificationCode") boolean displayListVerificationCode,
      @JsonProperty("candidateInformationDisplayModel") String candidateInformationDisplayModel,
      @JsonProperty("allowOpenCandidature") boolean allowOpenCandidature,
      @JsonProperty("allowMultipleMandates") boolean allowMultipleMandates,
      @JsonProperty("displayVoidOnEmptyBallotPaper") boolean displayVoidOnEmptyBallotPaper,
      @JsonProperty("displayedColumnsOnVerificationTable") String[] displayedColumnsOnVerificationTable,
      @JsonProperty("columnsOrderOnVerificationTable") String[] columnsOrderOnVerificationTable

  ) {
    this.id = id;
    this.modelName = modelName;
    this.displayCandidateSearchForm = displayCandidateSearchForm;
    this.allowChangeOfElectoralList = allowChangeOfElectoralList;
    this.displayEmptyPosition = displayEmptyPosition;
    this.displayCandidatePositionOnACompactBallotPaper = displayCandidatePositionOnACompactBallotPaper;
    this.displayCandidatePositionOnAModifiedBallotPaper = displayCandidatePositionOnAModifiedBallotPaper;
    this.displaySuffrageCount = displaySuffrageCount;
    this.displayListVerificationCode = displayListVerificationCode;
    this.candidateInformationDisplayModel = candidateInformationDisplayModel;
    this.allowOpenCandidature = allowOpenCandidature;
    this.allowMultipleMandates = allowMultipleMandates;
    this.displayVoidOnEmptyBallotPaper = displayVoidOnEmptyBallotPaper;
    this.displayedColumnsOnVerificationTable = displayedColumnsOnVerificationTable;
    this.columnsOrderOnVerificationTable = columnsOrderOnVerificationTable;
  }

  /**
   * entity ro value object mapper
   */
  public ElectionPagePropertiesModelVo(ElectionPagePropertiesModel model) {
    this(model.getId(),
         model.getModelName(),
         model.isDisplayCandidateSearchForm(),
         model.isAllowChangeOfElectoralList(),
         model.isDisplayEmptyPosition(),
         model.isDisplayCandidatePositionOnACompactBallotPaper(),
         model.isDisplayCandidatePositionOnAModifiedBallotPaper(),
         model.isDisplaySuffrageCount(),
         model.isDisplayListVerificationCode(),
         model.getCandidateInformationDisplayModel(),
         model.isAllowOpenCandidature(),
         model.isAllowMultipleMandates(),
         model.isDisplayVoidOnEmptyBallotPaper(),
         model.getDisplayedColumnsOnVerificationTable().split("\\|"),
         StringUtils.isEmpty(model.getColumnsOrderOnVerificationTable()) ?
             new String[0] : model.getColumnsOrderOnVerificationTable().split("\\|")
    );
  }

  /**
   * object mapper to entity mapper
   */
  public void updateEntity(ElectionPagePropertiesModel model) {
    model.setModelName(modelName);
    model.setDisplayCandidateSearchForm(displayCandidateSearchForm);
    model.setAllowChangeOfElectoralList(allowChangeOfElectoralList);
    model.setDisplayEmptyPosition(displayEmptyPosition);
    model.setDisplayCandidatePositionOnACompactBallotPaper(displayCandidatePositionOnACompactBallotPaper);
    model.setDisplayCandidatePositionOnAModifiedBallotPaper(displayCandidatePositionOnAModifiedBallotPaper);
    model.setDisplaySuffrageCount(displaySuffrageCount);
    model.setDisplayListVerificationCode(displayListVerificationCode);
    model.setCandidateInformationDisplayModel(candidateInformationDisplayModel);
    model.setAllowOpenCandidature(allowOpenCandidature);
    model.setAllowMultipleMandates(allowMultipleMandates);
    model.setDisplayVoidOnEmptyBallotPaper(displayVoidOnEmptyBallotPaper);
    model.setColumnsOrderOnVerificationTable(
        Stream.of(columnsOrderOnVerificationTable).collect(Collectors.joining("|")));
    model.setDisplayedColumnsOnVerificationTable(
        Stream.of(displayedColumnsOnVerificationTable).collect(Collectors.joining("|")));
  }

  public long getId() {
    return id;
  }

  public String getModelName() {
    return modelName;
  }

  public boolean isDisplayCandidateSearchForm() {
    return displayCandidateSearchForm;
  }

  public boolean isAllowChangeOfElectoralList() {
    return allowChangeOfElectoralList;
  }

  public boolean isDisplayEmptyPosition() {
    return displayEmptyPosition;
  }

  public boolean isDisplayCandidatePositionOnACompactBallotPaper() {
    return displayCandidatePositionOnACompactBallotPaper;
  }

  public boolean isDisplayCandidatePositionOnAModifiedBallotPaper() {
    return displayCandidatePositionOnAModifiedBallotPaper;
  }

  public boolean isDisplaySuffrageCount() {
    return displaySuffrageCount;
  }

  public boolean isDisplayListVerificationCode() {
    return displayListVerificationCode;
  }

  public String getCandidateInformationDisplayModel() {
    return candidateInformationDisplayModel;
  }

  public boolean isAllowOpenCandidature() {
    return allowOpenCandidature;
  }

  public boolean isAllowMultipleMandates() {
    return allowMultipleMandates;
  }

  public boolean isDisplayVoidOnEmptyBallotPaper() {
    return displayVoidOnEmptyBallotPaper;
  }

  public String[] getDisplayedColumnsOnVerificationTable() {
    return displayedColumnsOnVerificationTable;
  }

  public String[] getColumnsOrderOnVerificationTable() {
    return columnsOrderOnVerificationTable;
  }
}
