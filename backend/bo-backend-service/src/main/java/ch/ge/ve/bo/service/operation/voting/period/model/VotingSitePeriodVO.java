/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.voting.period.model;

import ch.ge.ve.bo.repository.entity.SimulationPeriodConfiguration;
import java.time.LocalDateTime;

/**
 * Value object for VotingSitePeriod
 */
public class VotingSitePeriodVO {
  private final LocalDateTime dateOpen;
  private final LocalDateTime dateClose;
  private final Integer       gracePeriod;

  /**
   * Default constructor for creation
   */
  public VotingSitePeriodVO( LocalDateTime dateOpen, LocalDateTime dateClose, Integer gracePeriod) {
    this.dateClose = dateClose;
    this.dateOpen = dateOpen;
    this.gracePeriod = gracePeriod;
  }

  /**
   * Default constructor frm entity
   */
  public VotingSitePeriodVO(SimulationPeriodConfiguration config) {
    this.dateClose = config.getDateClose();
    this.dateOpen = config.getDateOpen();
    this.gracePeriod = config.getGracePeriod();
  }

  public LocalDateTime getDateClose() {
    return dateClose;
  }

  public LocalDateTime getDateOpen() {
    return dateOpen;
  }

  public Integer getGracePeriod() {
    return gracePeriod;
  }
}
