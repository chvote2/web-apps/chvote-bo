/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register.processor;

import static ch.ge.ve.bo.service.utils.HashUtils.toHash;

import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.service.model.CountingCircleVo;
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceVo;
import ch.ge.ve.bo.service.operation.register.exception.BusinessValidationException;
import ch.ge.ve.bo.service.operation.register.model.OperationVoterRef;
import ch.ge.ve.bo.service.operation.register.model.RegisterVoterRef;
import ch.ge.ve.bo.service.operation.register.model.Voter;
import ch.ge.ve.interfaces.ech.eCH0007.v6.SwissMunicipalityType;
import ch.ge.ve.interfaces.ech.eCH0010.v6.AddressInformationType;
import ch.ge.ve.interfaces.ech.eCH0010.v6.PersonMailAddressType;
import ch.ge.ve.interfaces.ech.eCH0044.v4.DatePartiallyKnownType;
import ch.ge.ve.interfaces.ech.eCH0044.v4.PersonIdentificationType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.ForeignerType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.PersonType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.SwissAbroadType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.SwissDomesticType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.VotingPersonType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.extension.PersonTypeExtensionType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.CountingCircleType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.DomainOfInfluenceType;
import ch.ge.ve.interfaces.ech.service.EchCodec;
import ch.ge.ve.interfaces.ech.service.JAXBEchCodecImpl;
import java.text.Normalizer;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import javax.xml.stream.Location;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Rules used to validate register files
 */
class BusinessProcessorRules {
  private static final Logger logger = LoggerFactory.getLogger(BusinessProcessorRules.class);

  private static final Map<String, XmlElementProcessor>  ENTER_RULES = new HashMap<>();
  private static final Map<String, ExitXmlNodeProcessor> EXIT_RULES  = new HashMap<>();

  private static final String VOTER_LIST                 = "voterDelivery.voterList";
  private static final String NUMBER_OF_VOTERS           = "voterDelivery.voterList.numberOfVoters";
  private static final String REGISTER_ID                = ".registerIdentification";
  private static final String REGISTER_NAME              = ".registerName";
  private static final String CANTONAL_REGISTER          = VOTER_LIST + ".reportingAuthority.cantonalRegister";
  private static final String CANTONAL_REGISTER_ID       = CANTONAL_REGISTER + REGISTER_ID;
  private static final String CANTONAL_REGISTER_NAME     = CANTONAL_REGISTER + REGISTER_NAME;
  private static final String MUNICIPALITY_REGISTER      = VOTER_LIST + ".reportingAuthority.municipalityRegister";
  private static final String MUNICIPALITY_REGISTER_ID   = MUNICIPALITY_REGISTER + REGISTER_ID;
  private static final String MUNICIPALITY_REGISTER_NAME = MUNICIPALITY_REGISTER + REGISTER_NAME;
  private static final String OTHER_REGISTER             = VOTER_LIST + ".reportingAuthority.otherRegister";
  private static final String OTHER_REGISTER_ID          = OTHER_REGISTER + REGISTER_ID;
  private static final String OTHER_REGISTER_NAME        = OTHER_REGISTER + REGISTER_NAME;
  private static final String HEADER                     = "voterDelivery.deliveryHeader";

  private static final String SENDER_ID                        = HEADER + ".senderId";
  private static final String SENDING_APPLICATION_MANUFACTURER = HEADER + ".sendingApplication.manufacturer";
  private static final String SENDING_APPLICATION_PRODUCT      = HEADER + ".sendingApplication.product";
  private static final String MESSAGE_ID                       = HEADER + ".messageId";
  private static final String MESSAGE_DATE                     = HEADER + ".messageDate";

  private static final String VOTER                           = "voterDelivery.voterList.voter";
  private static final String DOI_AND_REGISTER_EMITTER_FORMAT = "%s.%s";

  private static final EchCodec<VotingPersonType> VOTER_CODEC = new JAXBEchCodecImpl<>(VotingPersonType.class);

  static {

    ENTER_RULES.put(NUMBER_OF_VOTERS, (ctx, xml) -> ctx.setExpectedNumberOfVoter(Integer.valueOf(ctx.getText(xml))));
    ENTER_RULES.put(VOTER, BusinessProcessorRules::parseVoter);

    ENTER_RULES.put(CANTONAL_REGISTER_ID, (ctx, xml) -> ctx.getFileInfo().setEmitter(ctx.getText(xml)));
    ENTER_RULES.put(CANTONAL_REGISTER_NAME, (ctx, xml) -> ctx.getFileInfo().setEmitter(
        String.format(DOI_AND_REGISTER_EMITTER_FORMAT, ctx.getFileInfo().getEmitter(), ctx.getText(xml))));

    ENTER_RULES.put(MUNICIPALITY_REGISTER_ID, (ctx, xml) -> ctx.getFileInfo().setEmitter(ctx.getText(xml)));
    ENTER_RULES.put(MUNICIPALITY_REGISTER_NAME, (ctx, xml) -> ctx.getFileInfo().setEmitter(
        String.format(DOI_AND_REGISTER_EMITTER_FORMAT, ctx.getFileInfo().getEmitter(), ctx.getText(xml))));

    ENTER_RULES.put(OTHER_REGISTER_ID, (ctx, xml) -> ctx.getFileInfo().setEmitter(ctx.getText(xml)));
    ENTER_RULES.put(OTHER_REGISTER_NAME, (ctx, xml) -> ctx.getFileInfo().setEmitter(
        String.format(DOI_AND_REGISTER_EMITTER_FORMAT, ctx.getFileInfo().getEmitter(), ctx.getText(xml))));


    ENTER_RULES.put(MESSAGE_ID, (ctx, xml) -> ctx.getUniqueMessageIdContext().setMessageId(ctx.getText(xml)));
    ENTER_RULES.put(MESSAGE_DATE, (ctx, xml) -> {
      String dateAsTest = ctx.getText(xml);
      ctx.getUniqueMessageIdContext().setMessageDate(dateAsTest);
      ctx.getFileInfo().setEmissionDate(LocalDateTime.parse(dateAsTest, DateTimeFormatter.ISO_DATE_TIME));
    });
    ENTER_RULES.put(SENDING_APPLICATION_MANUFACTURER,
                    (ctx, xml) -> ctx.getUniqueMessageIdContext().setManufacturer(ctx.getText(xml)));
    ENTER_RULES
        .put(SENDING_APPLICATION_PRODUCT, (ctx, xml) -> ctx.getUniqueMessageIdContext().setProduct(ctx.getText(xml)));
    ENTER_RULES.put(SENDER_ID, (ctx, xml) -> ctx.getUniqueMessageIdContext().setSenderId(ctx.getText(xml)));

    EXIT_RULES.put(HEADER, ctx -> ctx.getUniqueMessageIdContext().checkMessageUniqueness());
  }

  private static void parseVoter(BusinessProcessorContext ctx, XMLStreamReader xml) {
    Location location = xml.getLocation();
    try {
      VotingPersonType votingPersonType = VOTER_CODEC.deserialize(xml, false);
      SwissDomesticType swiss = votingPersonType.getPerson().getSwiss();
      SwissAbroadType swissAbroad = votingPersonType.getPerson().getSwissAbroad();
      ForeignerType foreigner = votingPersonType.getPerson().getForeigner();

      Voter voter;

      if (swiss != null) {
        voter = processSwiss(ctx, location, votingPersonType, swiss);
      } else if (swissAbroad != null) {
        voter = processSwissAbroad(ctx, location, votingPersonType, swissAbroad);
      } else {
        voter = processForeigner(ctx, location, votingPersonType, foreigner);
      }

      checkCommonExtensionElements(ctx, voter, votingPersonType);
      checkVoterId(ctx, voter);
      checkDuplicate(ctx, voter);
      checkVoterDateOfBirth(ctx, voter);
      checkVoterDoi(ctx, voter);
      checkVoterCountingCircle(ctx, voter);
      checkVoterMunicipalityAndCanton(ctx, voter);
      ctx.addVoter(voter);
    } catch (Exception e) {
      logger.warn("Cannot parse voter", e);
      ctx.addInvalidVoter(location, e.getMessage());
    }
    ctx.navigateBack();

  }

  private static void checkVoterCountingCircle(BusinessProcessorContext ctx, Voter voter) {
    if (voter.getCountingCircle() == null) {
      ctx.addMissingCountingCircle(voter);
    }
  }

  private static Voter processSwiss(BusinessProcessorContext ctx, Location location, VotingPersonType votingPerson,
                                    SwissDomesticType swiss) {
    PersonType person = swiss.getSwissDomesticPerson();
    Voter voter = createVoterWithCommonInfo(ctx, votingPerson, person, location);
    voter.setVoterType(Voter.VoterType.SWISS);
    SwissMunicipalityType municipality = swiss.getMunicipality();
    if (municipality != null && municipality.getMunicipalityId() != null) {
      voter.setMunicipality(municipality.getMunicipalityName());
      voter.setMunicipalityId(String.valueOf(municipality.getMunicipalityId()));
    }

    return voter;
  }

  private static Voter processSwissAbroad(BusinessProcessorContext ctx, Location location,
                                          VotingPersonType votingPerson, SwissAbroadType swissAbroad) {
    PersonType person = swissAbroad.getSwissAbroadPerson();
    Voter voter = createVoterWithCommonInfo(ctx, votingPerson, person, location);

    voter.setVoterType(Voter.VoterType.SWISS_ABROAD);
    voter.setLang(person.getLanguageOfCorrespondance().value());
    voter.setPostageCode(person.getExtension().getPostageCode());
    SwissMunicipalityType municipality = swissAbroad.getMunicipality();
    if (municipality != null && municipality.getMunicipalityId() != null) {
      voter.setMunicipalityId(String.valueOf(municipality.getMunicipalityId()));
      voter.setMunicipality(municipality.getMunicipalityName());
    }
    if (swissAbroad.getCanton() != null) {
      voter.setCanton(swissAbroad.getCanton().value());
    }

    return voter;
  }

  private static Voter processForeigner(BusinessProcessorContext ctx, Location location,
                                        VotingPersonType votingPerson, ForeignerType foreigner) {
    PersonType person = foreigner.getForeignerPerson();
    Voter voter = createVoterWithCommonInfo(ctx, votingPerson, person, location);
    voter.setVoterType(Voter.VoterType.FOREIGNER);
    SwissMunicipalityType municipality = foreigner.getMunicipality();
    if (municipality != null && municipality.getMunicipalityId() != null) {
      voter.setMunicipalityId(String.valueOf(municipality.getMunicipalityId()));
      voter.setMunicipality(municipality.getMunicipalityName());
    }
    return voter;
  }

  private static Voter createVoterWithCommonInfo(BusinessProcessorContext ctx, VotingPersonType votingPerson,
                                                 PersonType person, Location location) {
    Voter voter = new Voter(location.getLineNumber(), location.getColumnNumber());
    PersonIdentificationType personIdentification = person.getPersonIdentification();
    voter.setIdentifier(personIdentification.getLocalPersonId().getPersonId());
    voter.setSex(personIdentification.getSex());
    voter.setFirstName(personIdentification.getFirstName());
    voter.setLastName(personIdentification.getOfficialName());
    voter.setDateOfBirth(getDateOfBirth(personIdentification.getDateOfBirth()));
    PersonMailAddressType electoralAddress = votingPerson.getElectoralAddress();
    voter.setTitle(electoralAddress.getPerson().getMrMrs());

    AddressInformationType addressInfo = electoralAddress.getAddressInformation();
    voter.setAddressLine1(addressInfo.getAddressLine1());
    voter.setAddressLine2(addressInfo.getAddressLine2());
    voter.setStreet(addressInfo.getStreet());
    voter.setTown(addressInfo.getTown());
    voter.setZipCode(Optional.ofNullable(addressInfo.getSwissZipCode()).map(Object::toString)
                             .orElse(addressInfo.getForeignZipCode()));
    if (votingPerson.isIsEvoter() == null || !votingPerson.isIsEvoter()) {
      ctx.addNotAnEVoter(voter);
    }
    if (voter.getDateOfBirth() == null) {
      ctx.addInvalidDateOfBirth(voter);
    }
    votingPerson.getDomainOfInfluenceInfo().forEach(doiInfo -> processDoiInfo(ctx, voter, doiInfo));
    return voter;
  }

  private static LocalDate getDateOfBirth(DatePartiallyKnownType dateOfBirth) {
    if (dateOfBirth.getYearMonthDay() != null) {
      return dateOfBirth.getYearMonthDay();
    }
    if (dateOfBirth.getYearMonth() != null) {
      return dateOfBirth.getYearMonth().toGregorianCalendar().toZonedDateTime().toLocalDate();
    }
    return dateOfBirth.getYear() == null ?
        null : dateOfBirth.getYear().toGregorianCalendar().toZonedDateTime().toLocalDate();
  }

  private static void processDoiInfo(BusinessProcessorContext ctx, Voter voter,
                                     VotingPersonType.DomainOfInfluenceInfo doiInfo) {
    CountingCircleType cc = doiInfo.getCountingCircle();
    if (cc != null) {
      if (voter.getCountingCircle() != null && !voter.getCountingCircle().id.equals(cc.getCountingCircleId())) {
        ctx.addDuplicateCountingCirlce(voter);
      } else {
        voter.setCountingCircle(new CountingCircleVo(cc.getCountingCircleId(), cc.getCountingCircleName()));
      }
    }

    DomainOfInfluenceType domainOfInfluence = doiInfo.getDomainOfInfluence();
    DomainOfInfluenceVo doi = new DomainOfInfluenceVo(domainOfInfluence.getDomainOfInfluenceType().value(),
                                                      domainOfInfluence.getLocalDomainOfInfluenceIdentification(),
                                                      domainOfInfluence.getDomainOfInfluenceName(),
                                                      domainOfInfluence.getDomainOfInfluenceShortname());
    ctx.addDoi(doi);
    voter.getDomainOfInfluence().add(doi);
  }


  private static void checkVoterMunicipalityAndCanton(BusinessProcessorContext context, Voter voter) {
    if (StringUtils.isBlank(voter.getMunicipalityId()) &&
        (voter.getVoterType() != Voter.VoterType.SWISS_ABROAD || StringUtils.isBlank(voter.getCanton()))) {
      context.addMissingMunicipalityId(voter.getIdentifier(), voter);
    }

    if (StringUtils.isNotBlank(voter.getCanton()) && !ConnectedUser.get().realm.equals(voter.getCanton())) {
      context.addInvalidCanton(voter.getIdentifier(), voter);
    }
  }

  private static void checkVoterDoi(BusinessProcessorContext context, Voter voter) {
    if (isVoterWithoutDoiOfOperation(context.getOperationDomainOfInfluence(), voter)) {
      context.setVoterHasOnlyUndefinedDoi(voter);
    }

  }

  /**
   * test if none of the voter's doi matches the given operation's doi
   *
   * @param operationDois dois of operation
   * @param voter         a voter in an operation register
   *
   * @return true if none of the voter's doi matches the given operation's doi
   */
  protected static boolean isVoterWithoutDoiOfOperation(Set<String> operationDois, Voter voter) {
    //returns true if none of the stream elements matches the given predicate
    return (voter.getDomainOfInfluence().stream()
                 .noneMatch(doi -> operationDois
                     .contains(String.format(DOI_AND_REGISTER_EMITTER_FORMAT, doi.type, doi.id))));
  }


  static void enter(BusinessProcessorContext context, XMLStreamReader xmlStreamReader) throws XMLStreamException {
    XmlElementProcessor rule = ENTER_RULES.get(context.getCurrentPath());
    if (rule != null) {
      rule.process(context, xmlStreamReader);
    }
  }

  static void exit(BusinessProcessorContext context) throws BusinessValidationException {
    ExitXmlNodeProcessor rule = EXIT_RULES.get(context.getCurrentPath());
    if (rule != null) {
      rule.process(context);
    }
  }

  private static void checkVoterDateOfBirth(BusinessProcessorContext context, Voter voter) {
    if (voter.getDateOfBirth() == null) {
      context.setVoterHasInvalidDateOfBirth(voter);
    }
  }

  private static void checkDuplicate(BusinessProcessorContext context, Voter voter) {
    // calculate voter's hash
    String hashBase = String.format("%s#%s#%s#%s#%s#%s#%s#%s",
                                    voter.getSex(),
                                    normalize(voter.getFirstName()),
                                    normalize(voter.getLastName()),
                                    normalize(voter.getAddressLine1()),
                                    normalize(voter.getAddressLine2()),
                                    normalize(voter.getStreet()),
                                    normalize(voter.getTown()),
                                    normalize(voter.getZipCode()));
    voter.setHash(toHash(hashBase));
    Set<OperationVoterRef> otherVoters = context.getVoterRefTable().getOthers(voter);
    if (!otherVoters.isEmpty()) {
      context.setVoterAsDuplicate(voter);
      if (otherVoters.size() == 1) {
        // Mark the referenced vote also as a duplicate.
        // If size is greater than one it means that we already marked the referenced voter as duplicate
        // Note that the original voter reference table does not contain any duplicate since we do not allow the
        // import of register with duplicates
        context.addDuplicate(otherVoters.iterator().next(), voter);
      }
    }

    // add voter to the reference table
    context.getVoterRefTable().add(context.getFileInfo(), voter);
  }

  private static void checkVoterId(BusinessProcessorContext context, Voter voter) {
    List<RegisterVoterRef> voters = context.getVoterRefTable().getHashTableForFile(context.getFileInfo().getFileName());
    if (voters.stream().anyMatch(v -> v.getIdentifier().equals(voter.getIdentifier()))) {
      context.addDuplicateVoterId(voter);
    }
  }

  private static void checkCommonExtensionElements(BusinessProcessorContext context, Voter voter,
                                                   VotingPersonType votingPerson) {
    if (getExtension(votingPerson.getPerson()).map(PersonTypeExtensionType::getVotingPlace).isEmpty()) {
      context.addMissingExtensionElement(voter, "votingPlace");
    }
  }

  private static Optional<PersonTypeExtensionType> getExtension(VotingPersonType.Person person) {
    return Stream.of(Optional.ofNullable(person.getForeigner()).map(ForeignerType::getForeignerPerson),
                     Optional.ofNullable(person.getSwiss()).map(SwissDomesticType::getSwissDomesticPerson),
                     Optional.ofNullable(person.getSwissAbroad()).map(SwissAbroadType::getSwissAbroadPerson))
                 .filter(Optional::isPresent)
                 .map(p -> p.get().getExtension())
                 .findFirst();
  }

  /**
   * Normalize the input string to avoid comparison mistakes.
   *
   * @param origin the original string
   *
   * @return the normalized version
   */
  private static String normalize(String origin) {
    if (StringUtils.isBlank(origin)) {
      return null;
    }
    // all characters are in lower case
    String result = origin.toLowerCase(Locale.ENGLISH);
    // string is reduced to its canonical form
    result = java.text.Normalizer.normalize(result, Normalizer.Form.NFD);
    // special marks (accents, character modification, ...) are removed
    result = result.replaceAll("[\\p{InCombiningDiacriticalMarks}\\p{IsM}\\p{IsLm}\\p{IsSk}]+", "");
    // "'s" are removed
    result = result.replaceAll("'s", "");
    // "ß" are replaced by "ss"
    result = result.replaceAll("ß", "ss");
    // "ø" are replaced by "o"
    result = result.replaceAll("ø", "o");
    // remaining characters that are not alphanumerical (in ASCII) are removed
    result = result.replaceAll("[^a-z0-9]*", "");

    return result;
  }

  @FunctionalInterface
  private interface XmlElementProcessor {

    @SuppressWarnings("squid:S1160") /* It is actually private */
    void process(BusinessProcessorContext context, XMLStreamReader reader) throws XMLStreamException;
  }

  @FunctionalInterface
  private interface ExitXmlNodeProcessor {
    void process(BusinessProcessorContext context) throws BusinessValidationException;
  }

}
