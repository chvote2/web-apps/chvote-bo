/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.testing.card;

import ch.ge.ve.bo.service.model.VoterTestingCardsLotVo;
import java.util.List;

/**
 * Service related to {@link ch.ge.ve.bo.repository.entity.VoterTestingCardsLot} for an operation
 */
public interface VoterTestingCardsLotService {


  /**
   * Retrive all {@link VoterTestingCardsLotVo} for an operation
   */
  List<VoterTestingCardsLotVo> findAll(Long operationId, boolean forConfiguration);

  /**
   * Add a VoterTestingCardsLotVo to an operation
   */
  VoterTestingCardsLotVo create(Long operationId, VoterTestingCardsLotVo voterTestingCardsLot);

  /**
   * clone testing card lot configuration from test site to prod
   */
  void copyTestingCardFromTestSiteToProduction(Long operationId);


  /**
   * Update an existing {@link ch.ge.ve.bo.repository.entity.VoterTestingCardsLot}
   */
  VoterTestingCardsLotVo edit(VoterTestingCardsLotVo voterTestingCardsLot);

  /**
   * Retrieve a {@link VoterTestingCardsLotVo} by id
   */
  VoterTestingCardsLotVo findOne(long id);

  /**
   * delete a {@link VoterTestingCardsLotVo} by id
   */
  boolean delete(long id);

}
