/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A value object containing the state of the tally archive creation for a deployed protocol instance.
 */
public class TallyArchiveStatusVo {
  private final State  state;
  private final String tallyArchiveDownloadToken;

  /**
   * Initializes a {@link TallyArchiveStatusVo}.
   *
   * @param state                the state of the tally archive creation process.
   * @param tallyArchiveToken    the token to use in order to download the archive.
   */
  public TallyArchiveStatusVo(@JsonProperty("state") State state, String tallyArchiveToken) {
    this.state = state;
    this.tallyArchiveDownloadToken = tallyArchiveToken;
  }

  /**
   * constructor method for not requested tally archive
   */
  public static TallyArchiveStatusVo notRequested() {
    return new TallyArchiveStatusVo(State.NOT_REQUESTED, null);
  }

  public State getState() {
    return state;
  }

  public String getTallyArchiveDownloadToken() {
    return tallyArchiveDownloadToken;
  }

  /**
   * The list of possible states of the tally archive creation process.
   */
  public enum State {
    /**
     * The creation of the tally archive has not been requested and the protocol is still in an earlier stage.
     */
    NOT_REQUESTED,
    /**
     * The tally archive can't be created because there are not enough votes cast to initialize the tally process.
     */
    NOT_ENOUGH_VOTES_CAST,
    /**
     * The creation of the tally archive has started and is currently in progress.
     */
    CREATION_IN_PROGRESS,
    /**
     * The creation of the tally archive has failed for some technical reason.
     */
    CREATION_FAILED,
    /**
     * The tally archive has been created.
     */
    CREATED
  }
}
