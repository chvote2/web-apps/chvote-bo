/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.document;

import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.repository.HighlightedQuestionRepository;
import ch.ge.ve.bo.repository.LocalizedHighlightedQuestionRepository;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.entity.HighlightedQuestion;
import ch.ge.ve.bo.repository.entity.LocalizedHighlightedQuestion;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.service.model.ContentAndFileName;
import ch.ge.ve.bo.service.operation.SecuredOperationHolderService;
import ch.ge.ve.bo.service.operation.model.HighlightedQuestionVo;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

/**
 * Default implementation of {@link HighlightedQuestionService}
 */
@Transactional
public class HighlightedQuestionServiceImpl implements HighlightedQuestionService {

  public static final String HIGHLIGHTED_QUESTION = "highlightedQuestion ";
  private final SecuredOperationRepository operationRepository;
  private final HighlightedQuestionRepository          highlightedQuestionRepository;
  private final LocalizedHighlightedQuestionRepository localizedHighlightedQuestionRepository;
  private final SecuredOperationHolderService          securedOperationHolderService;

  /**
   * Default constructor
   */
  public HighlightedQuestionServiceImpl(SecuredOperationRepository operationRepository,
                                        HighlightedQuestionRepository highlightedQuestionRepository,
                                        LocalizedHighlightedQuestionRepository localizedHighlightedQuestionRepository,
                                        SecuredOperationHolderService securedOperationHolderService) {
    this.operationRepository = operationRepository;
    this.highlightedQuestionRepository = highlightedQuestionRepository;
    this.localizedHighlightedQuestionRepository = localizedHighlightedQuestionRepository;
    this.securedOperationHolderService = securedOperationHolderService;
  }


  @Override
  public HighlightedQuestionVo addQuestion(Long operationId) {
    return securedOperationHolderService.doNotOptionalUpdate(
        () -> {
          Operation operation = operationRepository.findOne(operationId, true);
          HighlightedQuestion highlightedQuestion = new HighlightedQuestion();
          highlightedQuestion.setOperation(operation);
          highlightedQuestion.setLocalizedQuestions(Collections.emptyList());
          return highlightedQuestion;
        },
        highlightedQuestion -> new HighlightedQuestionVo(highlightedQuestionRepository.save(highlightedQuestion)));
  }


  @Override
  public HighlightedQuestionVo addLocalizedQuestion(
      Long questionId, String filename, String localizedQuestion, Language language, byte[] fileContent) {

    return securedOperationHolderService.doNotOptionalUpdate(
        () -> highlightedQuestionRepository
            .findById(questionId)
            .orElseThrow(() -> new EntityNotFoundException(HIGHLIGHTED_QUESTION + questionId)),
        highlightedQuestion -> {
          LocalizedHighlightedQuestion localizedHighlightedQuestion = new LocalizedHighlightedQuestion();
          localizedHighlightedQuestion.setLocalizedQuestion(localizedQuestion);
          localizedHighlightedQuestion.setFileContent(fileContent);
          localizedHighlightedQuestion.setFileName(filename);
          localizedHighlightedQuestion.setQuestion(highlightedQuestion);
          localizedHighlightedQuestion.setLanguage(language);
          highlightedQuestion.getLocalizedQuestions().add(localizedHighlightedQuestion);
          return new HighlightedQuestionVo(highlightedQuestionRepository.save(highlightedQuestion));
        });
  }

  @Override
  public HighlightedQuestionVo deleteLocalizedQuestion(Long questionId, Long localizedQuestionId) {
    return securedOperationHolderService.doUpdate(
        () -> highlightedQuestionRepository.findById(questionId),
        hq -> {
          hq.getLocalizedQuestions()
            .removeIf(localizedQuestion -> localizedQuestion.getId().equals(localizedQuestionId));
          return new HighlightedQuestionVo(highlightedQuestionRepository.save(hq));
        }
    ).orElseThrow(() -> new EntityNotFoundException("highlightedQuestion " + questionId));
  }

  @Override
  public void deleteQuestion(Long questionId) {
    securedOperationHolderService.doNotOptionalUpdateAndForget(
        () -> highlightedQuestionRepository
            .findById(questionId)
            .orElseThrow(() -> new EntityNotFoundException("highlightedQuestion " + questionId)),
        highlightedQuestionRepository::delete
    );
  }

  @Override
  public List<HighlightedQuestionVo> findByOperation(Long operationId) {
    Operation operation = operationRepository.findOne(operationId, false);
    return securedOperationHolderService
        .safeReadAll(() -> highlightedQuestionRepository.findByOperation(operation))
        .stream()
        .map(HighlightedQuestionVo::new)
        .collect(Collectors.toList());
  }

  @Override
  public ContentAndFileName getFileContent(long localizedQuestionId) {
    LocalizedHighlightedQuestion question = securedOperationHolderService
        .safeRead(() -> localizedHighlightedQuestionRepository.findById(localizedQuestionId))
        .orElseThrow(() -> new EntityNotFoundException("localizedQuestion " + localizedQuestionId));
    return new ContentAndFileName(question.getFileName(), question.getFileContent());
  }

}
