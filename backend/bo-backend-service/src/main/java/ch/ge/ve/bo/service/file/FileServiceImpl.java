/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.file;

import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.FileRepository;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.entity.BaseEntity;
import ch.ge.ve.bo.repository.entity.File;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.file.exception.FileAlreadyImportedException;
import ch.ge.ve.bo.service.file.exception.InvalidFileException;
import ch.ge.ve.bo.service.file.model.FileVo;
import ch.ge.ve.bo.service.mapper.BeanMapper;
import ch.ge.ve.bo.service.operation.SecuredOperationHolderService;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * Default implementation of ${FileService}
 */
public class FileServiceImpl implements FileService {

  private final FileRepository                fileRepository;
  private final SecuredOperationRepository    operationRepository;
  private final SecuredOperationHolderService securedOperationHolderService;


  /**
   * Default constructor
   */
  public FileServiceImpl(FileRepository fileRepository,
                         SecuredOperationRepository operationRepository,
                         SecuredOperationHolderService securedOperationHolderService) {
    this.fileRepository = fileRepository;
    this.operationRepository = operationRepository;
    this.securedOperationHolderService = securedOperationHolderService;
  }

  @Override
  public FileVo importFile(FileVo fileVo, boolean deleteIfExist)
      throws BusinessException {
    Optional<File> inDbFile = securedOperationHolderService
        .safeRead(() -> fileRepository.findByBusinessKey(fileVo.businessKey));
    if (inDbFile.isPresent()) {
      if (deleteIfExist && inDbFile.get().getOperation().getId() == fileVo.operationId) {
        securedOperationHolderService.doNotOptionalUpdateAndForget(
            inDbFile::get,
            file -> {
              fileRepository.delete(file);
              fileRepository.flush();
            }
        );
      } else {
        throw new FileAlreadyImportedException(
            inDbFile.get().getSaveDate().format(BaseEntity.DATE_FORMATTER),
            inDbFile.get().getLogin(),
            inDbFile.get().getOperation().getLongLabel()
        );
      }
    }

    File newFile = new File();
    newFile.setBusinessKey(fileVo.businessKey);
    newFile.setOperation(
        operationRepository.findOne(fileVo.operationId, fileVo.type.isRestrictedToInChargeManagementEntity()));
    newFile.setType(fileVo.type);
    newFile.setLanguage(fileVo.language);
    newFile.setFileName(fileVo.fileName);
    newFile.setManagementEntity(fileVo.managementEntity);
    newFile.setFileContent(fileVo.fileContent);
    newFile.setDate(LocalDateTime.now());

    return securedOperationHolderService.doNotOptionalUpdate(
        () -> newFile,
        file -> BeanMapper.map(fileRepository.save(file), true));
  }

  @Override
  public FileVo replaceFile(long oldId, FileVo fileVo) {
    return securedOperationHolderService.doNotOptionalUpdate(
        () -> fileRepository.findById(oldId).orElseThrow(
            () -> new EntityNotFoundException("file id " + oldId)
        ),
        file -> {
          checkFilesBelongToSameOperation(fileVo, file);
          checkFilesBelongToSameTypes(fileVo, file);

          file.setBusinessKey(fileVo.businessKey);
          file.setLanguage(fileVo.language);
          file.setFileName(fileVo.fileName);
          file.setManagementEntity(fileVo.managementEntity);
          file.setFileContent(fileVo.fileContent);
          file.setDate(LocalDateTime.now());
          fileRepository.save(file);
          return BeanMapper.map(file, true);
        });
  }

  private void checkFilesBelongToSameTypes(FileVo fileVo, File file) {
    if (fileVo.type != file.getType()) {
      throw new TechnicalException(String.format(
          "cannot replace file for operation %s file types are not the same old %s new %s",
          file.getOperation().getId(),
          file.getType(),
          fileVo.type));
    }
  }

  private void checkFilesBelongToSameOperation(FileVo fileVo, File file) {
    if (fileVo.operationId != file.getOperation().getId()) {
      throw new TechnicalException(String.format("cannot replace file for operation %s by file for operation %s",
                                                 file.getOperation().getId(), fileVo.operationId));
    }
  }

  @Override
  @Transactional(readOnly = true)
  public List<FileVo> getFiles(long operationId, boolean withContent, Iterable<FileType> types) {
    final List<File> files = securedOperationHolderService
        .safeReadAll(() -> fileRepository.findByOperationIdAndTypeIn(operationId, types));
    return files
        .stream()
        .map(file -> BeanMapper.map(file, withContent))
        .collect(Collectors.toList());
  }

  @Override
  @Transactional(readOnly = true)
  public List<FileVo> getFiles(long operationId, FileType type, Language language) {
    final List<File> files = securedOperationHolderService
        .safeReadAll(() -> language == null ? fileRepository.findByOperationIdAndType(operationId, type)
            : fileRepository.findByOperationIdAndTypeAndLanguage(operationId, type, language));

    return files
        .stream()
        .map(file -> BeanMapper.map(file, true))
        .collect(Collectors.toList());
  }

  @Override
  @Transactional(readOnly = true)
  public FileVo getFile(long fileId) {
    return BeanMapper.map(securedOperationHolderService
                              .safeRead(() -> fileRepository.findById(fileId))
                              .orElseThrow(() -> new EntityNotFoundException("file " + fileId)),
                          true);
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public void deleteFile(long fileId) {
    securedOperationHolderService.doNotOptionalUpdateAndForget(
        () -> fileRepository.findById(fileId)
                            .orElseThrow(() -> new EntityNotFoundException("file " + fileId)),
        fileRepository::delete
    );
  }


  @Override
  public String buildDocumentBusinessKey(long operationId, FileType fileType, Language language) {
    Assert.isTrue(operationId != 0, "operationId should not be 0");
    Assert.notNull(fileType, "fileType should not be null");
    Assert.notNull(language, "language should not be null");
    return String.format("%s|%s|%s|%s", ConnectedUser.get().realm, operationId, fileType, language);
  }

  @Override
  public String buildApplicationBusinessKey(String senderId, String messageId)
      throws InvalidFileException {
    return String.format("%s|%s|%s", ConnectedUser.get().realm, checkSenderId(senderId), checkMessageId(messageId));
  }

  private static String checkMessageId(String messageId) throws InvalidFileException {
    if (messageId == null) {
      throw new InvalidFileException("global.errors.invalid-message-id");
    }
    return messageId;
  }

  private static String checkSenderId(String senderId) throws InvalidFileException {
    if (senderId == null) {
      throw new InvalidFileException("global.errors.invalid-sender-id");
    }
    return senderId;
  }

}
