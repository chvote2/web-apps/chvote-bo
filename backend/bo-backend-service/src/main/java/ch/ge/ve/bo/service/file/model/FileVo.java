/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.file.model;

import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.service.model.BaseVo;
import java.time.LocalDateTime;

/**
 * Value object representing an application file
 */
public class FileVo extends BaseVo {

  public static final String PDF_EXTENSION = ".pdf";
  public static final String XML_EXTENSION = ".xml";

  public final String   businessKey;
  public final long     operationId;
  public final FileType type;
  public final Language language;
  public final String   fileName;
  public final String   managementEntity;
  public final byte[]   fileContent;

  /**
   * Default full constructor
   */
  @SuppressWarnings("squid:S00107") // suppress Sonar check on parameters count
  public FileVo(long id, String login, LocalDateTime saveDate, String businessKey, long operationId,
                FileType type, Language language, String fileName, String managementEntity, byte[] fileContent) {
    super(id, login, saveDate);
    this.businessKey = businessKey;
    this.operationId = operationId;
    this.type = type;
    this.language = language;
    this.fileName = fileName;
    this.managementEntity = managementEntity;
    this.fileContent = fileContent;
  }

  /**
   * other constructor
   */
  public FileVo(String fileName, FileType fileType, String managementEntity, byte[] fileContent) {
    this(null, 0, fileType, null, fileName, managementEntity, fileContent);
  }

  /**
   * other constructor
   */
  public FileVo(String businessKey, long operationId, FileType type, Language language, String fileName,
                String managementEntity, byte[] fileContent) {
    this(0, null, null, businessKey, operationId, type, language, fileName, managementEntity, fileContent);
  }

  /**
   * other constructor
   */
  public FileVo(String fileName, byte[] fileContent) {
    this(0, null, null, null, 0, null, null, fileName, null, fileContent);
  }
}
