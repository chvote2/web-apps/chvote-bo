/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.document;

import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.file.FileService;
import ch.ge.ve.bo.service.file.exception.InvalidFileException;
import ch.ge.ve.bo.service.file.model.FileVo;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import org.apache.commons.io.IOUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * Implementation of the {@link OperationDocumentService} interface.
 */
public class OperationDocumentServiceImpl implements OperationDocumentService {

  /**
   * List of application file types which are considered as operation documents
   */
  private static final List<FileType> OPERATION_DOCUMENT_TYPES =
      Arrays.asList(FileType.DOCUMENT_FAQ, FileType.DOCUMENT_TERMS, FileType.DOCUMENT_CERTIFICATE);

  private FileService fileService;

  /**
   * Default constructor
   */
  public OperationDocumentServiceImpl(FileService fileService) {
    this.fileService = fileService;
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public FileVo importFile(long operationId, FileType type, Language language, String fileName, InputStream inputStream)
      throws BusinessException {
    Assert.isTrue(OPERATION_DOCUMENT_TYPES.contains(type), String.format("file type not supported : %s", type));

    try {
      // check that the file is a PDF file
      if (!fileName.toLowerCase(Locale.ENGLISH).endsWith(FileVo.PDF_EXTENSION)) {
        throw new InvalidFileException("global.errors.not-pdf-file");
      }

      return fileService.importFile(
          new FileVo(
              fileService.buildDocumentBusinessKey(operationId, type, language),
              operationId,
              type,
              language,
              fileName,
              ConnectedUser.get().managementEntity,
              IOUtils.toByteArray(inputStream)
          ), false);

    } catch (IOException e) {
      throw new TechnicalException(e);
    }
  }

  @Override
  @Transactional(readOnly = true)
  public FileVo getFile(long fileId) {
    return fileService.getFile(fileId);
  }

  @Override
  public List<FileVo> getFiles(long operationId) {
    return fileService.getFiles(operationId, false, OPERATION_DOCUMENT_TYPES)
                      .stream()
                      .filter(applicationFileVo -> OPERATION_DOCUMENT_TYPES.contains(applicationFileVo.type))
                      .collect(Collectors.toList());
  }
}
