/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model;

import ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * Voting material possible status
 */
public class VotingMaterialStatusVo {

  public enum State {
    INCOMPLETE(false),
    COMPLETE(false),
    AVAILABLE_FOR_CREATION(true),
    CREATION_REQUESTED(true),
    CREATION_REJECTED(false),
    CREATION_IN_PROGRESS(true),
    CREATION_FAILED(false),
    CREATED(true),
    VALIDATED(true),
    INVALIDATION_REQUESTED(true),
    INVALIDATION_REJECTED(true),
    INVALIDATED(false);

    private final boolean readOnly;

    State(boolean readOnly) {
      this.readOnly = readOnly;
    }

  }

  private final VotingMaterialStatusVo.State state;


  private final String                         comment;
  private final LocalDateTime                  stateDate;
  private final String                         stateUser;
  private final String                         pactUrl;
  private final Map<String, Boolean>           completedSections;
  private final Map<String, String>            votingCardLocations;
  private final Map<String, Boolean>           sectionsInError = new HashMap<>();
  private final VotingMaterialCreationProgress progress;
  private final boolean                        nonPrintableCardsAvailable;

  /**
   * Constructor used when status information is provided by PACT
   */
  public VotingMaterialStatusVo(State state, LocalDateTime stateDate,
                                Map<String, Boolean> completedSections, String stateUser) {
    this.state = state;
    this.stateDate = stateDate;
    this.stateUser = stateUser;
    this.completedSections = completedSections;
    this.comment = null;
    this.pactUrl = null;
    this.votingCardLocations = null;
    this.progress = null;
    this.nonPrintableCardsAvailable = false;
  }

  /**
   * Constructor used when status information is provided by BO
   */
  public VotingMaterialStatusVo(VotingMaterialsStatusVo status, Map<String, Boolean> completeBySection,
                                String virtualPrinterId) {

    this.state = VotingMaterialStatusVo.State.valueOf(status.getState().name());
    this.comment = status.getRejectionReason();
    this.stateDate = status.getLastChangeDate();
    this.pactUrl = getPactUrl(status);
    this.stateUser = status.getLastChangeUser();
    this.completedSections = completeBySection;
    this.votingCardLocations = status.getVotingCardLocations();

    if (status.getProtocolProgress() != null &&
        status.getState() == VotingMaterialsStatusVo.State.CREATION_IN_PROGRESS) {
      progress = new VotingMaterialCreationProgress(status.getProtocolProgress());
    } else {
      progress = null;
    }
    this.nonPrintableCardsAvailable = isNonPrintableCardsAvailable(virtualPrinterId);
  }

  private String getPactUrl(VotingMaterialsStatusVo status) {
    return (status.getState() == VotingMaterialsStatusVo.State.CREATED ||
            status.getState() == VotingMaterialsStatusVo.State.INVALIDATION_REJECTED ||
            status.getState() == VotingMaterialsStatusVo.State.INVALIDATION_REQUESTED) ?
        status.getInvalidationPageUrl() : status.getConfigurationPageUrl();
  }

  public boolean isReadOnly() {
    return state.readOnly;
  }


  void markSectionInError(String section) {
    sectionsInError.put(section, true);
  }

  private boolean isNonPrintableCardsAvailable(String vprId) {
    return this.votingCardLocations != null && this.votingCardLocations.containsKey(vprId);
  }

  public State getState() {
    return state;
  }

  @JsonIgnore
  public String getComment() {
    return comment;
  }

  @JsonIgnore
  public LocalDateTime getStateDate() {
    return stateDate;
  }

  @JsonIgnore
  public String getStateUser() {
    return stateUser;
  }

  public String getPactUrl() {
    return pactUrl;
  }

  public Map<String, Boolean> getCompletedSections() {
    return completedSections;
  }

  public Map<String, String> getVotingCardLocations() {
    return votingCardLocations;
  }

  public Map<String, Boolean> getSectionsInError() {
    return sectionsInError;
  }

  public VotingMaterialCreationProgress getProgress() {
    return progress;
  }

  public boolean isNonPrintableCardsAvailable() {
    return nonPrintableCardsAvailable;
  }

}
