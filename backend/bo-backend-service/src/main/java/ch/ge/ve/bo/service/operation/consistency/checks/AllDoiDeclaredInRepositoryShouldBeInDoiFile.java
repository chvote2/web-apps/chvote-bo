/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.consistency.checks;

import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.service.operation.consistency.AbstractConsistencyChecker;
import ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo.ConsistencyErrorType;
import ch.ge.ve.bo.service.operation.model.ConsistencyResultVo;
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryService;
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryServiceImpl;

/**
 * Check to verify all doi declared in repository are in doi file
 */
public class AllDoiDeclaredInRepositoryShouldBeInDoiFile extends AbstractConsistencyChecker {

  private final OperationRepositoryService repositoryService;

  /**
   * Default constructor
   */
  public AllDoiDeclaredInRepositoryShouldBeInDoiFile(OperationRepositoryService repositoryService) {
    this.repositoryService = repositoryService;
  }

  @Override
  public CheckType getCheckType() {
    return CheckType.CONFIGURATION;
  }


  @Override
  public ConsistencyResultVo checkForOperation(Operation operation) {
    ConsistencyResultVo consistencyResultVo = new ConsistencyResultVo();

    repositoryService
        .getAllDomainOfInfluenceForOperation(operation.getId())
        .stream()
        .filter(doi -> doi.type.equals(OperationRepositoryServiceImpl.UNKNOWN_DOI_TYPE))
        .forEach(unknownDoi ->
                     consistencyResultVo.addError(ConsistencyErrorType.CONF_UNKNOWN_OPERATION_REPOSITORY_DOI,
                                                  ConsistencyResultVo.param("unknownDoi", unknownDoi.id))
        );
    return consistencyResultVo;
  }

}
