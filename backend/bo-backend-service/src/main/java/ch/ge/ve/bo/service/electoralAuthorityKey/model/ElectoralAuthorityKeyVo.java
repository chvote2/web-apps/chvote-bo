/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.electoralAuthorityKey.model;

import ch.ge.ve.bo.repository.entity.ElectoralAuthorityKey;
import ch.ge.ve.bo.service.model.BaseVo;
import ch.ge.ve.bo.service.utils.HashUtils;
import java.time.LocalDateTime;

/**
 * Value object representing an electoral authority key
 */
public class ElectoralAuthorityKeyVo extends BaseVo {

  public final String        managementEntity;
  public final String        label;
  public final LocalDateTime importDate;
  public final String        keyHash;

  /**
   * Default constructor
   */
  public ElectoralAuthorityKeyVo(long id, String login, LocalDateTime saveDate, String managementEntity,
                                 String label, LocalDateTime importDate, String keyHash) {
    super(id, login, saveDate);
    this.managementEntity = managementEntity;
    this.label = label;
    this.importDate = importDate;
    this.keyHash = keyHash;
  }

  /**
   * constructor using entity
   */
  public ElectoralAuthorityKeyVo(ElectoralAuthorityKey key) {
    this(key.getId(), key.getLogin(), key.getSaveDate(), key.getManagementEntity(), key.getKeyName(),
         key.getImportDate(), HashUtils.publicKeyToHexHash(key.getKeyContent()));
  }

}
