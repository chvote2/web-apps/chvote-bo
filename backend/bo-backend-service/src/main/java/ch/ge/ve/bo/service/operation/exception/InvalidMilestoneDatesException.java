/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.exception;

import ch.ge.ve.bo.service.exception.FieldsValidationException;
import java.util.Map;

/**
 * Exception thrown when invalid operation milestone dates are detected.
 */
public class InvalidMilestoneDatesException extends FieldsValidationException {

  public static final String IN_FUTURE                 = "parameters.milestone.edit.form.errors.past-date";
  public static final String BEFORE_OPERATION          = "parameters.milestone.edit.form.errors.milestone-date-must-be-before-operation";
  public static final String BEFORE_OR_EQUAL_OPERATION = "parameters.milestone.edit.form.errors.milestone-date-must-be-before-or-equal-operation";
  public static final String AFTER_OPERATION           = "parameters.milestone.edit.form.errors.milestone-date-must-be-after-operation";
  public static final String AFTER_OR_EQUAL_OPERATION  = "parameters.milestone.edit.form.errors.milestone-date-must-be-after-or-equal-operation";
  public static final String CERTIFICATION_DATE        = "parameters.milestone.edit.form.errors.certification-date";
  public static final String PRINTER_FILE_DATE         = "parameters.milestone.edit.form.errors.printer-files-date";
  public static final String BALLOT_BOX_INIT_DATE      = "parameters.milestone.edit.form.errors.ballot-box-init-date";
  public static final String SITE_OPEN_DATE            = "parameters.milestone.edit.form.errors.site-open-date";
  public static final String SITE_CLOSE_DATE           = "parameters.milestone.edit.form.errors.site-close-date";
  public static final String BALLOT_BOX_DECRYPT_DATE   = "parameters.milestone.edit.form.errors.ballot-box-decrypt-date";
  public static final String RESULT_VALIDATION_DATE    = "parameters.milestone.edit.form.errors.result-validation-date";
  public static final String DATA_DESTRUCTION_DATE     = "parameters.milestone.edit.form.errors.data-destruction-date";

  /**
   * Construct a new exception.
   *
   * @param errorMessages a mapping between the milestones in error and their associated error message
   */
  public InvalidMilestoneDatesException(Map<String, String> errorMessages) {
    super(errorMessages);
  }
}
