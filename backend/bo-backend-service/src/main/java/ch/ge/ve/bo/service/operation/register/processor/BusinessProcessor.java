/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register.processor;

import ch.ge.ve.bo.service.operation.register.RegisterImportProcessor;
import ch.ge.ve.bo.service.operation.register.exception.BusinessValidationException;
import ch.ge.ve.bo.service.operation.register.exception.BusinessValidationUnknownFailure;
import ch.ge.ve.bo.service.operation.register.exception.InvalidNumberOfVoterException;
import ch.ge.ve.bo.service.operation.register.model.FileInfo;
import ch.ge.ve.bo.service.operation.register.model.VoterRefTable;
import java.io.InputStream;
import java.util.Set;
import java.util.concurrent.Executor;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * Processor responsible to validate business rules
 */
public class BusinessProcessor extends RegisterImportProcessor {

  private final BusinessProcessorContext context;

  /**
   * Default constructor
   */
  public BusinessProcessor(InputStream streamToProcess,
                           Executor executor,
                           FileInfo fileInfo,
                           VoterRefTable voterRefTable,
                           Set<String> alreadyImported,
                           Set<String> operationDomainOfInfluence) {
    super(streamToProcess, executor);
    context = new BusinessProcessorContext(voterRefTable, fileInfo, alreadyImported, operationDomainOfInfluence);
  }


  @Override
  protected void process() throws BusinessValidationException {
    XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
    try {
      XMLStreamReader xmlStreamReader = xmlInputFactory.createXMLStreamReader(streamToProcess);
      while (xmlStreamReader.hasNext() && !isCancelled()) {
        int eventType = xmlStreamReader.next();
        if (eventType == XMLStreamConstants.START_ELEMENT) {
          enteringElement(xmlStreamReader);
        } else if (eventType == XMLStreamConstants.END_ELEMENT) {
          exitingElement();
        }
      }
      xmlStreamReader.close();
    } catch (XMLStreamException e) {
      throw new BusinessValidationUnknownFailure(e);
    }


    if (context.getActualNumberOfVoter() != context.getExpectedNumberOfVoter()) {
      throw new InvalidNumberOfVoterException(context.getActualNumberOfVoter(), context.getExpectedNumberOfVoter());
    }

  }

  private void enteringElement(XMLStreamReader xmlStreamReader) throws XMLStreamException {
    context.navigateTo(xmlStreamReader.getName().getLocalPart());
    BusinessProcessorRules.enter(context, xmlStreamReader);
  }

  private void exitingElement() throws BusinessValidationException {
    BusinessProcessorRules.exit(context);
    context.navigateBack();
  }

  public String getMessageUniqueId() {
    return context.getUniqueMessageIdContext().getMessageUniqueId();
  }

  public BusinessProcessorContext getContext() {
    return context;
  }
}
