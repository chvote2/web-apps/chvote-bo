/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.dictionnary;

import ch.ge.ve.bo.repository.entity.DictionaryDataType;
import java.util.List;

/**
 * Service to access dictionary entry
 */
public interface DictionaryService {

  /**
   * @return All available dictionary entries for a dataType a given class and an realm and a management entity
   */
  <T> List<T> get(DictionaryDataType dataType, Class<T> objectClass, String realm, String managementEntity);

  /**
   * @return All available dictionary entries for a dataType a given class and an operation id (used to find realm)
   */
  <T> List<T> get(DictionaryDataType dataType, Class<T> objectClass, Long operationId);

  /**
   * @return one dictionary entries for a dataType a given class and an operation id (used to find realm)
   */
  <T> T getOne(DictionaryDataType dataType, Class<T> objectClass, Long operationId);
}

