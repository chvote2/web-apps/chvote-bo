/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register;

import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.file.model.FileVo;
import ch.ge.ve.bo.service.operation.register.model.ImportRegisterResult;
import java.io.InputStream;
import java.util.List;

/**
 * Service for importing register
 */
public interface RegisterImportService {


  /**
   * Import a register for a given operation
   */
  ImportRegisterResult importRegister(Long operationId, String name, InputStream inputStream) throws BusinessException;

  /**
   * Validate an imported register
   */
  void validateImport(Long operationId, String messageUniqueId) throws BusinessException;

  /**
   * Get all import reports for a given operation
   *
   * @param operationId operation id to retrieve reports for
   *
   * @return Reports as Json string
   */
  List<String> getReports(long operationId);

  /**
   * Remove imported register
   */
  void deleteImport(long operationId, String messageUniqueId);

  /**
   * Returns the PDF detailed report built from the register specified by the given parameters
   *
   * @param operationId     operation id
   * @param messageUniqueId messageUniqueId of the register
   *
   * @return Report as PDF
   */
  FileVo getDetailedReportPDF(long operationId, String messageUniqueId);

  /**
   * Returns the PDF detailed consolidated report built from the registers specified by operation id
   *
   * @param operationId operation id
   *
   * @return Report as PDF
   */
  FileVo getDetailedReportPDF(long operationId);
}
