/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.doi;

import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceFileVo;
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceImportResult;
import java.io.InputStream;

/**
 * Operation's domain of influence services.
 */
public interface OperationDomainInfluenceService {

  /**
   * Import a domain of influence for the given operation.
   *
   * @param operationId concerned operation's unique ID
   * @param fileName    name's file
   * @param inputStream content's file
   *
   * @return the imported file informations
   *
   * @throws BusinessException if an error occured during the import
   */
  DomainOfInfluenceImportResult importFile(long operationId, String fileName, InputStream inputStream) throws BusinessException;

  /**
   * Retrieves the domain of influence file linked to the given operation ID.
   *
   * @param operationId the concerned operation's ID
   * @param withContent if true, also return the file's content
   *
   * @return the domain of influence file associated to this operation or null if no file was imported for this
   * operation
   */
  DomainOfInfluenceFileVo getFile(long operationId, boolean withContent);
}
