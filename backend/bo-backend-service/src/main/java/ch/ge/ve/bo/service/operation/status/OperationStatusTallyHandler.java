/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.status;

import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.model.PactConfiguration;
import ch.ge.ve.bo.service.operation.download.token.DownloadTokenService;
import ch.ge.ve.bo.service.operation.exception.PactRequestException;
import ch.ge.ve.bo.service.operation.model.TallyArchiveStatusVo;
import ch.ge.ve.bo.service.operation.model.VotingPeriodStatusVo;
import java.nio.file.Path;
import java.nio.file.Paths;
/**
 * Service responsible of computing tally status
 */
public class OperationStatusTallyHandler {
  private final DownloadTokenService         downloadTokenService;
  private final OperationStatusHandlerHelper helper;
  private final PactConfiguration            pactConfiguration;

  /**
   * Default constructor
   */
  public OperationStatusTallyHandler(DownloadTokenService downloadTokenService,
                                     OperationStatusHandlerHelper helper,
                                     PactConfiguration pactConfiguration) {
    this.downloadTokenService = downloadTokenService;
    this.helper = helper;
    this.pactConfiguration = pactConfiguration;
  }

  /**
   * compute tally status
   */
  public TallyArchiveStatusVo getTallyArchiveStatusVo(VotingPeriodStatusVo.State state, Operation operation)
      throws PactRequestException {
    if (VotingPeriodStatusVo.State.CLOSED != state) {
      return TallyArchiveStatusVo.notRequested();
    }

    ch.ge.ve.chvote.pactback.contract.operation.status.TallyArchiveStatusVo pactStatus = helper.doStatusRequest(
        ch.ge.ve.chvote.pactback.contract.operation.status.TallyArchiveStatusVo.class,
        operation.getId(),
        pactConfiguration.getUrl().getTallyArchiveStatus());
    if (pactStatus == null) {
      throw new TechnicalException("operation " + operation.getId() +
                                   " Voting period is closed but PACT returned 404 on tally status request");

    }

    return new TallyArchiveStatusVo(TallyArchiveStatusVo.State.valueOf(pactStatus.getState().name()),
                                    createDownloadTokenForAuthorizedUsers(operation,
                                                                          pactStatus.getTallyArchiveLocation()));
  }


  private String createDownloadTokenForAuthorizedUsers(Operation operation, String tallyArchiveLocation) {
    if (tallyArchiveLocation == null || !ConnectedUser.get().roles.contains("ROLE_DOWNLOAD_TALLY_ARCHIVE")) {
      return null;
    }

    Path path = Paths.get(pactConfiguration.getVotingMaterialBaseLocation(), tallyArchiveLocation);

    return downloadTokenService.getOrCreateDownloadToken(operation, path.toString()).getId();
  }
}
