/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.model.printer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Configuration of all printer and all mappings
 */
public class PrinterTemplateVo {
  public final String                       templateName;
  public final PrinterConfiguration[]       printerConfigurations;
  public final PrinterMunicipalityMapping[] printerMunicipalityMappings;
  public final String                       printerIdForPrintableTestingCard;
  public final PrinterSwissAbroadMapping    printerSwissAbroadMapping;


  /**
   * Default constructor
   */
  @JsonCreator()
  public PrinterTemplateVo(
      @JsonProperty("templateName") String templateName,
      @JsonProperty("printerConfigurations") PrinterConfiguration[] printerConfigurations,
      @JsonProperty("printerMunicipalityMappings") PrinterMunicipalityMapping[] printerMunicipalityMappings,
      @JsonProperty("printerIdForPrintableTestingCard") String printerIdForPrintableTestingCard,
      @JsonProperty("printerSwissAbroadMapping") PrinterSwissAbroadMapping printerSwissAbroadMapping) {
    this.templateName = templateName;
    this.printerConfigurations = printerConfigurations;
    this.printerMunicipalityMappings = printerMunicipalityMappings;
    this.printerIdForPrintableTestingCard = printerIdForPrintableTestingCard;
    this.printerSwissAbroadMapping = printerSwissAbroadMapping;
  }

  public String getTemplateName() {
    return templateName;
  }

}
