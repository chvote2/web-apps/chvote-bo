/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.printer;

import ch.ge.ve.bo.service.conf.Canton;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.model.printer.PrinterConfiguration;
import ch.ge.ve.bo.service.model.printer.PrinterTemplateVo;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation of {@link PrinterTemplateService}
 */
public class PrinterTemplateServiceImpl implements PrinterTemplateService {
  private static final Logger  log           = LoggerFactory.getLogger(PrinterTemplateServiceImpl.class);
  private static final Charset CHARSET_UTF_8 = Charset.forName("UTF-8");

  private final Map<String, PrinterTemplateVo[]> allPrinterTemplates;
  private final PrinterConfiguration             virtualPrinterForTestCard;

  /**
   * Default constructor
   */
  public PrinterTemplateServiceImpl(ObjectMapper objectMapper) throws IOException {
    virtualPrinterForTestCard = objectMapper.readValue(
        IOUtils.toString(getClass().getResourceAsStream("/virtualPrinterForTestCard.json"), CHARSET_UTF_8),
        PrinterConfiguration.class);
    allPrinterTemplates = Stream
        .of(Canton.values())
        .collect(Collectors.toMap(
            Enum::name,
            canton -> {
              InputStream is = getClass()
                  .getResourceAsStream("/" + canton.name().toLowerCase(Locale.US) + "/PrinterTemplates.json");
              if (is != null) {
                try {
                  return
                      objectMapper.readValue(IOUtils.toString(is, CHARSET_UTF_8), PrinterTemplateVo[].class);
                } catch (IOException e) {
                  log.debug("Cannot read template for canton " + canton, e);
                }
              }
              return new PrinterTemplateVo[0];
            }));
  }

  @Override
  public PrinterConfiguration getVirtualPrinterForTestCard() {
    return virtualPrinterForTestCard;
  }

  @Override
  public PrinterTemplateVo[] getAllPrinterTemplates(String canton) {
    return allPrinterTemplates.get(canton);
  }

  @Override
  public PrinterTemplateVo[] getPrinterTemplatesByPrinterId(String printerId) {
    return allPrinterTemplates
        .values().stream().flatMap(Stream::of)
        .filter(printerTemplateVo ->
                    Stream.of(printerTemplateVo.printerConfigurations).anyMatch(
                        printerConfiguration -> printerId.equals(printerConfiguration.id)))
        .toArray(PrinterTemplateVo[]::new);
  }

  @Override
  public PrinterTemplateVo getPrinterTemplate(String canton, String printerTemplateName) {
    return Stream.of(getAllPrinterTemplates(canton))
                 .filter(printerTemplateVo -> printerTemplateName.equals(printerTemplateVo.templateName)).findFirst()
                 .orElseThrow(() -> new TechnicalException("no such template " + printerTemplateName));
  }

}
