/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.status;

import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.repository.entity.OperationParameterType;
import ch.ge.ve.bo.repository.entity.WorkflowStep;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.model.PactConfiguration;
import ch.ge.ve.bo.service.operation.exception.PactRequestException;
import ch.ge.ve.bo.service.operation.model.VotingPeriodStatusVo;
import java.util.Map;

/**
 * Service responsible of computing voting period status status
 */
public class OperationStatusVotingPeriodHandler {

  private final OperationStatusHandlerHelper helper;
  private final PactConfiguration            pactConfiguration;

  /**
   * Default constructor
   */
  public OperationStatusVotingPeriodHandler(OperationStatusHandlerHelper helper, PactConfiguration pactConfiguration) {
    this.helper = helper;
    this.pactConfiguration = pactConfiguration;
  }

  /**
   * compute voting period status
   */
  public VotingPeriodStatusVo getVotingPeriodStatusVo(Operation operation) throws PactRequestException {
    Map<String, Boolean> completedSections =
        helper.computeIsCompleteBySection(operation, OperationParameterType.VOTING_PERIOD);

    VotingPeriodStatusVo notSentStatus = VotingPeriodStatusVo
        .notSent(completedSections, operation.getLastVotingPeriodConfigUpdateDate(), operation.getLogin());

    if (operation.getWorkflowStep().ordinal() < WorkflowStep.VOTING_PERIOD_CONF_SENT.ordinal()) {
      return notSentStatus;
    }

    ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo pactStatus =
        helper.doStatusRequest(
            ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo.class,
            operation.getId(),
            pactConfiguration.getUrl().getVotingPeriodStatus());
    if (pactStatus == null) {
      throw new TechnicalException("operation " + operation.getId() + " status is " + operation
          .getWorkflowStep() + " but PACT returned 404 on voting period status request");
    }


    if (pactStatus.getLastChangeDate().isAfter(operation.getLastVotingPeriodConfigUpdateDate())) {
      return new VotingPeriodStatusVo(VotingPeriodStatusVo.State.valueOf(pactStatus.getState().name()),
                                      completedSections,
                                      pactStatus.getLastChangeDate(),
                                      pactStatus.getLastChangeUser(),
                                      pactStatus.getConfigurationPageUrl(),
                                      pactStatus.getRejectionReason());
    }

    return notSentStatus;
  }
}
