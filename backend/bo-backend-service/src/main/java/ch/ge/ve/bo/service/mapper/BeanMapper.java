/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.mapper;

import ch.ge.ve.bo.repository.entity.File;
import ch.ge.ve.bo.repository.entity.Milestone;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.repository.entity.VoterTestingCardsLot;
import ch.ge.ve.bo.service.file.model.FileVo;
import ch.ge.ve.bo.service.model.VoterTestingCardsLotVo;
import ch.ge.ve.bo.service.operation.model.MilestoneVo;
import ch.ge.ve.bo.service.operation.model.OperationVo;
import java.util.stream.Collectors;

/**
 * Class offering mapping services between entity objects and their corresponding model object.
 */
public class BeanMapper {

  private BeanMapper() {
  }

  /**
   * Map an {@link File} entity to its corresponding {@link FileVo} model.
   *
   * @param file        the application file entity
   * @param withContent true if the file's content should be included
   *
   * @return the corresponding application file model
   */
  public static FileVo map(File file, boolean withContent) {
    return new FileVo(
        file.getId(),
        file.getLogin(),
        file.getSaveDate(),
        file.getBusinessKey(),
        file.getOperation().getId(),
        file.getType(),
        file.getLanguage(),
        file.getFileName(),
        file.getManagementEntity(),
        withContent ? file.getFileContent() : null
    );
  }

  /**
   * Map an {@link Operation} entity to its corresponding {@link OperationVo} model.
   *
   * @param operation      the operation entity
   * @return the corresponding operation model
   */
  public static OperationVo map(Operation operation) {
    return new OperationVo(
        operation.getId(),
        operation.getLogin(),
        operation.getSaveDate(),
        operation.getShortLabel(),
        operation.getLongLabel(),
        operation.getDate(),
        operation.getGracePeriod(),
        operation.getMilestones().stream().map(BeanMapper::map).collect(Collectors.toList()),
        operation.getSaveDate(),
        operation.getSimulationName(),
        operation.getDeploymentTarget(),
        operation.getLastConfigurationUpdateDate(),
        operation.getVotingCardTitle(),
        operation.getPrinterTemplate(),
        operation.getManagementEntity(),
        operation.getGuestManagementEntities()
        );
  }

  /**
   * Map a {@link Milestone} entity to its corresponding {@link MilestoneVo} model.
   *
   * @param milestone the milestone entity
   *
   * @return the corresponding milestone model
   */
  public static MilestoneVo map(Milestone milestone) {
    return new MilestoneVo(
        milestone.getId(),
        milestone.getLogin(),
        milestone.getSaveDate(),
        milestone.getType(),
        milestone.getDate()
    );
  }

  /**
   * Map a {@link VoterTestingCardsLot} entity to its value object
   */
  public static VoterTestingCardsLotVo map(VoterTestingCardsLot lot) {
    return new VoterTestingCardsLotVo(lot.getId(), lot.getLotName(), lot.getCardType(), lot.getCount(),
                                      lot.getSignature(), lot.getFirstName(), lot.getLastName(), lot.getBirthday(),
                                      lot.getAddress1(), lot.getAddress2(), lot.getStreet(), lot.getPostalCode(),
                                      lot.getCity(), lot.getCountry(), lot.getLanguage(), lot.isShouldPrint(),
                                      lot.getDoi());
  }
}
