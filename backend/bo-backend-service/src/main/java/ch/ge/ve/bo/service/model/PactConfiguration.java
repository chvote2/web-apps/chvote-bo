/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.model;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Pact configuration instantiated by spring
 */
@Configuration
@ConfigurationProperties(prefix = "pact")
public class PactConfiguration {

  private String username;
  private String password;
  private String votingMaterialBaseLocation = "";
  private Urls   url                        = new Urls();

  public Urls getUrl() {
    return url;
  }


  public String getVotingMaterialBaseLocation() {
    return votingMaterialBaseLocation;
  }

  public void setVotingMaterialBaseLocation(String votingMaterialBaseLocation) {
    this.votingMaterialBaseLocation = votingMaterialBaseLocation;
  }

  public void setUrl(Urls url) {
    this.url = url;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public static class Urls {
    private String operationConfigurationStatus;
    private String invalidateTestSite;
    private String validateTestSite;
    private String operationConfigurationUpload;
    private String votingMaterialUpload;
    private String votingMaterialStatus;
    private String votingPeriodUpload;
    private String votingPeriodStatus;
    private String validateVotingMaterial;
    private String tallyArchiveStatus;
    private String triggerTallyArchive;

    public void setVotingPeriodUpload(String votingPeriodUpload) {
      this.votingPeriodUpload = votingPeriodUpload;
    }

    public String getVotingPeriodStatus() {
      return votingPeriodStatus;
    }

    public void setVotingPeriodStatus(String votingPeriodStatus) {
      this.votingPeriodStatus = votingPeriodStatus;
    }

    public String getOperationConfigurationStatus() {
      return operationConfigurationStatus;
    }

    public void setOperationConfigurationStatus(String operationConfigurationStatus) {
      this.operationConfigurationStatus = operationConfigurationStatus;
    }

    public String getInvalidateTestSite() {
      return invalidateTestSite;
    }

    public void setInvalidateTestSite(String invalidateTestSite) {
      this.invalidateTestSite = invalidateTestSite;
    }

    public String getValidateTestSite() {
      return validateTestSite;
    }

    public void setValidateTestSite(String validateTestSite) {
      this.validateTestSite = validateTestSite;
    }

    public String getOperationConfigurationUpload() {
      return operationConfigurationUpload;
    }

    public void setOperationConfigurationUpload(String operationConfigurationUpload) {
      this.operationConfigurationUpload = operationConfigurationUpload;
    }

    public String getVotingMaterialUpload() {
      return votingMaterialUpload;
    }

    public void setVotingMaterialUpload(String votingMaterialUpload) {
      this.votingMaterialUpload = votingMaterialUpload;
    }

    public String getVotingMaterialStatus() {
      return votingMaterialStatus;
    }

    public void setVotingMaterialStatus(String votingMaterialStatus) {
      this.votingMaterialStatus = votingMaterialStatus;
    }

    public String getVotingPeriodUpload() {
      return votingPeriodUpload;
    }

    public String getValidateVotingMaterial() {
      return validateVotingMaterial;
    }

    public void setValidateVotingMaterial(String validateVotingMaterial) {
      this.validateVotingMaterial = validateVotingMaterial;
    }

    public String getTallyArchiveStatus() {
      return tallyArchiveStatus;
    }

    public void setTallyArchiveStatus(String tallyArchiveStatus) {
      this.tallyArchiveStatus = tallyArchiveStatus;
    }

    public String getTriggerTallyArchive() {
      return triggerTallyArchive;
    }

    public void setTriggerTallyArchive(String triggerTallyArchive) {
      this.triggerTallyArchive = triggerTallyArchive;
    }
  }
}
