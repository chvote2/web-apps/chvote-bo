/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.file.exception;

import ch.ge.ve.bo.service.exception.BusinessException;

/**
 * Exception thrown when the application file is invalid.
 */
public class InvalidFileException extends BusinessException {

  /**
   * Default constructor with no parameters
   */
  public InvalidFileException(String errorKey) {
    super(errorKey);
  }

  /**
   * Default constructor
   */
  public InvalidFileException(String errorKey, String... params) {
    super(errorKey, params);
  }
}
