/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.scaffold;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class EntityField {
  private static final Map<String, String> JAVA_TO_TS_TYPE = new HashMap<>() {{
    put("java.lang.String", "string");
    put("java.time.LocalDateTime", "Date");
    put("java.lang.Long", "number");
    put("int", "number");
    put("java.lang.Integer", "number");
    put("long", "number");
    put("java.lang.Long", "number");
    put("long", "number");
    put("java.lang.Double", "number");
    put("double", "number");
    put("java.lang.Float", "number");
    put("float", "number");
    put("java.lang.Boolean", "boolean");
    put("boolean", "boolean");
  }};

  private final String  name;
  private final String  humanName;
  private final String  enumType;
  private final String  tsType;
  private final String  reader;
  private final boolean required;
  private       boolean dateWithTime;
  private final int     minLength;
  private final int     maxLength;

  public EntityField(Field field, TemplateContext context) {
    name = field.getName();
    humanName = Utils.toHumanName(name);
    if (Enum.class.isAssignableFrom(field.getType())) {
      Object[] c = field.getType().getEnumConstants();
      enumType = field.getType().getSimpleName();
      context.addEnum(enumType, Stream.of(c).map(Object::toString).collect(Collectors.toList()));
      tsType = "string";
    } else {
      enumType = null;
      tsType = map(field.getType(), name);
    }


    required = field.getDeclaredAnnotation(NotNull.class) != null;
    Size size = field.getDeclaredAnnotation(Size.class);
    if (size != null) {
      minLength = size.min();
      maxLength = size.max() == Integer.MAX_VALUE ? 0 : size.max();
    } else {
      minLength = 0;
      maxLength = 0;
    }


    try {
      reader = Stream.of(Introspector.getBeanInfo(field.getDeclaringClass()).getPropertyDescriptors())
                     .filter(propertyDescriptor -> propertyDescriptor.getName().equals(name))
                     .findFirst().map(pd -> pd.getReadMethod().getName() + "()")
                     .orElse(name);
    } catch (IntrospectionException e) {
      throw new RuntimeException(e);
    }


  }

  private String map(Class<?> type, String name) {
    return Optional.ofNullable(JAVA_TO_TS_TYPE.get(type.getName()))
                   .orElseThrow(() -> new IllegalArgumentException("field " + name + " is of type " + type.getName()));
  }

  public String getName() {
    return name;
  }

  public String getGetter() {
    return reader;
  }


  public String getHumanName() {
    return humanName;
  }

  public String getTsType() {
    return tsType;
  }


  public boolean isRequired() {
    return required;
  }

  public int getMinLength() {
    return minLength;
  }

  public int getMaxLength() {
    return maxLength;
  }

  public boolean isEnum() {
    return enumType != null;
  }

  public String getEnumType() {
    return enumType;
  }

  public boolean isDateWithTime() {
    return dateWithTime;
  }

  public void setDateWithTime(boolean dateWithTime) {
    this.dateWithTime = dateWithTime;
  }
}
