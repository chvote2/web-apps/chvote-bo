/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.scaffold;

import static java.util.stream.Stream.concat;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.io.FileUtils;

public class CrudGenerator {
  private final Config        config;
  private final Configuration cfg;
  private final Template      entityTsTemplate;
  private final Template      tsServiceTemplate;
  private final Template      resolverTsTemplate;
  private final Template      listTsTemplate;
  private final Template      i18nJsonTemplate;
  private final Template      listHtmlTemplate;
  private final Template      listScssTemplate;
  private final Template      moduleTemplate;
  private final Template      routingModuleTemplate;
  private final Template      detailsScssTemplate;
  private final Template      detailsHtmlTemplate;
  private final Template      detailsTsTemplate;
  private final Template      viewScssTemplate;
  private final Template      viewHtmlTemplate;
  private final Template      viewTsTemplate;
  private final Template      controllerTemplate;
  private final Template      javaServiceTemplate;
  private final Template      javaMockServiceTemplate;
  private final Template      readmeTemplate;

  public CrudGenerator(Config config) throws IOException {

    this.config = config;
    cfg = new Configuration(Configuration.VERSION_2_3_23);
    cfg.setClassLoaderForTemplateLoading(CrudGenerator.class.getClassLoader(), "");
    cfg.setDefaultEncoding("UTF-8");
    cfg.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
    cfg.setLogTemplateExceptions(false);
    entityTsTemplate = cfg.getTemplate("entity.ts.ftl");
    tsServiceTemplate = cfg.getTemplate("service.ts.ftl");
    javaServiceTemplate = cfg.getTemplate("service.java.ftl");
    javaMockServiceTemplate = cfg.getTemplate("serviceMockImpl.java.ftl");
    resolverTsTemplate = cfg.getTemplate("resolver.ts.ftl");
    i18nJsonTemplate = cfg.getTemplate("i18n.json.ftl");
    listTsTemplate = cfg.getTemplate("list.component.ts.ftl");
    listHtmlTemplate = cfg.getTemplate("list.component.html.ftl");
    listScssTemplate = cfg.getTemplate("list.component.scss.ftl");
    detailsTsTemplate = cfg.getTemplate("details.component.ts.ftl");
    detailsHtmlTemplate = cfg.getTemplate("details.component.html.ftl");
    detailsScssTemplate = cfg.getTemplate("details.component.scss.ftl");
    viewTsTemplate = cfg.getTemplate("view.component.ts.ftl");
    viewHtmlTemplate = cfg.getTemplate("view.component.html.ftl");
    viewScssTemplate = cfg.getTemplate("view.component.scss.ftl");
    moduleTemplate = cfg.getTemplate("module.ts.ftl");
    routingModuleTemplate = cfg.getTemplate("routing.module.ts.ftl");
    controllerTemplate = cfg.getTemplate("controller.java.ftl");


    readmeTemplate = cfg.getTemplate("readme.txt.ftl");

  }

  public TemplateContext prepare(Class<?> clazz) {
    return new TemplateContext(clazz, config.restControllerPackage, config.javaServicePackage);
  }


  public void generate(TemplateContext ctx) throws IOException, TemplateException {
    templateToFile(controllerTemplate, ctx,
                   ctx.getClassName() + "Controller.java",
                   config.restControllerDir, ctx.getObjectName());

    templateToFile(javaServiceTemplate, ctx,
                   ctx.getClassName() + "Service.java",
                   config.javaServiceDir, ctx.getObjectName());

    templateToFile(javaMockServiceTemplate, ctx,
                   ctx.getClassName() + "ServiceMockImpl.java",
                   config.javaServiceDir, ctx.getObjectName(), "impl");


    templateToFile(entityTsTemplate, ctx,
                   ctx.getKebabCaseClassName() + ".ts",
                   config.frontDir,
                   Utils.toTsFileName(ctx.getKebabCaseClassName()),
                   "model");


    templateToFile(moduleTemplate, ctx,
                   ctx.getKebabCaseClassName() + ".module.ts",
                   config.frontDir,
                   Utils.toTsFileName(ctx.getKebabCaseClassName()));

    templateToFile(routingModuleTemplate, ctx,
                   ctx.getKebabCaseClassName() + "-routing.module.ts",
                   config.frontDir,
                   Utils.toTsFileName(ctx.getKebabCaseClassName()));

    templateToFile(resolverTsTemplate, ctx,
                   ctx.getKebabCaseClassName() + ".resolver.ts",
                   config.frontDir,
                   Utils.toTsFileName(ctx.getKebabCaseClassName()),
                   "service");
    templateToFile(tsServiceTemplate, ctx,
                   ctx.getKebabCaseClassName() + ".service.ts",
                   config.frontDir,
                   Utils.toTsFileName(ctx.getKebabCaseClassName()),
                   "service");

    String listComponentName = ctx.getKebabCaseClassName() + "-list";

    templateToFile(listTsTemplate, ctx,
                   listComponentName + ".component.ts",
                   config.frontDir,
                   Utils.toTsFileName(ctx.getKebabCaseClassName()),
                   listComponentName);

    templateToFile(listHtmlTemplate, ctx,
                   listComponentName + ".component.html",
                   config.frontDir,
                   Utils.toTsFileName(ctx.getKebabCaseClassName()),
                   listComponentName);

    templateToFile(listScssTemplate, ctx,
                   listComponentName + ".component.scss",
                   config.frontDir,
                   Utils.toTsFileName(ctx.getKebabCaseClassName()),
                   listComponentName);


    String detailsComponentName = ctx.getKebabCaseClassName() + "-details";

    templateToFile(detailsTsTemplate, ctx,
                   detailsComponentName + ".component.ts",
                   config.frontDir,
                   Utils.toTsFileName(ctx.getKebabCaseClassName()),
                   detailsComponentName);

    templateToFile(detailsHtmlTemplate, ctx,
                   detailsComponentName + ".component.html",
                   config.frontDir,
                   Utils.toTsFileName(ctx.getKebabCaseClassName()),
                   detailsComponentName);

    templateToFile(detailsScssTemplate, ctx,
                   detailsComponentName + ".component.scss",
                   config.frontDir,
                   Utils.toTsFileName(ctx.getKebabCaseClassName()),
                   detailsComponentName);


    String viewComponentName = ctx.getKebabCaseClassName() + "-view";

    templateToFile(viewTsTemplate, ctx,
                   viewComponentName + ".component.ts",
                   config.frontDir,
                   Utils.toTsFileName(ctx.getKebabCaseClassName()),
                   viewComponentName);

    templateToFile(viewHtmlTemplate, ctx,
                   viewComponentName + ".component.html",
                   config.frontDir,
                   Utils.toTsFileName(ctx.getKebabCaseClassName()),
                   viewComponentName);

    templateToFile(viewScssTemplate, ctx,
                   viewComponentName + ".component.scss",
                   config.frontDir,
                   Utils.toTsFileName(ctx.getKebabCaseClassName()),
                   viewComponentName);

    templateToFile(i18nJsonTemplate, ctx.asFr(),
                   ctx.getKebabCaseClassName() + ".fr.json",
                   config.frontDir,
                   Utils.toTsFileName(ctx.getKebabCaseClassName()));

    templateToFile(i18nJsonTemplate, ctx.asDe(),
                   ctx.getKebabCaseClassName() + ".de.json",
                   config.frontDir,
                   Utils.toTsFileName(ctx.getKebabCaseClassName()));

    printReadMe(ctx);
  }

  private void printReadMe(TemplateContext templateContext) throws IOException, TemplateException {
    readmeTemplate.process(templateContext, new OutputStreamWriter(System.out));
  }

  private void templateToFile(Template template, TemplateContext templateContext, String fileMame, String... dirNames)
      throws IOException, TemplateException {

    File dir = FileUtils.getFile(dirNames);
    dir.mkdirs();
    File fileToGenerate = FileUtils.getFile(dir, fileMame);
    if (!config.shouldOverride && fileToGenerate.exists()) {
      System.out.println(fileToGenerate + " already exist : skipping");
      return;
    }


    try (Writer out = new FileWriter(fileToGenerate)) {
      template.process(templateContext, out);
    }
    System.out.println(fileToGenerate.getCanonicalFile() + " has been successfully generated");
  }


  public static class Config {
    private String  frontDir;
    private boolean shouldOverride;
    private String  restControllerDir;
    private String  restControllerPackage;
    private String  javaServicePackage;
    private String  javaServiceDir;

    public Config front(String... frontPath) {
      this.frontDir = Stream.of(frontPath).collect(Collectors.joining(File.separator));
      return this;
    }

    public Config rest(String controllerPackage, String... restDir) {
      String[] packagePath = controllerPackage.split("\\.");
      this.restControllerDir =
          concat(concat(Stream.of(restDir), Stream.of("src", "main", "java")), Stream.of(packagePath))
              .collect(Collectors.joining(File.separator));
      this.restControllerPackage = controllerPackage;
      return this;
    }

    public Config javaService(String javaServicePackage, String... javaServiceDir) {
      String[] packagePath = javaServicePackage.split("\\.");
      this.javaServiceDir =
          concat(concat(Stream.of(javaServiceDir), Stream.of("src", "main", "java")), Stream.of(packagePath))
              .collect(Collectors.joining(File.separator));
      this.javaServicePackage = javaServicePackage;
      return this;
    }


    public Config shouldOverride(boolean shouldOverride) {
      this.shouldOverride = shouldOverride;
      return this;
    }


  }


}
