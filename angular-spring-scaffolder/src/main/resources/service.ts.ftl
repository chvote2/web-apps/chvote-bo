<#-- @formatter:off -->
import "rxjs/add/operator/catch";
import "rxjs/add/operator/toPromise";
import "rxjs/add/operator/map";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import {  ${className} } from '../model/${kebabCaseClassName}';
import { HttpService } from '../../core/service/http/http.service';

@Injectable()
export class ${className}Service {
    private serviceUrl: string;

    constructor(private http: HttpService) {
        this.serviceUrl = this.http.apiBaseURL + '/${restApiBaseName}' ;
    }

    create(${objectName}:${className}): Observable<${className}> {
        return this.http.post(this.serviceUrl, ${objectName}).map(response => response.json());
    }

    update(${objectName}:${className}): Observable<${className}> {
        return this.http.put(this.serviceUrl, ${objectName}).map(response => response.json());
    }

    remove(id:number): Observable<boolean> {
        return this.http.delete(this.serviceUrl + "/" + id).map(response => response.json());
    }

    findAll(): Observable<${className}[]> {
        return this.http.get(this.serviceUrl).map(response => response.json());
    }

    find(id:number): Observable<${className}> {
        return this.http.get(this.serviceUrl + "/" + id).map(response => response.json());
    }
}

<#-- @formatter:on -->
