<#-- @formatter:off -->
package ${servicePackage};

import ${modelFullClass};
import java.util.List;

public interface ${className}Service {

   List<${rawClassName}> findAll();

   ${rawClassName} create(${rawClassName} ${objectName});

   ${rawClassName} edit(${rawClassName}  ${objectName});

   ${rawClassName} findOne(long id);

   boolean delete(long id);
}
<#-- @formatter:on -->