<#-- @formatter:off -->
{
    "dictionary": {
        <#list enums?keys as enum >
        "${enum}":{
                <#list enums[enum] as enumValue >"${enumValue}" : "${i18nPrefix}XXX ${enumValue}" <#if !enumValue?is_last>,</#if></#list>
        }<#if !enum?is_last>,</#if>
        </#list>
    },

    "${objectName}": {
      "toolbarSection": "${i18nPrefix}${humanName}",
      "title": "${i18nPrefix}${humanName}",
      "subtitle": {
        "list": "${i18nPrefix}Liste de XXX ${humanName}",
        "create": "${i18nPrefix}Création de XXX ${humanName}",
        "edit": "${i18nPrefix}Modification de XXX ${humanName}"
      },
      "actions": {
        "create": "${i18nPrefix}Créer un XXX ${humanName}",
        "delete": "${i18nPrefix}Supprimer un XXX ${humanName}"
      },
      "dialogs": {
        "delete": "${i18nPrefix}Voulez-vous vraiment supprimer le XXX ${humanName} ?",
        "save-success" : "${i18nPrefix} ${humanName} a été correctement enregisté",
        "update-success" : "${i18nPrefix} ${humanName} a été correctement mis à jour"
      },
      "grid": {
        "header": {
        <#list fields as field>
          "${field.name}": "${i18nPrefix}${field.humanName}",
        </#list>
          "actions" : "${i18nPrefix}Actions"
        }
      },
      "form": {
        "placeholder": {
        <#list fields as field>
          "${field.name}": "<#if field.required>* </#if>${i18nPrefix}${field.humanName}",
        </#list>
          "actions" : "${i18nPrefix}Actions"
        }
      }
    }
}
<#-- @formatter:on -->