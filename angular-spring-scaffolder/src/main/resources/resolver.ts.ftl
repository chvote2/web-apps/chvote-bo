<#-- @formatter:off -->
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { ${className} } from '../model/${kebabCaseClassName}';
import { ${className}Service } from './${kebabCaseClassName}.service';

@Injectable()
export class ${className}Resolver implements Resolve<${className}> {

  constructor(private service: ${className}Service,
              private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<${className}> {
    let id = +route.paramMap.get('${objectName}Id');
    return this.service.find(id).map(value => {
      if (value) {
        return value;
      } else { // id not found
        this.router.navigate(['/']);
        return null;
      }
    });
  }
}
<#-- @formatter:on -->
