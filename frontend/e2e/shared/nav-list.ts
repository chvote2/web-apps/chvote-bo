/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser, by, ElementFinder, ProtractorBrowser } from "protractor";
import { promise } from 'selenium-webdriver';

/**
 * Object used to manipulate a navigation list.
 */
export class NavList {

  private navList: ElementFinder;

  constructor(id: string, private b: ProtractorBrowser = browser) {
    this.navList = b.element(by.id(id));
  }

  /**
   * @returns true if the navigation list is present, false otherwise
   */
  get present(): promise.Promise<boolean> {
    return this.navList.isPresent();
  }

  /**
   * @returns the number of elements in the navigation list
   */
  get itemCount(): promise.Promise<number> {
    return this.navList.all(by.tagName('mat-list-item')).count();
  }

  /**
   * Return the text of the given navigation element
   *
   * @param index the element's index in the list
   * @returns the text of the given element
   */
  itemText(index: number): promise.Promise<string> {
    return this.navList.all(by.tagName('mat-list-item')).get(index).element(by.className('mat-list-text')).getText();
  }

  /**
   * Return the presence of the error flag on the given navigation element
   *
   * @param index the element's index in the list
   * @returns {promise.Promise<boolean>} the presence of the "in error" badge
   */
  inErrorBadgeDisplayed(index: number): promise.Promise<boolean> {
    return this.navList.all(by.tagName('mat-list-item')).get(index).element(by.className('task-error')).isPresent();
  }
}
