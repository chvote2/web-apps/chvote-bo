/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser, by, ElementFinder, ProtractorBrowser } from "protractor";
import { promise } from 'selenium-webdriver';

/**
 * Object used to manipulate an angular table.
 */
export class MatTable {

  table: ElementFinder;

  /**
   * Create a table reference based on its unique ID or the element directly.
   *
   * @param idOrElement the button's ID or the button element directly
   * @param b the browser on which this button appears (main browser by default)
   */
  constructor(idOrElement: string | ElementFinder,
              private b: ProtractorBrowser = browser) {
    this.table = (typeof idOrElement === "string") ? b.element(by.id(idOrElement)) : idOrElement;
  }

  /**
   * @returns {promise.Promise<boolean>} whether the table is displayed or not
   */
  get displayed(): promise.Promise<boolean> {
    return this.table.isDisplayed();
  }

  /**
   * @returns {promise.Promise<number>} the number of row in the table
   */
  get rowCount(): promise.Promise<number> {
    return this.table.all(by.xpath('./mat-row')).count();
  };

  /**
   * Get header element of the given column.
   *
   * @param columnId ID of the column
   * @returns {ElementFinder} the corresponding header element
   */
  header(columnId: string): ElementFinder {
    return this.table.element(by.css(`.mat-header-cell.mat-column-${columnId}`));
  };

  /**
   * Get header text of the given column.
   *
   * @param columnId ID of the column
   * @returns {promise.Promise<string>} the corresponding header's text
   */
  headerValue(columnId: string): promise.Promise<string> {
    return this.header(columnId).getText();
  };

  /**
   * Get the cell at the given position.
   *
   * @param rowIndex index of the row
   * @param columnId ID of the column
   * @returns {ElementFinder} the corresponding cell element
   */
  cell(rowIndex: number, columnId: string): ElementFinder {
    return this.table.all(by.xpath('./mat-row')).get(rowIndex)
      .element(by.className(`mat-column-${columnId}`));
  };

  /**
   * Get the text of the given cell.
   *
   * @param rowIndex index of the row
   * @param columnId ID of the column
   * @returns {promise.Promise<string>} the corresponding cell's text
   */
  cellValue(rowIndex: number, columnId: string): promise.Promise<string> {
    return this.cell(rowIndex, columnId).getText();
  };
}
