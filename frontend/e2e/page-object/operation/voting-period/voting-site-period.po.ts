/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Button } from '../../../shared/button';
import { Card } from '../../../shared/card';
import { Cache } from '../../cache.po';

/**
 * Page object for the voting site period edit section.
 */
export class VotingSitePeriodPage {

  static get root(): Card {
    return Cache.card('votingSitePeriodEdit');
  }

  static get dateOpenField() {
    return Cache.datePickerField(this.root.content, 'votingSitePeriodEdit', 'dateOpen');
  };

  static get dateCloseField() {
    return Cache.datePickerField(this.root.content, 'votingSitePeriodEdit', 'dateClose');
  };

  static get gracePeriodField() {
    return Cache.inputField(this.root.content, 'votingSitePeriodEdit', 'gracePeriod');
  };

  static get saveButton(): Button {
    return Cache.button('saveButton');
  }

  static get closeVotingPeriodButton(): Button {
    return Cache.button('closeVotingPeriod');
  }

}
