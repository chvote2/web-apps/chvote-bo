/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Card } from '../../../shared/card';
import { Cache } from '../../cache.po';
import { by } from 'protractor';

/**
 * Page object for the tally.
 */
export class TallyPage {

  static get root(): Card {
    return Cache.card('tally');
  }

  static get tallyArchiveInProgressElement() {
    return this.root.content.element(by.id("tally-archive-in-progress"));
  };

  static get tallyArchiveFailedElement() {
    return this.root.content.element(by.id("tally-archive-failed"));
  };

  static get tallyArchiveCreatedElement() {
    return this.root.content.element(by.id("tally-archive-created"));
  };

  static get tallyArchiveNotEnoughtVotes() {
    return this.root.content.element(by.id("tally-not-enough-votes"));
  }

  static get isDownloadArchiveDenied() {
    return this.root.content.element(by.id("tally-archive-access-denied")).isPresent();
  }

  static get tallyArchiveDownloadButton() {
    return Cache.button("tally-archive-download-button");
  }

  static get tallyArchiveRetryButton() {
    return Cache.button("tally-archive-retry-button");
  }

}
