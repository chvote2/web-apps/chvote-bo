/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { by, ElementFinder } from 'protractor';
import { Button } from '../../../shared/button';
import { InputField } from '../../../shared/input-field';
import { SelectField } from '../../../shared/select-field';
import { UploadInput } from '../../../shared/upload-input';
import { MatTable } from '../../../shared/table';
import { promise } from 'selenium-webdriver';
import { Cache } from '../../cache.po';

/**
 * Page object for the highlighted question edit section.
 */
export class HighlightedQuestionEdit {

  /**
   * @returns {ElementFinder} the element displaying the list of highlighted questions
   */
  static get list(): ElementFinder {
    return Cache.element('highlightedQuestionsList', by.tagName('highlighted-question-list'));
  }

  /**
   * @returns {ElementFinder} the element used for highlighted question edit
   */
  static get edit(): ElementFinder {
    return Cache.element('highlightedQuestionsEdit', by.tagName('highlighted-question-edit'));
  }

  /**
   * @returns {ElementFinder} the element containing the form to add a new question
   */
  static get localizedQuestionInputBox(): ElementFinder {
    return this.edit.element(by.id('localizedQuestionInputBox'));
  }

  /**
   * @returns {promise.Promise<number>} the number of displayed questions
   */
  static get questionCount(): promise.Promise<number> {
    return this.list.all(by.className('highlighted-question')).count();
  }

  /**
   * @returns {Button} the [ADD QUESTION] button
   */
  static get addQuestionButton(): Button {
    return Cache.button('addAQuestion');
  }

  /**
   * Get the [EDIT] button for the given question
   *
   * @param {number} number the question's number
   * @returns {Button}  the corresponding [EDIT] button
   */
  static editQuestionButton(number: number): Button {
    return Cache.button(`edit-question-${number}-button`);
  }

  /**
   * Get the [DELETE] button for the given question
   *
   * @param {number} number the question's number
   * @returns {Button}  the corresponding [DELETE] button
   */
  static deleteQuestionButton(number: number): Button {
    return Cache.button(`delete-question-${number}-button`);
  }

  /**
   * @returns {promise.Promise<boolean>} whether the section is in edit mode or not
   */
  static get editMode(): promise.Promise<boolean> {
    return this.edit.isPresent();
  }

  /**
   * @returns {Button} the [BACK] button
   */
  static get backButton(): Button {
    return Cache.button('closeEdition');
  }

  /**
   * @returns {InputField} the question's label input field
   */
  static get questionLabelInput(): InputField {
    return Cache.inputField(this.localizedQuestionInputBox, 'localizedQuestionInputBox', 'localizedQuestion');
  }

  /**
   * @returns {SelectField} the language select field
   */
  static get languageSelect(): SelectField {
    return Cache.selectField(this.localizedQuestionInputBox, 'localizedQuestionInputBox', 'language');
  }

  /**
   * @returns {UploadInput} the question file input field
   */
  static get uploadInput(): UploadInput {
    return Cache.uploadInputField('highlightedQuestionsFileUploadField', this.localizedQuestionInputBox);
  }

  /**
   * @returns {MatTable} the grid displaying the localized questions
   */
  static get localizedQuestionTable(): MatTable {
    return Cache.table('localizedQuestionTable');
  }

  /**
   * Get the [DELETE] button for the given localized question
   *
   * @param {number} index the question's index
   * @returns {Button}  the corresponding [DELETE] button
   */
  static deleteLocalizedQuestionButton(index: number): Button {
    return Cache.button(`localizedQuestionTable-deleteButton-${index}`, this.localizedQuestionTable.cell(index, 'actions').element(by.className('btn-delete')));
  }

}
