/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Card } from '../../../shared/card';
import { Button } from '../../../shared/button';
import { Cache } from '../../cache.po';
import { SelectField } from '../../../shared/select-field';
import { Checkbox } from '../../../shared/checkbox';

/**
 * Page object for the voting site configuration edit section.
 */
export class VotingSiteConfigurationEdit {

  static get root(): Card {
    return Cache.card('votingSiteConfigurationEdit');
  }

  static get defaultLanguage(): SelectField {
    return Cache.selectField(this.root.content, 'votingSiteConfigurationEdit', 'defaultLanguage');
  }

  static get saveButton(): Button {
    return Cache.button('saveButton');
  }

  static get frCheckbox(): Checkbox {
    return new Checkbox(VotingSiteConfigurationEdit.root.element, "FR");
  }

  static get deCheckbox(): Checkbox {
    return new Checkbox(VotingSiteConfigurationEdit.root.element, "DE");
  }

  static get itCheckbox(): Checkbox {
    return new Checkbox(VotingSiteConfigurationEdit.root.element, "IT");
  }

  static get rmCheckbox(): Checkbox {
    return new Checkbox(VotingSiteConfigurationEdit.root.element, "RM");
  }

}

