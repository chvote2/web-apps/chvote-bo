/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser, by, element, ElementFinder, Locator } from 'protractor';
import { InputField } from '../shared/input-field';
import { Card } from '../shared/card';
import { Button } from '../shared/button';
import { DatePicker } from '../shared/date-picker';
import { UploadInput } from '../shared/upload-input';
import { MatTable } from '../shared/table';
import { ExpansionPanel } from '../shared/expansion-panel';
import { SlideDetails } from '../shared/slide-details';
import { GroupedGrid } from '../shared/grouped-grid';
import { SelectField } from '../shared/select-field';
import { SummaryCard } from '../shared/completable-card';
import { SlideToggle } from '../shared/slide-toggle';

export class Cache {

  /**
   * Create an element reference or return the previously created one
   *
   * @param {string} id the element's unique identifier
   * @param {Locator} locator (optional) the element's locator
   * @returns {ElementFinder} the corresponding Card object
   */
  static element(id: string, locator?: Locator): ElementFinder {
    return browser.element(locator ? locator : by.id(id));
  }

  /**
   * Create an input field or return the previously created one
   *
   * @param {ElementFinder} container the element containing the input field
   * @param {string} containerId unique ID of the container
   * @param {string} identifier the identifier
   * @param {boolean} byFormControlName whether the field is identified by its from control name or directly its ID
   * @param {boolean} byName whether the input field is identified by its name attribute or directly its ID
   * @returns {InputField} the corresponding InputField object
   */
  static inputField(container: ElementFinder,
                    containerId: string,
                    identifier: string,
                    byFormControlName: boolean = true,
                    byName: boolean = false): InputField {
    return new InputField(container, identifier, byFormControlName, byName);
  }

  /**
   * Create a select field or return the previously created one
   *
   * @param {ElementFinder} container the element containing the select field
   * @param {string} containerId unique ID of the container
   * @param {string} formControlName the field's form control name
   * @returns {SelectField} the corresponding SelectField object
   */
  static selectField(container: ElementFinder, containerId: string, formControlName?: string): SelectField {
    return new SelectField(container, formControlName);
  }

  /**
   * Create a date picker field or return the previously created one
   *
   * @param {ElementFinder} container the element containing the date picker field
   * @param {string} containerId unique ID of the container
   * @param {string} id the field's unique id
   * @param {boolean} byFormControlName whether the field is identified by its from control name or directly its ID
   * @returns {DatePicker} the corresponding DatePicker object
   */
  static datePickerField(container: ElementFinder,
                         containerId: string,
                         id: string,
                         byFormControlName: boolean = true): DatePicker {

    return new DatePicker(container, id, byFormControlName);
  }

  /**
   * Create an upload input field or return the previously created one
   *
   * @param {string} id the field's unique id
   * @param {ElementFinder} container the element containing the upload input field
   * @returns {UploadInput} the corresponding UploadInput object
   */
  static uploadInputField(id: string, container: ElementFinder): UploadInput {
    return new UploadInput(container);
  }

  /**
   * Create a card or return the previously created one
   *
   * @param {string} id the card's unique identifier
   * @returns {Card} the corresponding Card object
   */
  static card(id: string): Card {
    return new Card(id);
  }

  /**
   * Create a table or return the previously created one
   *
   * @param {string} id the table's unique identifier
   * @param {ElementFinder} element (optional) the table's direct element
   * @returns {MatTable} the corresponding MatTable object
   */
  static table(id: string, element?: ElementFinder): MatTable {
    return new MatTable(element ? element : id);
  }

  /**
   * Create a slide toggle or return the previously created one
   *
   * @param {ElementFinder} container the element containing the slide toggle
   * @param {string} containerId unique ID of the container
   * @param {string} formControlName the field's form control name
   * @returns {SlideToggle} the corresponding SlideToggle object
   */
  static slideToggle(container: ElementFinder, containerId: string, formControlName: string): SlideToggle {
    return new SlideToggle(container, formControlName);
  }

  /**
   * Create an expansion panel or return the previously created one
   *
   * @param {string} id the panel's unique id
   * @param {ElementFinder} container the element containing the expansion panel
   * @returns {ExpansionPanel} the corresponding ExpansionPanel object
   */
  static expansionPanel(id: string, container: ElementFinder): ExpansionPanel {
    return new ExpansionPanel(container);
  }

  /**
   * Create a slide detail or return the previously created one
   *
   * @param {string} id the slide detail's unique id
   * @param {ElementFinder} container the element containing the slide detail
   * @returns {SlideDetails} the corresponding SlideDetails object
   */
  static slideDetail(id: string, container: ElementFinder): SlideDetails {
    return new SlideDetails(container);
  }

  /**
   * Create a grouped grid or return the previously created one
   *
   * @param {string} id the grouped grid's unique id
   * @param {ElementFinder} container the element containing the grouped grid
   * @returns {GroupedGrid} the corresponding GroupedGrid object
   */
  static groupedGrid(id: string, container: ElementFinder): GroupedGrid {
    return new GroupedGrid(container);
  }

  /**
   * Create a button or return the previously created one
   *
   * @param {string} id the button's unique identifier
   * @param {ElementFinder} element (optional) the button's direct element
   * @returns {Button} the corresponding Button object
   */
  static button(id: string, element?: ElementFinder): Button {
    return new Button(element ? element : id);
  }

  /**
   * Create a navigation tab or return the previously created one
   *
   * @param {number} index the tab's index
   * @returns {ElementFinder} the corresponding ElementFinder object referencing the tab
   */
  static navigationTab(index: number): ElementFinder {
    return element(by.css(`*[id="operationManagementNavBar"] .mat-tab-link:nth-child(${index + 1})`));
  }

  /**
   * Create a summary or return the previously created one
   *
   * @param {string} cardName the summary card's unique name
   * @returns {SummaryCard} the corresponding SummaryCard object
   */
  static summaryCard(cardName: string): SummaryCard {
    return new SummaryCard(cardName);
  }
}
