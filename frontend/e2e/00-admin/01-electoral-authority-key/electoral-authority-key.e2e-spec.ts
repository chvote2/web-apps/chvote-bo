/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { AdminPage } from '../../page-object/admin/admin.po';
import { ElectoralAuthorityKeyEdit, UpdateNamePopup } from '../../page-object/admin/electoral-authority-key.po';
import { browser, by } from 'protractor';
import { Main } from '../../page-object/main.po';
import { resetDB } from '../../shared/mock-server';

const FLOW = browser.controlFlow();

describe('Admin/Electoral authority key', () => {

  beforeAll(() => {
    FLOW.execute(() => resetDB());
    Main.login();
    Main.navigateTo('admin');
  });

  describe('create for one management entity', () => {

    describe('summary card before edit', () => {
      it('should display card for electoral authority key on the admin dashboard', () => {
        expect(AdminPage.electoralAuthorityKeyCard.displayed).toBeTruthy();
      });

      it('title should be "Clés de l\'autorité électorale *"', () => {
        expect(AdminPage.electoralAuthorityKeyCard.title).toBe('Clés de l\'autorité électorale *');
      });

      it('subtitle should be "Clés publiques utilisées pour le chiffrement de l\'urne électronique"', () => {
        expect(AdminPage.electoralAuthorityKeyCard.subtitle)
          .toBe('Clés publiques utilisées pour le chiffrement de l\'urne électronique');
      });

      it('content should not be displayed', () => {
        expect(AdminPage.electoralAuthorityKeyCard.noConfigDisplayed).toBeTruthy();
      });

      it('[CONFIGURE] button should be displayed', () => {
        expect(AdminPage.electoralAuthorityKeyCard.buttonLabel).toBe('CONFIGURER');
      });

      it('completed badge should not be present', () => {
        expect(AdminPage.electoralAuthorityKeyCard.completeBadgeDisplayed).toBeFalsy();
      });
    });

    describe('edit section', () => {

      it('clicking on [CONFIGURE] button should open the edition page', () => {
        AdminPage.electoralAuthorityKeyCard.button.click();
        expect(ElectoralAuthorityKeyEdit.root.displayed).toBeTruthy();
      });

      it('card title should be "Clés de l\'autorité électorale"', () => {
        expect(ElectoralAuthorityKeyEdit.root.title).toBe('Clés de l\'autorité électorale');
      });

      it('card subtitle should be "Définir les clés publiques utilisées pour le chiffrement de l\'urne électronique"',
        () => {
          expect(ElectoralAuthorityKeyEdit.root.subtitle)
            .toContain('Définir les clés publiques utilisées pour le chiffrement de l\'urne électronique');
        });

      it('should not allow user to add a file without specifying a label for the key', () => {
        ElectoralAuthorityKeyEdit.uploadInput.file = '../resources/electoral-authority.pub';
        ElectoralAuthorityKeyEdit.uploadInput.uploadButton.click();
        expect(ElectoralAuthorityKeyEdit.label.error).toBe('Champ obligatoire');
      });

      it('should allow user to add a file after specifying a label for the key', () => {
        ElectoralAuthorityKeyEdit.label.value = 'test';
        ElectoralAuthorityKeyEdit.uploadInput.uploadButton.click();
        expect(ElectoralAuthorityKeyEdit.grid.rowCount).toBe(1);
      });

      it('should allow user to update label', () => {
        ElectoralAuthorityKeyEdit.grid.cell(0, 'label').element(by.tagName('text-with-edit')).click();
        UpdateNamePopup.label.value = "Updated label";
        UpdateNamePopup.acceptButton.click();
        UpdateNamePopup.waitForInvisibility();
        expect((ElectoralAuthorityKeyEdit.grid.cell(0, 'label').element(by.className('text')).getText()))
          .toBe('Updated label');
      });

      it('should prevent user to add a key with the same label', () => {
        ElectoralAuthorityKeyEdit.label.value = 'Updated label';
        ElectoralAuthorityKeyEdit.uploadInput.file = '../resources/electoral-authority.pub';
        ElectoralAuthorityKeyEdit.uploadInput.uploadButton.click();
        expect(ElectoralAuthorityKeyEdit.label.error).toBe('Ce libellé est déjà utilisé');
        expect(ElectoralAuthorityKeyEdit.grid.rowCount).toBe(1);
      });

      it('should allow user to add a key with a differnt label', () => {
        ElectoralAuthorityKeyEdit.label.value = 'test2';
        ElectoralAuthorityKeyEdit.uploadInput.file = '../resources/electoral-authority.pub';
        ElectoralAuthorityKeyEdit.uploadInput.uploadButton.click();
        expect(ElectoralAuthorityKeyEdit.grid.rowCount).toBe(2);
      });

      it('should prevent user to modify a label to an existing one', () => {
        ElectoralAuthorityKeyEdit.grid.cell(0, 'label').element(by.tagName('text-with-edit')).click();
        UpdateNamePopup.label.value = 'test2';
        UpdateNamePopup.acceptButton.click();
        expect(UpdateNamePopup.label.error).toBe('Ce libellé est déjà utilisé');
      });

      it('should allow user to modify a label to a different one', () => {
        UpdateNamePopup.label.value = 'test3';
        UpdateNamePopup.acceptButton.click();
        UpdateNamePopup.waitForInvisibility();
        expect((ElectoralAuthorityKeyEdit.grid.cell(0, 'label').element(by.className('text')).getText())).toBe('test3');
      });

      it('should update card accordingly', () => {
        AdminPage.returnFromEdit();
        expect(AdminPage.electoralAuthorityKeyCard.completeBadgeDisplayed).toBeTruthy();
        expect(AdminPage.electoralAuthorityKeyCard.getContentLabel(0)).toBe('Nombre de clés');
        expect(AdminPage.electoralAuthorityKeyCard.getContentValue(0)).toBe('2');
      });
    });
  });

  describe('other management entity', () => {
    it('should not display key for other management entity', () => {
      Main.login('ge3');
      Main.navigateTo('admin');
      expect(AdminPage.electoralAuthorityKeyCard.completeBadgeDisplayed).toBeFalsy();
      AdminPage.electoralAuthorityKeyCard.button.click();
      expect(ElectoralAuthorityKeyEdit.grid.rowCount).toBe(0);
    });
  });

  describe('user with limited right', () => {
    it('should display card in read-only mode', () => {
      Main.login('ge4');
      Main.navigateTo('admin');
      expect(AdminPage.electoralAuthorityKeyCard.readOnly).toBeTruthy();
    });

    it('should allow user to see keys imported on the list', () => {
      AdminPage.electoralAuthorityKeyCard.button.click();
      expect(ElectoralAuthorityKeyEdit.grid.rowCount).toBe(2);
    });

    it('should disable import of a new key', () => {
      expect(ElectoralAuthorityKeyEdit.label.present).toBeFalsy();
    });

    it('should disable change of a label', () => {
      expect((ElectoralAuthorityKeyEdit.grid.cell(0, 'label').element(by.tagName('mat-icon')).isPresent()))
        .toBeTruthy();
    });

  });

});
