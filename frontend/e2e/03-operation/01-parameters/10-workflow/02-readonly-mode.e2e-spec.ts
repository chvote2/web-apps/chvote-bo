/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { OperationManagementPage } from '../../../page-object/operation/operation-management.po';
import { BaseEdit } from '../../../page-object/operation/parameters/base-edit.po';
import { Main } from '../../../page-object/main.po';
import { ParameterEditPage } from '../../../page-object/operation/parameters/parameter-edit.po';
import { MilestoneEdit } from '../../../page-object/operation/parameters/milestone-edit.po';
import { DomainInfluenceEdit } from '../../../page-object/operation/parameters/domain-influence-edit.po';
import { RepositoryEdit } from '../../../page-object/operation/parameters/repository-edit.po';
import { DocumentationEdit } from '../../../page-object/operation/parameters/documentation-edit.po';
import { TestingCardsList } from '../../../page-object/operation/testing-cards/testing-cards-list.po';

describe('Operation/Parameters/Workflow/Read only mode', () => {

  it('should have base parameter inputs and buttons disabled or absent', () => {
    OperationManagementPage.baseCard.button.click();
    Main.screenshot('inside edit component');
    expect(BaseEdit.dateInput.enabled).toBeFalsy();
    expect(BaseEdit.longLabelInput.enabled).toBeFalsy();
    expect(BaseEdit.shortLabelInput.enabled).toBeFalsy();
    expect(BaseEdit.saveButton.present).toBeFalsy();
    ParameterEditPage.clickBackLink();
  });

  it('should have milestone inputs and buttons disabled or absent', () => {
    OperationManagementPage.milestoneCard.button.click();
    Main.screenshot('inside edit component');
    expect(MilestoneEdit.milestoneInput('SITE_VALIDATION').enabled).toBeFalsy();
    expect(MilestoneEdit.milestoneInput('CERTIFICATION').enabled).toBeFalsy();
    expect(MilestoneEdit.milestoneInput('PRINTER_FILES').enabled).toBeFalsy();
    expect(MilestoneEdit.milestoneInput('BALLOT_BOX_INIT').enabled).toBeFalsy();
    expect(MilestoneEdit.milestoneInput('SITE_OPEN').enabled).toBeFalsy();
    expect(MilestoneEdit.milestoneInput('SITE_CLOSE').enabled).toBeFalsy();
    expect(MilestoneEdit.milestoneInput('BALLOT_BOX_DECRYPT').enabled).toBeFalsy();
    expect(MilestoneEdit.milestoneInput('RESULT_VALIDATION').enabled).toBeFalsy();
    expect(MilestoneEdit.milestoneInput('DATA_DESTRUCTION').enabled).toBeFalsy();
    expect(MilestoneEdit.gracePeriodInput.enabled).toBeFalsy();
    ParameterEditPage.clickBackLink()
  });

  it('should have no domain of influence button', () => {
    OperationManagementPage.domainInfluenceCard.button.click();
    Main.screenshot('inside edit component');
    expect(DomainInfluenceEdit.deleteButton.present).toBeFalsy();
    expect(DomainInfluenceEdit.uploadInput.uploadButton.present).toBeFalsy();
    ParameterEditPage.clickBackLink()
  });

  it('should have no repository button', () => {
    OperationManagementPage.repositoryCard.button.click();
    Main.screenshot('inside edit component');
    expect(RepositoryEdit.getDeleteButton(0).present).toBeFalsy();
    expect(RepositoryEdit.uploadInput.uploadButton.present).toBeFalsy();
    ParameterEditPage.clickBackLink()
  });

  it('should have no documentation buttons', () => {
    OperationManagementPage.documentationCard.button.click();
    Main.screenshot('inside edit component');
    expect(DocumentationEdit.getDeleteButton(0).present).toBeFalsy();
    expect(DocumentationEdit.uploadInput.uploadButton.present).toBeFalsy();
    ParameterEditPage.clickBackLink()
  });

  it('should have no testing card lot buttons', () => {
    OperationManagementPage.testingCardsLotCard.button.click();
    Main.screenshot('inside edit component');
    expect(TestingCardsList.getDeleteButton(0).present).toBeFalsy();
    expect(TestingCardsList.createButton.present).toBeFalsy();
    ParameterEditPage.clickBackLink()
  });

});
