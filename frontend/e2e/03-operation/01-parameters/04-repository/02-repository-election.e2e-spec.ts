/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { ParameterEditPage } from "../../../page-object/operation/parameters/parameter-edit.po";
import { OperationOption } from "../../../shared/mock-server";
import { OperationManagementPage } from '../../../page-object/operation/operation-management.po';
import { ElectionDetails, RepositoryEdit } from '../../../page-object/operation/parameters/repository-edit.po';
import { Main } from '../../../page-object/main.po';
let since = require('jasmine2-custom-message');
import * as moment from "moment";

describe('Operation/Parameters/Repository/Election', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      'operations/[OPERATION_ID]/parameters/edit/repository',
      'test_repository_elec',
      'TEST_e2e_repository_election',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION
      ]
    );
  });

  describe('import a repository file', () => {
    it("should display the result grid if the file is valid", () => {
      // given
      RepositoryEdit.uploadInput.file = '../resources/parameters/valid-repository-election.xml';
      // when
      RepositoryEdit.uploadInput.uploadButton.click();
      // then
      expect(RepositoryEdit.table.displayed).toBeTruthy();
    });

    it('should display 1 row', () => {
      expect(RepositoryEdit.table.cellValue(1, 'file')).toContain('ELECTIONS CANTONALES - 201804EC - 12.06.2057');
      expect(RepositoryEdit.table.cellValue(1, 'type')).toBe('Election');
      expect(RepositoryEdit.table.cellValue(1, 'author')).toBe('ge1');
      expect(RepositoryEdit.table.cellValue(1, 'importedDate')).toBe(moment().format('D.MM.Y'));
    });

    it('should show details when clicking on details button', () => {
      // when
      RepositoryEdit.getDetailButton(1).click();
      // then
      RepositoryEdit.slideDetails.waitForDetailsDisplayed();
    });

    it('should display a grid for proportional ballots', () => {
      expect(ElectionDetails.proportionalGrid.displayed).toBeTruthy();
      expect(ElectionDetails.proportionalGrid.rowCount).toBe(1);
      expect(ElectionDetails.proportionalGrid.cellValue(0, 'ballot')).toBe('Grand Conseil 2018');
      expect(ElectionDetails.proportionalGrid.cellValue(0, 'mandate')).toBe('Député-e');
      expect(ElectionDetails.proportionalGrid.cellValue(0, 'numberOfMandates')).toBe('100');
      expect(ElectionDetails.proportionalGrid.cellValue(0, 'listCount')).toBe('3');
      expect(ElectionDetails.proportionalGrid.cellValue(0, 'candidatureCount')).toBe('9');
      expect(ElectionDetails.proportionalGrid.cell(0, 'candidateCount').isPresent()).toBeFalsy();
    });

    it('should show the electoral rolls when clicking on the roll\'s count', () => {
      ElectionDetails.proportionalGrid.cell(0, "listCount").click();
      ElectionDetails.waitForElectoralRollsDisplayed();

    });

    it('should display 3 rows', () => {
      expect(ElectionDetails.electoralRolls.rowCount).toBe(3);

      expect(ElectionDetails.electoralRolls.cellValue(0, 'indentureNumber')).toBe('1');
      expect(ElectionDetails.electoralRolls.cellValue(0, 'name')).toBe('12345678901234567890');
      expect(ElectionDetails.electoralRolls.cellValue(0, 'inAnUnion')).toBe('Oui');
      expect(ElectionDetails.electoralRolls.cellValue(0, 'candidateCount')).toBe('4');

      expect(ElectionDetails.electoralRolls.cellValue(1, 'indentureNumber')).toBe('2');
      expect(ElectionDetails.electoralRolls.cellValue(1, 'name')).toBe('VERTS');
      expect(ElectionDetails.electoralRolls.cellValue(1, 'inAnUnion')).toBe('Oui');
      expect(ElectionDetails.electoralRolls.cellValue(1, 'candidateCount')).toBe('2');

      expect(ElectionDetails.electoralRolls.cellValue(2, 'indentureNumber')).toBe('3');
      expect(ElectionDetails.electoralRolls.cellValue(2, 'name')).toBe('SOCIALISTES');
      expect(ElectionDetails.electoralRolls.cellValue(2, 'inAnUnion')).toBe('Non');
      expect(ElectionDetails.electoralRolls.cellValue(2, 'candidateCount')).toBe('3');

      ElectionDetails.closeElectoralRolls();
    });

    it('should display a grid for majority ballots with lists election', () => {
      expect(ElectionDetails.majorityWithListGrid.displayed).toBeTruthy();
      expect(ElectionDetails.majorityWithListGrid.rowCount).toBe(1);
      expect(ElectionDetails.majorityWithListGrid.cellValue(0, 'ballot')).toBe('Conseil d\'Etat 2018');
      expect(ElectionDetails.majorityWithListGrid.cellValue(0, 'mandate')).toBe('Conseiller-ère d\'Etat');
      expect(ElectionDetails.majorityWithListGrid.cellValue(0, 'numberOfMandates')).toBe('7');
      expect(ElectionDetails.majorityWithListGrid.cellValue(0, 'listCount')).toBe('3');
      expect(ElectionDetails.majorityWithListGrid.cellValue(0, 'candidatureCount')).toBe('7');
      expect(ElectionDetails.majorityWithListGrid.cell(0, 'candidateCount').isPresent()).toBeFalsy();
    });

    it('should show the electoral rolls when clicking on the roll\'s count', () => {
      ElectionDetails.majorityWithListGrid.cell(0, 'listCount').click();
      ElectionDetails.waitForElectoralRollsDisplayed();
    });

    it('should have 3 rows', () => {
      expect(ElectionDetails.electoralRolls.rowCount).toBe(3);
      ElectionDetails.closeElectoralRolls();
    });

    it('should display a grid for majority ballots without electorl roll', () => {
      expect(ElectionDetails.majorityGrid.displayed).toBeTruthy();
      expect(ElectionDetails.majorityGrid.rowCount).toBe(1);
      expect(ElectionDetails.majorityGrid.cellValue(0, 'ballot')).toBe('Test majoritaire sans liste 2018');
      expect(ElectionDetails.majorityGrid.cellValue(0, 'mandate')).toBe('Développeur-euse');
      expect(ElectionDetails.majorityGrid.cellValue(0, 'numberOfMandates')).toBe('1');
      expect(ElectionDetails.majorityGrid.cellValue(0, 'candidateCount')).toBe('2');
      expect(ElectionDetails.majorityGrid.cell(0, 'listCount').isPresent()).toBeFalsy();
      expect(ElectionDetails.majorityGrid.cell(0, 'candidatureCount').isPresent()).toBeFalsy();
    });
  });

  describe('repository summary card', () => {
    it('content should display the number of repositories by type', () => {
      ParameterEditPage.clickBackLink();

      since('first type label should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.repositoryCard.getContentLabel(0)).toBe('Votation');
      since('first type value should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.repositoryCard.getContentValue(0)).toBe('1 référentiel');
      since('second type label should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.repositoryCard.getContentLabel(1)).toBe('Élection');
      since('second type value should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.repositoryCard.getContentValue(1)).toBe('1 référentiel');
    });
  });
});
