/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { OperationOption } from "../../../shared/mock-server";
import { HighlightedQuestionEdit } from '../../../page-object/operation/parameters/highlighted-question-edit.po';
import { ParameterEditPage } from '../../../page-object/operation/parameters/parameter-edit.po';
import { OperationManagementPage } from '../../../page-object/operation/operation-management.po';
import { Main } from '../../../page-object/main.po';
import { browser, protractor } from 'protractor';
import { waitForVisibility, waitForInvisibility } from '../../../shared/e2e.utils';

let since = require('jasmine2-custom-message');

describe('Operation/Parameters/Documentation/Highlighted Questions', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      'operations/[OPERATION_ID]/parameters/edit/document',
      'test_highlighted_question',
      'TEST_e2e_highlighted_question',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION,
        OperationOption.ADD_REPOSITORY_ELECTION
      ]
    );
  });

  describe('question creation', () => {

    it('should initially display no question', () => {
      expect(HighlightedQuestionEdit.questionCount).toBe(0);
    });

    it('should be able to create a new question', () => {
      HighlightedQuestionEdit.addQuestionButton.click();
      expect(HighlightedQuestionEdit.editMode).toBeTruthy();
    });

    it('should not create a question if we don\'t add any localized version', () => {
      HighlightedQuestionEdit.backButton.click();
      expect(HighlightedQuestionEdit.editMode).toBeFalsy();
      expect(HighlightedQuestionEdit.questionCount).toEqual(0);
    });

    it('should be able to create a new question with a localized version', () => {
      HighlightedQuestionEdit.addQuestionButton.click();
      HighlightedQuestionEdit.questionLabelInput.value = 'Question';
      HighlightedQuestionEdit.uploadInput.file = '../resources/parameters/test-file-1.pdf';
      HighlightedQuestionEdit.languageSelect.select('Français');
      HighlightedQuestionEdit.uploadInput.uploadButton.click();
      waitForVisibility(HighlightedQuestionEdit.localizedQuestionTable.table);
      since('localized question count should be #{expected} (was #{actual})').expect(HighlightedQuestionEdit.localizedQuestionTable.rowCount).toEqual(1);

      HighlightedQuestionEdit.backButton.click();
      since('question count should be #{expected} (was #{actual})').expect(HighlightedQuestionEdit.questionCount).toEqual(1);
    });

    it('should close localized question edit view when no more language is left', () => {
      HighlightedQuestionEdit.editQuestionButton(1).click();
      HighlightedQuestionEdit.questionLabelInput.value = 'Question';
      HighlightedQuestionEdit.uploadInput.file = '../resources/parameters/test-file-1.pdf';
      HighlightedQuestionEdit.languageSelect.select('Allemand');
      HighlightedQuestionEdit.uploadInput.uploadButton.click();
      since('first add: localized question count should be #{expected} (was #{actual})').expect(HighlightedQuestionEdit.localizedQuestionTable.rowCount).toEqual(2);

      HighlightedQuestionEdit.questionLabelInput.value = 'Question';
      HighlightedQuestionEdit.uploadInput.file = '../resources/parameters/test-file-1.pdf';
      HighlightedQuestionEdit.languageSelect.select('Italien');
      HighlightedQuestionEdit.uploadInput.uploadButton.click();
      since('second add: localized question count should be #{expected} (was #{actual})').expect(HighlightedQuestionEdit.localizedQuestionTable.rowCount).toEqual(3);

      HighlightedQuestionEdit.questionLabelInput.value = 'Question';
      HighlightedQuestionEdit.uploadInput.file = '../resources/parameters/test-file-1.pdf';
      HighlightedQuestionEdit.languageSelect.select('Romanche');
      HighlightedQuestionEdit.uploadInput.uploadButton.click();
      since('localized question edit should be closed').expect(HighlightedQuestionEdit.editMode).toBeFalsy();
    });

    it('should display questions count in the card', () => {
      ParameterEditPage.clickBackLink();
      expect(OperationManagementPage.documentationCard.getContentValue(1)).toBe('1 question');
    });

    it('should not be able to add localized version when all have been set', () => {
      OperationManagementPage.documentationCard.button.click();
      HighlightedQuestionEdit.editQuestionButton(1).click();
      since('edit view should be displayed').expect(HighlightedQuestionEdit.editMode).toBeTruthy();
      since('localized question input box should be hidden').expect(HighlightedQuestionEdit.localizedQuestionInputBox.isPresent()).toBeFalsy()
    });
  });

  describe('question deletion', () => {

    it('should be able to delete localized version', () => {
      HighlightedQuestionEdit.deleteLocalizedQuestionButton(0).click();
      expect(Main.confirmDialog.content.getText()).toEqual('Voulez-vous vraiment supprimer la traduction en Français ?');

      Main.confirmDialog.accept();
      since('localized question input box should be displayed').expect(HighlightedQuestionEdit.localizedQuestionInputBox.isPresent()).toBeTruthy();
      since('localized question count should be #{expected} (was #{actual})').expect(HighlightedQuestionEdit.localizedQuestionTable.rowCount).toEqual(3);
    });

    it('should delete completely the question when there is no more localized version', () => {
      HighlightedQuestionEdit.deleteLocalizedQuestionButton(0).click();
      Main.confirmDialog.accept();
      HighlightedQuestionEdit.deleteLocalizedQuestionButton(0).click();
      Main.confirmDialog.accept();
      HighlightedQuestionEdit.deleteLocalizedQuestionButton(0).click();
      Main.confirmDialog.accept();
      waitForInvisibility(HighlightedQuestionEdit.localizedQuestionTable.table);
      since('edit view should be closed').expect(HighlightedQuestionEdit.editMode).toBeFalsy();
      since('question count should be #{expected} (was #{actual})').expect(HighlightedQuestionEdit.questionCount).toEqual(0);
    });

    it('should be able to delete a question directly, without having to delete localized version', () => {
      HighlightedQuestionEdit.addQuestionButton.click();
      HighlightedQuestionEdit.questionLabelInput.value = 'Question';
      HighlightedQuestionEdit.uploadInput.file = '../resources/parameters/test-file-1.pdf';
      HighlightedQuestionEdit.languageSelect.select('Français');
      HighlightedQuestionEdit.uploadInput.uploadButton.click();
      waitForVisibility(HighlightedQuestionEdit.localizedQuestionTable.table);
      since('localized question count should be #{expected} (was #{actual})').expect(HighlightedQuestionEdit.localizedQuestionTable.rowCount).toEqual(1);

      HighlightedQuestionEdit.backButton.click();
      since('before delete: question count should be #{expected} (was #{actual})').expect(HighlightedQuestionEdit.questionCount).toEqual(1);

      HighlightedQuestionEdit.deleteQuestionButton(1).click();
      expect(Main.confirmDialog.content.getText()).toEqual('Voulez-vous vraiment supprimer la question numéro 1 ainsi que toutes ses traductions ?');
      Main.confirmDialog.accept();
      since('after delete: question count should be #{expected} (was #{actual})').expect(HighlightedQuestionEdit.questionCount).toEqual(0);
      ParameterEditPage.clickBackLink();
    });
  });

});
