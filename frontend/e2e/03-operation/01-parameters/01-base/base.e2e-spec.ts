/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { DashboardPage } from '../../../page-object/dashboard/dashboard.po';
import { BaseEdit } from '../../../page-object/operation/parameters/base-edit.po';
import { OperationManagementPage } from '../../../page-object/operation/operation-management.po';
import { ParameterEditPage } from '../../../page-object/operation/parameters/parameter-edit.po';
import { log } from "util";
import { browser } from 'protractor';
import { Main } from '../../../page-object/main.po';

let since = require('jasmine2-custom-message');

describe('Operation/Parameters/Base', () => {

  beforeAll(() => {
    Main.login();
    Main.navigateTo('dashboard');
  });

  describe('base parameters edit section', () => {

    it('should display the base parameters section when clicking on the [CREATE] button', () => {
      // when
      DashboardPage.createButton.click();

      // then
      expect(BaseEdit.root.displayed).toBeTruthy();
    });

    it('card title should be "Opération"', () => {
      expect(BaseEdit.root.title).toBe('Opération');
    });

    it('card subtitle should be "Définir les paramètres de l\'opération"', () => {
      expect(BaseEdit.root.subtitle).toContain('Définir les paramètres de l\'opération');
    });

    it('long label input should have placeholder "Libellé de l\'opération"', () => {
      expect(BaseEdit.longLabelInput.placeholder).toBe('Libellé de l\'opération');
      since('input should be required').expect(BaseEdit.longLabelInput.required).toBeTruthy();
    });

    it('short label input should have placeholder "Libellé court"', () => {
      expect(BaseEdit.shortLabelInput.placeholder).toBe('Libellé court');
      since('input should be required').expect(BaseEdit.shortLabelInput.required).toBeTruthy();
    });

    it('date input should have placeholder "Date de l\'opération"', () => {
      expect(BaseEdit.dateInput.placeholder).toBe('Date de l\'opération *');
      since('input should be required').expect(BaseEdit.dateInput.required).toBeTruthy();
    });

    it('[SAVE] button should be present', () => {
      since('button should be displayed').expect(BaseEdit.saveButton.displayed).toBeTruthy();
      expect(BaseEdit.saveButton.text).toBe('ENREGISTRER');
    });
  });

  describe('save base parameters', () => {

    it('[SAVE] button should be disable if there is no change on the form', () => {
      expect(BaseEdit.saveButton.enabled).toBeFalsy();
    });

    it('[SAVE] button should be enabled when there is a change on the form', () => {
      BaseEdit.longLabelInput.value = 'Élections communales du 12 juin 2057';
      expect(BaseEdit.saveButton.enabled).toBeTruthy();
    });

    it('should display error messages if missing input', () => {
      // when
      BaseEdit.saveButton.click();

      // then
      since('short label input should be in error').expect(BaseEdit.shortLabelInput.error).toBe('Champ obligatoire');
      since('date input should be in error').expect(BaseEdit.dateInput.error).toBe('Champ obligatoire');
    });

    it('should create the operation and switch to the operation management', () => {
      // given
      BaseEdit.shortLabelInput.value = 'EL062057';
      BaseEdit.dateInput.value = '12.06.2057';

      // when
      BaseEdit.saveButton.click(true);

      // then
      expect(OperationManagementPage.title).toBe('Élections communales du 12 juin 2057 12.06.2057');

      // extract operation ID for other tests
      browser.getCurrentUrl().then((url) => {
        Main.operationId = +url.match(`^${browser.baseUrl}operations/([0-9]*)/parameters$`)[1];
        log(`operation ID is ${Main.operationId}`);
      });
    });

    it('should display the parameters tab', () => {
      expect(OperationManagementPage.isNavigationTabDisplayed(0)).toBeTruthy();
      expect(OperationManagementPage.getNavigationTabName(0)).toBe('Paramétrage');
    });
  });

  describe('base parameters summary card', () => {

    it('title should be "Opération *"', () => {
      expect(OperationManagementPage.baseCard.title).toBe('Opération *');
    });

    it('subtitle should be "Information de base concernant l\'opération"', () => {
      expect(OperationManagementPage.baseCard.subtitle).toBe('Information de base concernant l\'opération');
    });

    it('content should display the operation\'s long label', () => {
      since('label should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.baseCard.getContentLabel(0)).toBe('Libellé de l\'opération');
      since('value should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.baseCard.getContentValue(0)).toBe('Élections communales du 12 juin 2057');
    });

    it('content should display the operation\'s short label', () => {
      since('label should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.baseCard.getContentLabel(1)).toBe('Libellé court');
      since('value should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.baseCard.getContentValue(1)).toBe('EL062057');
    });

    it('content should display the operation\'s date', () => {
      since('label should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.baseCard.getContentLabel(2)).toBe('Date de l\'opération');
      since('value should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.baseCard.getContentValue(2)).toBe('12.06.2057');
    });

    it('[CONFIGURE] button should be present', () => {
      expect(OperationManagementPage.baseCard.buttonLabel).toBe('CONFIGURER');
    });

    it('completed badge should be present', () => {
      expect(OperationManagementPage.baseCard.completeBadgeDisplayed).toBeTruthy();
    });
  });

  describe('form modification detection', () => {

    it('should be able to enter edit mode and quit without any warning if no change has been made', () => {
      OperationManagementPage.baseCard.button.click();
      ParameterEditPage.clickBackLink();
      expect(OperationManagementPage.baseCard.present).toBeTruthy();
    });

    it('should not quit the current screen when user try to quit with unsaved changes and doesn\'t confirm', () => {
      OperationManagementPage.baseCard.button.click();
      BaseEdit.longLabelInput.value = 'Élections communales du 12 juin 2057 modified';
      ParameterEditPage.clickBackLink(false);

      expect(Main.confirmDialog.content.getText()).toBe('Voulez vous vraiment interrompre l\'action en cours ?');
      Main.confirmDialog.cancel();
      expect(OperationManagementPage.baseCard.present).toBeFalsy();
    });

    it('should quit the current screen when user try to quit with unsaved changes and confirm', () => {
      ParameterEditPage.clickBackLink(false);
      Main.confirmDialog.accept();
      expect(OperationManagementPage.baseCard.present).toBeTruthy();
    });
  });

});

