/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { OperationOption } from "../../../shared/mock-server";
import { OperationManagementPage } from "../../../page-object/operation/operation-management.po";
import { Main } from '../../../page-object/main.po';
import { RegisterEdit } from '../../../page-object/operation/voting-material/register/register-edit.po';
import { RegisterUpload } from '../../../page-object/operation/voting-material/register/register-upload.po';
import { VotingMaterialEditPage } from '../../../page-object/operation/voting-material/voting-material-edit.po';
import { RegisterSummary } from '../../../page-object/operation/voting-material/register/register-summary.po';
import { waitForVisibility } from '../../../shared/e2e.utils';

let since = require('jasmine2-custom-message');

describe("Operation/Voting material/Register", () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      "operations/[OPERATION_ID]/voting-material",
      'test_register',
      'TEST_e2e_register',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION,
        OperationOption.ADD_REPOSITORY_ELECTION,
        OperationOption.ADD_DOCUMENT,
        OperationOption.ADD_TESTING_CARDS,
        OperationOption.FOR_SIMULATION,
      ],
    );
  });

  describe('register edit section', () => {

    it('should display the register edit section when clicking on the [CONFIGURE] button', () => {
      // when
      OperationManagementPage.registerCard.button.click();
      // then
      expect(RegisterEdit.root.displayed).toBeTruthy();
    });

    it('card title should be "Registre électoral"', () => {
      expect(RegisterEdit.root.title).toBe('Registre électoral');
    });

    it('card subtitle should be "Liste des registres électoraux importés"', () => {
      expect(RegisterEdit.root.subtitle).toContain('Liste des registres électoraux importés');
    });

    it('[IMPORT REGISTER] button should be present', () => {
      since('button should be displayed').expect(RegisterEdit.uploadButton.displayed).toBeTruthy();
      expect(RegisterEdit.uploadButton.text).toBe('file_upload IMPORTER UN REGISTRE ÉLECTORAL');
    });
  });

  describe('upload a register', () => {
    it('register upload page should be displayed when clicking on the [IMPORT REGISTER] button', () => {
      RegisterEdit.uploadButton.click();
      expect(RegisterUpload.root.displayed).toBeTruthy();
    });

    it('card title should be "Registre électoral"', () => {
      expect(RegisterUpload.root.title).toBe('Registre électoral');
    });

    it('card subtitle should be "Importer un registre électoral pour l\'opération : ..."', () => {
      expect(RegisterUpload.root.subtitle).toContain('Importer un registre électoral pour l\'opération : ');
    });

    it('[BROWSE] button should be present', () => {
      since('button should be displayed').expect(RegisterUpload.uploadInput.browseButton.displayed).toBeTruthy();
      expect(RegisterUpload.uploadInput.browseButton.text).toBe('attach_file PARCOURIR');
    });

    it('[PREVIEW] button should be present and disabled', () => {
      since('button should be displayed').expect(RegisterUpload.uploadInput.uploadButton.displayed).toBeTruthy();
      since('button should be disable').expect(RegisterUpload.uploadInput.uploadButton.enabled).toBeFalsy();
      expect(RegisterUpload.uploadInput.uploadButton.text).toBe('search PRÉ-VISUALISER');
    });

    it('[CANCEL] button should be present', () => {
      since('button should be displayed').expect(RegisterUpload.cancelButton.displayed).toBeTruthy();
      expect(RegisterUpload.cancelButton.text).toBe('ABANDONNER');
    });

    it('should failed importing a file with wrong number of voters', () => {
      RegisterUpload.uploadInput.file = '../resources/register/wrong-number-of-voters.xml';
      RegisterUpload.uploadInput.uploadButton.click();
      expect(RegisterUpload.businessErrors).toBe(
        'Le nombre d\'électeurs défini dans l\'en-tête du fichier eCH-0045 (5) ne correspond pas au nombre d\'électeurs effectivement lu (4)');
    });

    it('should failed importing a wrong xml', () => {
      RegisterUpload.uploadInput.file = '../resources/register/wrong-xml.xml';
      RegisterUpload.uploadInput.uploadButton.click();
      expect(RegisterUpload.validationErrors.isDisplayed()).toBeTruthy();
    });

    it('should failed importing a file with duplicates', () => {
      RegisterUpload.uploadInput.file = '../resources/register/duplicates.xml';
      RegisterUpload.uploadInput.uploadButton.click();
      expect(RegisterUpload.duplicateErrors.isDisplayed()).toBeTruthy();
    });

    it('should failed importing a file with undefined domain of influence', () => {
      RegisterUpload.uploadInput.file = '../resources/register/undefined-doi.xml';
      RegisterUpload.uploadInput.uploadButton.click();
      expect(RegisterUpload.undefinedDoiErrors.isDisplayed()).toBeTruthy();
      // registerUpload.undefinedDoiErrorsDownloadButton.click();
      // expect(lastDownloadedContent()).toEqual(undefinedDoiCsv);
    });

    it('should display a report when importing a valid xml', () => {
      RegisterUpload.uploadInput.file = '../resources/register/valid.xml';
      RegisterUpload.uploadInput.uploadButton.click();
      expect(RegisterUpload.reportTable.cellValue(0, 'countingCircleCount')).toBe('3');
      expect(RegisterUpload.reportTable.cellValue(0, 'doiCount')).toBe('3');
      expect(RegisterUpload.reportTable.cellValue(0, 'voterCount')).toBe('4');
      expect(RegisterUpload.reportTable.cellValue(0, 'emissionDate')).toBe("04.05.2018");
      expect(RegisterUpload.reportTable.cellValue(0, 'emitter')).toBe("id.register name");
      expect(RegisterUpload.reportTable.cellValue(0, 'fileName')).toBe("valid.xml");
    });

    it('should warn user trying to quit if he has not validate the report', () => {
      VotingMaterialEditPage.clickBackLink(false);
      expect(Main.confirmDialog.content.getText()).toBe('Voulez vous vraiment interrompre l\'action en cours ?');
      Main.confirmDialog.cancel();
    });

    it('should allow validation', () => {
      RegisterUpload.validateButton.click();
      expect(RegisterEdit.root.displayed).toBeTruthy();
    });

    it('register summary should be displayed', () => {
      expect(RegisterSummary.root.rowCount).toBe(1);
      expect(RegisterSummary.countingCircleCount(0)).toBe('3');
      expect(RegisterSummary.doiCount(0)).toBe('3');
      expect(RegisterSummary.voterCount(0)).toBe('4');
      expect(RegisterSummary.emissionDate(0)).toBe("04.05.2018");
      expect(RegisterSummary.emitter(0)).toBe("id.register name");
      expect(RegisterSummary.fileName(0)).toBe("valid.xml");
    });
  });

  describe('delete a register', () => {
    it('should open a confirmation dialog when clicking on the [DELETE] button', () => {
      // when
      RegisterSummary.deleteButton(0).click();

      // then
      since('the confirmation dialog should be displayed').expect(Main.confirmDialog.content.isDisplayed())
        .toBeTruthy();
      expect(Main.confirmDialog.content.getText()).toBe('Souhaitez-vous supprimer le registre électoral valid.xml ?');
    });

    it('cancelling action should close the dialog', () => {
      // when
      Main.confirmDialog.cancel();
      // then
      expect(Main.confirmDialog.content.isPresent()).toBeFalsy();
      expect(RegisterSummary.root.rowCount).toBe(1);
    });

    it('accepting action should remove the register file', () => {
      // when
      RegisterSummary.deleteButton(0).click();
      Main.confirmDialog.accept();
      // then
      expect(Main.confirmDialog.content.isPresent()).toBeFalsy();
      since('summary table should be hidden').expect(RegisterSummary.root.displayed).toBeFalsy();
    });
  });

  describe('register summary card', () => {

    it('title should be "Registre électoral *"', () => {
      VotingMaterialEditPage.clickBackLink();
      Main.updateOperation([OperationOption.ADD_REGISTER_SE,OperationOption.ADD_REGISTER_SR]);
      Main.refresh();
      waitForVisibility(OperationManagementPage.registerCard.card);
      expect(OperationManagementPage.registerCard.title).toBe('Registre électoral *');
    });

    it('subtitle should be "Informations concernant les registres électoraux"', () => {
      expect(OperationManagementPage.registerCard.subtitle).toBe('Informations concernant les registres électoraux');
    });

    it('content should display the register information', () => {
      since('first label should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.registerCard.getContentLabel(0)).toBe('Nombre d\'électeurs');
      since('first value should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.registerCard.getContentValue(0)).toBe('20');
      since('second label should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.registerCard.getContentLabel(1)).toBe('Nombre de fichiers de registre');
      since('second value should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.registerCard.getContentValue(1)).toBe('2');
    });

    it('[CONFIGURE] button should be present', () => {
      expect(OperationManagementPage.registerCard.buttonLabel).toBe('CONFIGURER');
    });

    it('completed badge should be present', () => {
      expect(OperationManagementPage.registerCard.completeBadgeDisplayed).toBeTruthy();
    });
  });
});
