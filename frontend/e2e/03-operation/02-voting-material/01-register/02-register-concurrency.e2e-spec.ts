/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser, by, ProtractorBrowser } from 'protractor';
import { Main } from '../../../page-object/main.po';
import { UploadInput } from '../../../shared/upload-input';
import { RegisterUpload } from '../../../page-object/operation/voting-material/register/register-upload.po';
import { Button } from '../../../shared/button';
import { RegisterSummary } from '../../../page-object/operation/voting-material/register/register-summary.po';
import { RegisterEdit } from '../../../page-object/operation/voting-material/register/register-edit.po';
import { MatTable } from '../../../shared/table';
import { OperationOption } from '../../../shared/mock-server';

describe('Operation/Voting material/Register/Concurrency', () => {
  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      "operations/[OPERATION_ID]/voting-material",
      'test_register',
      'TEST_e2e_register',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION,
        OperationOption.ADD_REPOSITORY_ELECTION,
        OperationOption.ADD_DOCUMENT,
        OperationOption.ADD_TESTING_CARDS,
        OperationOption.FOR_SIMULATION,
      ]
    );
  });

  let browser2: ProtractorBrowser;
  let browser2UploadInput: UploadInput;
  let browser2ValidateButton: Button;
  let browser2RegisterFilesTable: MatTable;

  it('should start another browser', () => {
    browser2 = browser.forkNewDriverInstance(true);
    Main.login('ge1', false, browser2);
    Main.navigateTo('operations/[OPERATION_ID]/voting-material/register/upload', browser2);
    Main.navigateTo('operations/[OPERATION_ID]/voting-material/register');

    browser2UploadInput =
      new UploadInput(browser2.element(by.id('registerUpload')).element(by.className('mat-card-content')));

    Main.screenshot('browser 2 upload page', browser2);
    expect(browser2UploadInput.browseButton.displayed).toBeTruthy();
  });

  it('first user should preview the register import', () => {
    RegisterEdit.uploadButton.click();
    RegisterUpload.uploadInput.file = '../resources/register/valid.xml';
    RegisterUpload.uploadInput.uploadButton.click();
    expect(RegisterUpload.reportTable.displayed).toBeTruthy();
  });

  it('second user should preview the register import', () => {
    browser2ValidateButton = new Button('validateImport', browser2);
    browser2RegisterFilesTable = new MatTable('registerFilesTable', browser2);

    browser2UploadInput.file = '../resources/register/valid2.xml';
    browser2UploadInput.uploadButton.click();
    browser2ValidateButton.click();
    Main.screenshot('browser 2 registers', browser2);
    expect(browser2RegisterFilesTable.displayed).toBeTruthy();
  });

  it('should fail to import report when an import has already been done by another user', () => {
    RegisterUpload.validateButton.click();
    expect(Main.businessErrorDialog.waitContentTextToBe(
      'Un autre utilisateur a activé la fonctionnalité d\'import d\'un registre électoral. ' +
      'L\'analyse de détection des doublons doit être relancée.'));
    Main.businessErrorDialog.close();
  });

  it('should allow user to reimport', () => {
    RegisterUpload.uploadInput.file = '../resources/register/valid.xml';
    RegisterUpload.uploadInput.uploadButton.click();
    RegisterUpload.validateButton.click(true);
    expect(RegisterSummary.root.rowCount).toBe(2);
  });

  it('should close other browser', () => {
    browser2.quit();
  });
});
