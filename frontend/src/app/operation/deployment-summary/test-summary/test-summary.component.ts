/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnInit } from '@angular/core';
import {
  COMPLETE, DEPLOYMENT_REFUSED, DEPLOYMENT_REQUESTED, IN_ERROR, IN_VALIDATION, INCOMPLETE, INVALIDATED,
  MessageWithParameters, NOT_REQUESTED, OperationStatus, TEST_SITE_IN_DEPLOYMENT, VALIDATED
} from '../../model/operation-status';
import { Operation } from '../../model/operation';
import { TranslateService } from '@ngx-translate/core';
import { AuthorizationService } from '../../../core/service/http/authorization.service';

@Component({
  selector: 'test-summary',
  templateUrl: './test-summary.component.html',
  styleUrls: ['./../deployment-summary.component.scss']
})
export class TestSummaryComponent implements OnInit {
  @Input()
  operationStatus: OperationStatus;

  @Input()
  operation: Operation;

  constructor(private translateService: TranslateService, private authorizationService: AuthorizationService) {
  }

  get isTestSiteAccessible() {
    return this.operationStatus.configurationStatus.state === IN_VALIDATION ||
           this.operationStatus.tallyArchiveStatus.state === NOT_REQUESTED &&
           (this.operationStatus.votingPeriodStatus.state === VALIDATED &&
           this.operation.date >= new Date(Date.now()));
  }

  /**
   * Check if block containing URL of VR Site and/or testing cards archive should be displayed
   */
  get displayLinks() {
    return this.operationStatus.configurationStatus.state === IN_VALIDATION ||
           this.operationStatus.configurationStatus.state === VALIDATED;
  }

  get testStatusIcon() {
    switch (this.operationStatus.configurationStatus.state) {
      case INCOMPLETE :
      case DEPLOYMENT_REQUESTED :
        return "info_outline";
      case COMPLETE:
      case VALIDATED:
        return "check";
      case TEST_SITE_IN_DEPLOYMENT:
        return "timelapse";
      case IN_VALIDATION:
        return "assignment";
      case INVALIDATED:
      case DEPLOYMENT_REFUSED:
        return "cancel";
      case IN_ERROR:
        return "error";
      default :
        return null;
    }
  }

  get isTestStatusInError() {
    const state = this.operationStatus.configurationStatus.state;
    return state === INVALIDATED ||
           state === DEPLOYMENT_REFUSED ||
           state === IN_ERROR;
  }

  get inTestStatusMessage() {
    return this.translateMessageWithParameters(this.operationStatus.inTestStatusMessage);
  }

  get userBelongsToOperationManagementEntity() {
    return this.operation.managementEntity === this.authorizationService.userSnapshot.managementEntity;
  }

  get goToVoteReceiver() {
    return window.open(this.operationStatus.configurationStatus.voteReceiverUrl, "vote_receiver");
  }

  ngOnInit() {
  }

  translateMessageWithParameters(message: MessageWithParameters) {
    return this.translateService.instant(message.message, message.parameters);
  }

}
