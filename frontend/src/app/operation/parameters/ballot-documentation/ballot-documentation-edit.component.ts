/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Operation } from '../../model/operation';
import { MatSnackBar, MatSort, MatSortable } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { sortOnProperty } from '../../../core/util/table-utils';
import { BallotDocumentationService } from './service/ballot-documentation.service';
import { FileService } from '../../../core/service/file-service/file-service';
import { TdDialogService } from '@covalent/core';
import { TranslateService } from '@ngx-translate/core';
import { map, merge } from 'rxjs/operators';
import { AuthorizationService } from '../../../core/service/http/authorization.service';
import { ConfirmBeforeQuit } from '../../../core/confirm-before-quit';
import { UploadInputComponent } from '../../../shared/upload-input/upload-input.component';
import { Subscription } from 'rxjs/Subscription';
import { ReadOnlyService } from '../../service/read-only.service';
import { BallotDocumentation } from './model/ballot-documentation';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OperationManagementService } from '../../service/operation-managment.service';
import { of } from 'rxjs/observable/of';
import { OperationDataService } from '../../service/operation.data.service';
import { isFormModifiedAndNotSaved } from '../../../core/util/form-utils';


const ALL_LANGUAGES = [
  {value: 'DE', labelKey: 'parameters.ballot-documentation.edit.form.fields.language.label-key.DE'},
  {value: 'FR', labelKey: 'parameters.ballot-documentation.edit.form.fields.language.label-key.FR'},
  {value: 'IT', labelKey: 'parameters.ballot-documentation.edit.form.fields.language.label-key.IT'},
  {value: 'RM', labelKey: 'parameters.ballot-documentation.edit.form.fields.language.label-key.RM'}
];


@Component({
  templateUrl: './ballot-documentation-edit.component.html',
  styleUrls: ['../parameter-edit.scss', './ballot-documentation-edit.component.scss']
})
export class BallotDocumentationEditComponent implements OnInit, OnDestroy, ConfirmBeforeQuit {
  private _subscriptions: Subscription[] = [];
  private _readOnly = false;

  allBallots = [];
  possibleBallots = [];
  possibleLanguages = [];
  duplicateFile: string;
  operation: Operation;
  ballotDocumentations: BallotDocumentation[] = [];
  dataSource: BallotDocumentationDataSource;
  @ViewChild(UploadInputComponent)
  uploadInputComponent: UploadInputComponent;
  @ViewChild(MatSort) sort: MatSort;
  form: FormGroup;


  constructor(fb: FormBuilder,
              private operationManagementService: OperationManagementService,
              private ballotDocumentationService: BallotDocumentationService,
              private operationDataService: OperationDataService,
              private fileService: FileService,
              private dialogService: TdDialogService,
              private translateService: TranslateService,
              private authorizationService: AuthorizationService,
              private readOnlyService: ReadOnlyService,
              private snackBar: MatSnackBar) {


    this.form = fb.group({
        localizedLabel: [null, [Validators.required, Validators.maxLength(30)]],
        ballotId: [null, [Validators.required,]],
        language: [null, [Validators.required,]],
      },
      {validator: this.labelExistenceValidator('localizedLabel', 'ballotId', 'language')}
    );

  }

  /**
   * This validator is to check that file label is unique for the same ballot and language
   * @param localizedLabel
   * @param ballotId
   * @param language
   */
  labelExistenceValidator(localizedLabel: string, ballotId: string, language: string) {

    return (group: FormGroup): { [key: string]: any } => {
      let selectedLocalizedLabel = group.controls[localizedLabel].value;
      let selectedBallot = group.controls[ballotId].value;
      let selectedLanguage = group.controls[language].value;

      for (let bd of  this.ballotDocumentations) {
        if (bd.ballotId === selectedBallot && bd.language === selectedLanguage &&
            bd.localizedLabel === selectedLocalizedLabel) {
          this.duplicateFile = this.translateService.instant(
            `parameters.ballot-documentation.edit.form.duplicate-localized-label`,
            {language: selectedLanguage, ballotId: selectedBallot});
          return {labelExists: true};
        }
      }
      return null;
    }
  }

  ngOnInit() {
    this._subscriptions.push(
      this.operationManagementService.operation.subscribe(operation => {
          this.operation = operation;
          this.ballotDocumentationService.list(operation.id)
            .subscribe(ballotDocumentations => this.updateDataSource(ballotDocumentations));

          this.operationDataService.getAllRelatedBallots(operation.id, true, true)
            .subscribe(ballots => {
              this.allBallots = ballots;
              this.updatePossibleBallots();
            });

        }
      ),
      this.readOnlyService.isRepositoryInReadOnly()
        .subscribe(
          readOnly => {
            this._readOnly = readOnly;
          }
        ),
    );

    //The first sort is done on ballotId column
    // To conform to the SFD, the second column to be sorted is language but matsort doesn't support this feature yet.
    // An issue is opened for this purpose
    // TODO : https://github.com/angular/material2/issues/7226
    this.sort.sort(<MatSortable>{
        id: "ballotId",
        start: "asc",
      }
    );
  }

  get readOnly() {
    return this._readOnly;
  }

  get displayedColumns() {
    let readableColumn = ['managementEntity', 'ballotId', 'file', 'localizedLabel', "language"];
    if (this.readOnly || !this.authorizationService.hasAtLeastOneRole(["DELETE_BALLOT_DOCUMENTATION"])) {
      return readableColumn;
    }
    return readableColumn.concat(['delete']);
  }

  get uploadUrl() {
    return this.ballotDocumentationService.serviceUrl(this.operation.id) + "?" +
           "localizedLabel=" + encodeURIComponent(this.form.value.localizedLabel) + "&" +
           "language=" + encodeURIComponent(this.form.value.language) + "&" +
           "ballotId=" + encodeURIComponent(this.form.value.ballotId);
  }

  updatePossibleBallots() {
    this.possibleBallots = this.allBallots;
  }

  updatePossibleLanguages() {
    this.possibleLanguages = ALL_LANGUAGES;
  }

  checkBeforeUpload() {
    return () => {
      this.form.controls.localizedLabel.markAsTouched();
      this.form.controls.ballotId.markAsTouched();
      this.form.controls.language.markAsTouched();
      this.form.updateValueAndValidity();
      return this.form.valid;
    }
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach(s => s.unsubscribe());
  }


  updateDataSource(ballotDocumentations) {
    this.ballotDocumentations = ballotDocumentations;
    this.form.reset({localizedLabel: null, ballotId: null, language: null});
    this.updatePossibleLanguages();
    this.updatePossibleBallots();
    this.dataSource = new BallotDocumentationDataSource(this.translateService, this.ballotDocumentations, this.sort);
  }

  isFormModified(): boolean {
    return this.uploadInputComponent && this.uploadInputComponent.isFormModified() ||
           isFormModifiedAndNotSaved(this.form, false);
  }

  processUploadResult(result: BallotDocumentation) {
    this.snackBar.open(
      this.translateService.instant('parameters.ballot-documentation.edit.form.import-success'),
      '', {duration: 5000}
    );

    this.updateDataSource([...this.ballotDocumentations, result]);
  }

  download(e, element: BallotDocumentation) {
    this.ballotDocumentationService.download(this.operation.id, element.id).subscribe(blob => {
      this.fileService.downloadBlob(element.fileName, blob)
    });
    e.preventDefault();
  }

  canDeleteFile(element: BallotDocumentation) {
    let userManagementEntity = this.authorizationService.userSnapshot.managementEntity;
    return (userManagementEntity === this.operation.managementEntity ||
            userManagementEntity === element.managementEntity);
  }

  deleteFile(element: BallotDocumentation) {
    this.dialogService.openConfirm({
      message: this.translateService.instant('parameters.ballot-documentation.edit.delete-dialog.message', element),
      disableClose: true,
      cancelButton: this.translateService.instant('global.actions.no'),
      acceptButton: this.translateService.instant('global.actions.yes'),
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.ballotDocumentationService.remove(this.operation.id, element.id).subscribe(() => {
          this.snackBar.open(
            this.translateService.instant(
              'parameters.ballot-documentation.edit.delete-success'),
            '',
            {duration: 5000}
          );
          this.updateDataSource(this.ballotDocumentations.filter(bd => bd.id !== element.id));
        });
      }
    });
  }
}

class BallotDocumentationDataSource extends DataSource<BallotDocumentation> {

  constructor(private translateService: TranslateService, private ballotDocumentations: BallotDocumentation[],
              private sort: MatSort) {
    super();
  }

  connect(): Observable<BallotDocumentation[]> {
    return of(this.ballotDocumentations)
      .pipe(
        merge(this.sort.sortChange),
        map(() => this.ballotDocumentations.map(
          bdoc => Object.assign({}, bdoc, {
            language: this.translateService.instant(
              `parameters.ballot-documentation.edit.form.fields.language.label-key.${bdoc.language}`)
          })
        )),
        map((data) => sortOnProperty(data, this.sort.active, this.sort.direction)));
  }

  disconnect() {
  }
}

