/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpParameters } from '../../../../core/service/http/http.parameters.service';
import { BallotDocumentation } from '../model/ballot-documentation';

@Injectable()
export class BallotDocumentationService {
  public serviceUrl;

  constructor(private http: HttpClient, private params: HttpParameters) {
    this.serviceUrl = (operationId) => `${this.params.apiBaseURL}/operation/${operationId}/ballot-documentation`;
  }

  /**
   * Retrieve the list of ballot documentation for an operation
   */
  list(operationId: number): Observable<BallotDocumentation[]> {
    return this.http.get<BallotDocumentation[]>(this.serviceUrl(operationId));
  }

  /**
   * Download the pdf of a given ballot documentation
   */
  download(operationId: number, ballotDocumentationId: number): Observable<Blob> {
    return this.http.get(`${this.serviceUrl(operationId)}/download/${ballotDocumentationId}`,
      {headers: new HttpHeaders({'Accept': 'application/pdf'}), responseType: 'blob'});
  }

  /**
   * Delete the given ballot documentation.
   */
  remove(operationId: number, ballotDocumentationId: number): Observable<any> {
    return this.http.delete(`${this.serviceUrl(operationId)}/${ballotDocumentationId}`);
  }
}
