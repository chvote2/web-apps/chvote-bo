/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_voting_site_configuration = {
  fr: {
    "card": {
      "title": "Configuration du site de vote",
      "subtitle": "",
      "default-language": "Langue par défaut"
    },
    "sidebar": {
      "title": "Site de vote"
    },
    "edit": {
      "title": "Configuration du site de vote",
      "subtitle": "",
      "form": {
        "fields": {
          "default-language": {
            "placeholder": "Sélectionner la langue par défaut"
          }
        }
      }
    },
    "default-languages": "Langue par défaut sur le site de vote",
    "available-languages": "Langues disponibles sur le site de vote",
    "language": {
      "DE": "Allemand",
      "FR": "Français",
      "IT": "Italien",
      "RM": "Romanche",
    }
  },
  de: {
    "card": {
      "title": "DE - Configuration du site de vote",
      "subtitle": "",
      "default-language": "DE - Langue par défaut"
    },
    "sidebar": {
      "title": "DE - Site de vote"
    },
    "edit": {
      "title": "DE - Configuration du site de vote",
      "subtitle": "",
      "form": {
        "fields": {
          "default-language": {
            "placeholder": "DE - Sélectionner la langue par défaut"
          }
        }
      }
    },
    "default-languages": "DE - Langue par défaut sur le site de vote",
    "available-languages": "DE - Langues disponibles sur le site de vote",
    "language": {
      "DE": "DE - Allemand",
      "FR": "DE - Français",
      "IT": "DE - Italien",
      "RM": "DE - Romanche",
    }
  }
};
