/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MilestoneType } from './model/milestone-type';
import { OperationParametersModel } from '../model/operation-parameters.model';
import { Milestone } from './model/milestone';
import { Operation } from '../../model/operation';
import { OperationService } from '../../service/operation.service';
import { TdDialogService } from '@covalent/core';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material';
import { Exceptions } from '../../../core/model/exceptions';
import * as _ from "underscore";
import * as moment from 'moment';
import { AuthorizationService } from '../../../core/service/http/authorization.service';
import { enableFormComponents, focusOnError, isFormModifiedAndNotSaved } from '../../../core/util/form-utils';
import { ConfirmBeforeQuit } from '../../../core/confirm-before-quit';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { ReadOnlyService } from 'app/operation/service/read-only.service';
import { MilestoneConfiguration } from './milestone-configuration';
import { OperationManagementService } from '../../service/operation-managment.service';

@Component({
  templateUrl: './milestone-edit.component.html',
  styleUrls: ['../parameter-edit.scss', './milestone-edit.component.scss']
})
export class MilestoneEditComponent implements OnInit, OnDestroy, ConfirmBeforeQuit {

  firstLoad = true;
  operation: Operation;
  milestoneTypes = Object.keys(MilestoneType).filter(key => !isNaN(Number(MilestoneType[key])));
  milestonesForm: FormGroup;
  now: Date = moment().add(1, 'days').hours(0).minutes(0).seconds(0).toDate();
  saved = false;

  private _subscriptions: Subscription[] = [];

  constructor(private formBuilder: FormBuilder,
              private parametersModel: OperationParametersModel,
              private operationService: OperationService,
              private operationManagementService: OperationManagementService,
              private dialogService: TdDialogService,
              private translateService: TranslateService,
              private router: Router,
              private authorizationService: AuthorizationService,
              private readOnlyService: ReadOnlyService,
              private snackBar: MatSnackBar) {
    this.createForm();
  }

  private _readOnly: boolean;

  get readOnly(): boolean {
    return this._readOnly;
  }

  get cardReadOnly(): boolean {
    return this._readOnly;
  }

  isFormModified(): boolean {
    return isFormModifiedAndNotSaved(this.milestonesForm, this.saved);
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach(s => s.unsubscribe());
  }

  ngOnInit() {
    this._subscriptions.push(
      this.readOnlyService.isMilestoneInReadOnly()
        .subscribe(
          readOnly => {
            let oldValue = this._readOnly;
            this._readOnly = readOnly;
            if (oldValue != this.readOnly || this.firstLoad) {
              this.updateFormEnable();
              this.firstLoad = false;
            }
          }
        ),

      this.parametersModel.currentOperation.subscribe(operation => {
          this.operation = operation;
          let milestones = this.indexByType(operation.milestones);

          let formValues = {};
          this.milestoneTypes.forEach(milestoneType => {
            formValues[milestoneType] = (milestones[milestoneType] && milestones[milestoneType].date !== undefined) ?
              moment(milestones[milestoneType].date) : null;
          });
          formValues['gracePeriod'] = operation.gracePeriod;
          this.milestonesForm.reset(formValues);
        }
      )
    );
  }

  save(): void {
    // reset previous server validation error
    this.milestonesForm.reset(this.milestonesForm.getRawValue());

    // validate form model
    this.milestoneTypes.forEach(milestoneType => {
      this.milestonesForm.controls[milestoneType].markAsTouched();
    });
    this.milestonesForm.controls.gracePeriod.markAsTouched();
    this.milestonesForm.updateValueAndValidity();

    if (!this.milestonesForm.valid) {
      this.milestonesForm.controls.gracePeriod.markAsDirty();
      focusOnError(this.milestonesForm);
    } else {
      // form is valid, update operation's milestones from form model
      const formModel = this.milestonesForm.getRawValue();
      let milestoneConfig: MilestoneConfiguration = {
        gracePeriod: formModel.gracePeriod,
        milestones: []
      };

      this.milestoneTypes.forEach(milestoneType => {
        let milestone = new Milestone();
        milestone.type = MilestoneType[milestoneType];
        milestoneConfig.milestones.push(milestone);
        if (Boolean(formModel[milestoneType]) && Boolean(formModel[milestoneType].date)) {
          milestone.date = formModel[milestoneType].toDate();
        } else {
          milestone.date = undefined;
        }
      });

      // send to backend
      this.operationService.updateMilestones(this.operation.id, milestoneConfig).subscribe(
        this.updateSuccess(),
        errorProvider => errorProvider.onValidationError(Exceptions.INVALID_MILESTONE_DATE,
          error => {
            error.fieldErrors.forEach(fieldError => {
              this.milestonesForm.controls[fieldError.field].setErrors(
                {'invalidDate': {'message': fieldError.message}});
            });
            focusOnError(this.milestonesForm);

          }
        )
      )
    }
  };

  clear() {
    this.dialogService.openConfirm({
      message: this.translateService.instant('parameters.milestone.edit.delete-dialog.message'),
      disableClose: true,
      cancelButton: this.translateService.instant('global.actions.no'),
      acceptButton: this.translateService.instant('global.actions.yes'),
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        let milestoneConfig: MilestoneConfiguration = {
          milestones: [],
          gracePeriod: null
        };
        this.operationService.updateMilestones(this.operation.id, milestoneConfig).subscribe(this.updateSuccess());
      }
    });
  }

  private updateSuccess() {
    return savedOperation => {
      this.parametersModel.updateOperation(savedOperation);
      this.saved = true;
      this.router.navigate(['/operations', savedOperation.id, 'parameters']);

      this.snackBar.open(
        this.translateService.instant('parameters.milestone.edit.form.save-success',
          {operation: savedOperation.longLabel}), '', {duration: 5000});
    };
  }

  private updateFormEnable() {
    enableFormComponents(this.milestonesForm.controls,
      !this._readOnly && this.authorizationService.hasAtLeastOneRole(['UPDATE_MILESTONE']));
  }

  private createForm() {
    let formGroup = {};
    this.milestoneTypes.forEach(milestoneType => {
      formGroup[milestoneType] = [
        {value: null}
      ];
    });
    formGroup['gracePeriod'] = [
      {value: null},
      [
        Validators.required,
        Validators.min(0),
        Validators.max(60)
      ]
    ];

    this.milestonesForm = this.formBuilder.group(formGroup);
  }

  private indexByType(milestones: Milestone[]) {
    return _.indexBy(milestones, milestone => {
      return (typeof milestone.type === 'string') ? milestone.type : MilestoneType[milestone.type];
    });
  }

}
