/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_parameters_doi = {
  fr: {
    "card": {
      "title": "Domaines d'influence",
      "subtitle": "",
      "1-domain": "1 domaine",
      "n-domain": "{{count}} domaines"
    },
    "edit": {
      "title": "Domaines d'influence",
      "subtitle": "Liste des domaines d'influence de l'opération",
      "form": {
        "fields": {
          "inputFile": {
            "placeholder": "Sélectionner un fichier de domaines d'influence"
          }
        },
        "import-success": "Le fichier \"{{fileName}}\" a été importé avec succès"
      },
      "delete-success": "Le fichier \"{{fileName}}\" a été supprimé avec succès",
      "delete-dialog": {
        "message": "Souhaitez-vous supprimer le fichier {{fileName}}\u00A0?"
      },
      "file-grid": {
        "header": {
          "file": "Fichier",
          "author": "Auteur",
          "imported-date": "Importé le",
          "delete": "Supprimer"
        },
      },
      "detail-grid": {
        "header": {
          "type": "Type de domaine d'influence",
          "id": "Identification locale",
          "name": "Nom du domaine d'influence",
          "shortName": "Nom abrégé"
        }
      }
    }
  },
  de: {
    "card": {
      "title": "DE - Domaines d'influence",
      "subtitle": "DE - ",
      "1-domain": "DE - 1 domaine",
      "n-domain": "DE - {{count}} domaines"
    },
    "edit": {
      "title": "DE - Domaines d'influence",
      "subtitle": "DE - Liste des domaines d'influence de l'opération",
      "form": {
        "fields": {
          "inputFile": {
            "placeholder": "DE - Sélectionner un fichier de domaines d'influence"
          }
        },
        "import-success": "DE - Le fichier \"{{fileName}}\" a été importé avec succès"
      },
      "delete-success": "DE - Le fichier \"{{fileName}}\" a été supprimé avec succès",
      "delete-dialog": {
        "message": "DE - Souhaitez-vous supprimer le fichier {{fileName}}\u00A0?"
      },
      "file-grid": {
        "header": {
          "file": "DE - Fichier",
          "author": "DE - Auteur",
          "imported-date": "DE - Importé le",
          "delete": "DE - Supprimer"
        },
        "contest": "DE - Fichier type domaines d'influence - {{contestIdentification}}"
      },
      "detail-grid": {
        "header": {
          "type": "DE - Type de domaine d'influence",
          "id": "DE - Identification locale",
          "name": "DE - Nom du domaine d'influence",
          "shortName": "DE - Nom abrégé"
        }
      }
    }
  }
};
