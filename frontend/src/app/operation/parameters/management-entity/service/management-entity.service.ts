/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpParameters } from '../../../../core/service/http/http.parameters.service';
import { Operation } from '../../../model/operation';

@Injectable()
export class ManagementEntityService {

  constructor(private http: HttpClient, private httpParams: HttpParameters) {

  }

  getAllManagementEntities() {
    return this.http.get<string[]>(`${this.httpParams.apiBaseURL}/management-entity/all`)
  }

  invite(operationId: number, managementEntities: string[]) {
    return this.http.put<Operation>(`${this.httpParams.apiBaseURL}/management-entity/invite-to-operation`,
      {managementEntities, operationId});
  }

  revoke(operationId: number, managementEntities: string[]) {
    return this.http.put<Operation>(`${this.httpParams.apiBaseURL}/management-entity/revoke-from-operation`,
      {managementEntities, operationId});
  }
}
