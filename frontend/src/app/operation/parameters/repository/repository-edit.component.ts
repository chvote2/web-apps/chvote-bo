/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { OperationParametersModel } from '../model/operation-parameters.model';
import { RepositoryFile } from './model/repository-file';
import { Operation } from '../../model/operation';
import { RepositoryImportResult } from './model/repository-import-result';
import { MatDialog, MatSnackBar, MatSort } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { sortOnProperty } from '../../../core/util/table-utils';
import { RepositoryService } from './service/repository.service';
import { FileService } from '../../../core/service/file-service/file-service';
import { TdDialogService } from '@covalent/core';
import { TranslateService } from '@ngx-translate/core';
import * as _ from "underscore";
import { DatePipe } from '@angular/common';
import { map } from 'rxjs/operators';
import { merge } from 'rxjs/observable/merge';
import { AuthorizationService } from '../../../core/service/http/authorization.service';
import { ConfirmBeforeQuit } from '../../../core/confirm-before-quit';
import { UploadInputComponent } from '../../../shared/upload-input/upload-input.component';
import { Subscription } from 'rxjs/Subscription';
import { ReadOnlyService } from '../../service/read-only.service';
import { SlideDetailsComponent } from '../../../shared/slide-details/slide-details.component';
import { OperationDataService } from '../../service/operation.data.service';
import { DomainOfInfluence } from '../../../model/domain-of-Influence.model';
import { OperationManagementService } from '../../service/operation-managment.service';

@Component({
  templateUrl: './repository-edit.component.html',
  styleUrls: ['../parameter-edit.scss', './repository-edit.component.scss']
})
export class RepositoryEditComponent implements OnInit, OnDestroy, ConfirmBeforeQuit {

  operation: Operation;
  repositories: RepositoryFile[];
  uploadResult: RepositoryImportResult = undefined;
  validationErrorCsvContent: string;
  private _isReplacementOpen = false;
  repositoriesDataSource: RepositoriesDataSource;
  @ViewChild(SlideDetailsComponent)
  slideDetailsComponent: SlideDetailsComponent;
  @ViewChild(UploadInputComponent)
  uploadInputComponent: UploadInputComponent;
  selectedFile: RepositoryFile;
  @ViewChild(MatSort) sort: MatSort;
  private _subscriptions: Subscription[] = [];
  allDois: DomainOfInfluence[];
  private inPartialModification = false;
  fileToReplace: RepositoryFile;

  constructor(private parametersModel: OperationParametersModel,
              private operationManagementService: OperationManagementService,
              private repositoryService: RepositoryService,
              private fileService: FileService,
              private dialogService: TdDialogService,
              private dialog: MatDialog,
              private translateService: TranslateService,
              private datePipe: DatePipe,
              private authorizationService: AuthorizationService,
              private operationDataService: OperationDataService,
              private readOnlyService: ReadOnlyService,
              private snackBar: MatSnackBar) {
  }

  private _readOnly = false;

  get readOnly() {
    return this._readOnly;
  }

  get repositoriesDisplayedColumns() {
    return ['managementEntity', 'type', 'file', 'author', 'importedDate', 'actions'];
  }

  get uploadUrl(): string {
    return `${this.repositoryService.serviceUrl}/${this.operation.id}/upload`
  }

  isElection(file: RepositoryFile) {
    return file.type === "ELECTION_REPOSITORY";
  }

  isVotation(file: RepositoryFile) {
    return file.type === "VOTATION_REPOSITORY";
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach(s => s.unsubscribe());
  }

  ngOnInit() {
    this._subscriptions.push(
      this.readOnlyService.isRepositoryInReadOnly()
        .subscribe(
          readOnly => {
            this._readOnly = readOnly;
          }
        ),
      this.parametersModel.currentOperation.subscribe(operation => {
        this.operation = operation;
        this.updateDoi();

      }),
      this.parametersModel.currentRepositoryFiles.subscribe(repositories => this.repositories = repositories),
      this.operationManagementService.status.subscribe(status => this.inPartialModification =
        status.configurationStatus.modificationMode == "IN_PARTIAL_MODIFICATION")
    );

    this.repositoriesDataSource = new RepositoriesDataSource(this.parametersModel, this.sort);
  }

  isFormModified(): boolean {
    return this.uploadInputComponent && this.uploadInputComponent.isFormModified();
  }

  get replaceUrl(): string {
    return `${this.repositoryService.serviceUrl}/${this.operation.id}/replace/${this.fileToReplace.id}`
  }

  get isReplacementOpen(): boolean {
    return this._isReplacementOpen;
  }

  set isReplacementOpen(value: boolean) {
    if (!value) {
      this.uploadResult = null;
      this.validationErrorCsvContent = null;
    }
    this._isReplacementOpen = value;
  }

  getContest(repository: RepositoryFile) {
    return {
      contestDescription: repository.contestDescription,
      contestIdentification: repository.contestIdentification,
      contestDate: this.datePipe.transform(repository.contestDate, 'd.MM.y')
    };
  }

  resetUploadResult() {
    this.uploadResult = undefined;
  }

  processUploadResult(result: RepositoryImportResult, forReplacement: boolean) {
    this.uploadResult = result;
    if (this.uploadResult) {
      if (this.uploadResult.validationErrors.length > 0) {
        this.validationErrorCsvContent = this.fileService.generateCsvFromFlatObject(
          this.uploadResult.validationErrors, "global.validation.csv.header");

      } else {
        if (forReplacement) {
          this.repositories.splice(this.repositories.indexOf(this.fileToReplace), 1);
          this.isReplacementOpen = false;
        }


        this.repositories.push(result.repositoryFile);
        this.parametersModel.updateRepositoryFiles(this.repositories);
        this.updateDoi();
        this.snackBar.open(
          this.translateService.instant(
            'parameters.repository.edit.form.import-success',
            {fileName: result.repositoryFile.fileName}
          ),
          '',
          {
            duration: 5000
          }
        );
      }
    }
  }

  downloadValidationReport() {
    this.fileService.downloadFile('repository_errors.csv', this.validationErrorCsvContent);
  }

  download(repository: RepositoryFile, event: Event) {
    event.preventDefault();  // prevent href link
    event.stopPropagation(); // prevent repository panel's expansion toggle

    this.repositoryService.download(repository.id).subscribe(blob => {
      this.fileService.downloadBlob(repository.fileName, blob);
    });
  }

  canDeleteFile(repository: RepositoryFile) {
    return !this.readOnly &&
           !this.inPartialModification &&
           this.authorizationService.hasAtLeastOneRole(["DELETE_REGISTER_FILE"]) &&
           (this.authorizationService.userSnapshot.managementEntity === this.operation.managementEntity ||
            this.authorizationService.userSnapshot.managementEntity === repository.managementEntity);
  }

  canReplaceFile(repository: RepositoryFile) {
    return !this.readOnly &&
           this.inPartialModification &&
           this.authorizationService.hasAtLeastOneRole(["DELETE_REGISTER_FILE"]) &&
           (this.authorizationService.userSnapshot.managementEntity === this.operation.managementEntity ||
            this.authorizationService.userSnapshot.managementEntity === repository.managementEntity);
  }


  openReplacementBox(repositoryFile: RepositoryFile) {
    this.isReplacementOpen = true;
    this.fileToReplace = repositoryFile;
  }




  deleteFile(repository: RepositoryFile) {
    this.dialogService.openConfirm({
      message: this.translateService.instant('parameters.repository.edit.delete-dialog.message',
        {fileName: repository.fileName}),
      disableClose: true,
      cancelButton: this.translateService.instant('global.actions.no'),
      acceptButton: this.translateService.instant('global.actions.yes'),
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.repositoryService.deleteFile(repository.id).subscribe(() => {
          this.snackBar.open(
            this.translateService.instant(
              'parameters.repository.edit.delete-success', {fileName: repository.fileName}),
            '',
            {duration: 5000}
          );
          this.repositories = _.reject(this.repositories, function (rep: RepositoryFile) {
            return rep.id === repository.id;
          });
          this.parametersModel.updateRepositoryFiles(this.repositories);
        });
      }
    });
  }

  selectFile(file: RepositoryFile) {
    this.selectedFile = file;
    this.slideDetailsComponent.showDetails();
  }

  detailsHasBeenClosed() {
    this.selectedFile = null;
  }

  private updateDoi() {
    this.operationDataService.getAllRelatedDois(this.operation.id).subscribe(dois => this.allDois = dois);
  }
}

class RepositoriesDataSource extends DataSource<RepositoryFile> {

  private repositories: RepositoryFile[];

  constructor(private parametersModel: OperationParametersModel,
              private sort: MatSort) {
    super();
    this.parametersModel.currentRepositoryFiles.subscribe(files => this.repositories = files);
  }

  connect(): Observable<RepositoryFile[]> {
    return merge(this.parametersModel.currentRepositoryFiles, this.sort.sortChange)
      .pipe(map(() => sortOnProperty(this.repositories, this.sort.active, this.sort.direction)));
  }

  disconnect() {
  }
}

