/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatSort, MatTableDataSource } from '@angular/material';
import { ElectionInformationList } from '../model/repository-file';

@Component({
  selector: 'election-details-electoral-lists',
  templateUrl: './election-details-electoral-lists.component.html',
  styleUrls: ['./election-details-electoral-lists.component.scss']
})
export class ElectionDetailsElectoralListComponent implements OnInit {
  displayedColumns = ["indentureNumber", "name", "inAnUnion", "candidateCount"];
  dataSource: MatTableDataSource<ElectionInformationList>;

  constructor(@Inject(MAT_DIALOG_DATA) lists: ElectionInformationList[]) {
    this.dataSource = new MatTableDataSource(lists);
  }

  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
  }

}
