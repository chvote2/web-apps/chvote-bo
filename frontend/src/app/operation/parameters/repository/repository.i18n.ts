/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_parameters_repository = {
  fr: {
    "card": {
      "title": "Référentiel(s)",
      "subtitle": "",
      "votation": "Votation",
      "election": "Élection",
      "no-repository": "aucun référentiel",
      "1-repository": "1 référentiel",
      "n-repository": "{{count}} référentiels"
    },
    "edit": {
      "title": "Référentiel(s) de l'opération",
      "subtitle": "Liste des référentiels de l'opération",
      "form": {
        "fields": {
          "inputFile": {
            "placeholder": "Sélectionner un fichier référentiel"
          }
        },
        "import-success": "Le référentiel \"{{fileName}}\" a été importé avec succès",
        "errors": {
          "date-mismatch": "La date d'opération lue dans le fichier référentiel \"{{param0}}\" est inconsistante avec la date opération"
        }
      },
      "actions": {
        "replace": {
          "button": "Remplacer",
          "title": "Remplacer le fichier : ",
          "place-holder": "Sélectionner la nouvelle version du référentiel",
          "cancel-button": "Ne pas remplacer",
        }
      },
      "delete-success": "Le référentiel \"{{fileName}}\" a été supprimé avec succès",
      "delete-dialog": {
        "message": "Souhaitez-vous supprimer le référentiel {{fileName}}\u00A0?"
      },
      "file-grid": {
        "header": {
          "management-entity": "Entité de gestion",
          "file": "Fichier",
          "author": "Auteur",
          "imported-date": "Importé le",
          "actions": "Actions",
          "type": "Type"
        },
        "contest": "{{contestDescription}} - {{contestIdentification}} - {{contestDate}}",
        "election_repository": "Election",
        "votation_repository": "Votation"
      },
      "election-details": {
        "not-in-a-union": "Non",
        "in-a-union": "Oui",
        "title": {
          "MAJORITY_WITH_LIST": "Système majoritaire avec liste",
          "MAJORITY": "Système majoritaire sans liste",
          "PROPORTIONAL": "Système proportionnel"
        },
        "electoral-list": {
          "table": {
            "header": {
              "indenture-number": "Numéro de liste",
              "name": "Nom de liste",
              "in-union": "Apparentement",
              "candidate-count": "Nombre de candidatures",
            }
          }
        },

        "table": {
          "header": {
            "ballot": "Nom du scrutin",
            "number-of-mandates": "Nombre de mandats (sièges)",
            "mandate": "Fonction",
            "list-count": "Nombre de listes",
            "candidature-count": "Nombre total candidatures",
            "candidate-count": "Nombre de candidats",
          }
        }
      },
      "detail-grid": {
        "header": {
          "subject-position": "Numéro d'affichage de la question",
          "domain-of-influence": "Domaine d'influence",
          "answer-type": "Types de réponse acceptés"
        },
        "answer-type-string": {
          "1": "Oui / Non",
          "2": "Oui / Non / Vide",
          "3": "Coché / Vide",
          "4": "Initiative / Contre-projet / Vide",
          "5": "Initiative / Contre-proposition / Vide",
          "6": "Objet / Proposition Populaire / Vide",
          "7": "Objet Principal / Proposition Subsidiaire / Vide",
        }
      }
    }
  },
  de: {
    "card": {
      "title": "DE - Référentiel(s)",
      "subtitle": "DE - ",
      "votation": "DE - Votation",
      "election": "DE - Élection",
      "no-repository": "DE - aucun référentiel",
      "1-repository": "DE - 1 référentiel",
      "n-repository": "DE - {{count}} référentiels"
    },
    "edit": {
      "title": "DE - Référentiel(s) de l'opération",
      "subtitle": "DE - Liste des référentiels de l'opération",
      "form": {
        "fields": {
          "inputFile": {
            "placeholder": "DE - Sélectionner un fichier référentiel"
          }
        },
        "import-success": "DE - Le référentiel \"{{fileName}}\" a été importé avec succès",
        "errors": {
          "date-mismatch": "DE - La date d'opération lue dans le fichier référentiel \"{{param0}}\" est inconsistante avec la date opération"
        }
      },
      "delete-success": "DE - Le référentiel \"{{fileName}}\" a été supprimé avec succès",
      "delete-dialog": {
        "message": "DE - Souhaitez-vous supprimer le référentiel {{fileName}}\u00A0?"
      },
      "file-grid": {
        "header": {
          "management-entity": "DE - Entité de gestion",
          "file": "DE - Fichier",
          "author": "DE - Auteur",
          "imported-date": "DE - Importé le",
          "delete": "DE - Supprimer"
        },
        "contest": "DE - Fichier type eCH-0159 Votation - {{contestDescription}} - {{contestIdentification}} - {{contestDate}}"
      },
      "detail-grid": {
        "header": {
          "subject-position": "DE - Numéro d'affichage de la question",
          "domain-of-influence": "DE - Domaine d'influence",
          "answer-type": "DE - Types de réponse acceptés"
        },
        "answer-type-string": {
          "2": "DE - OUI / NON / BLANC",
          "5": "DE - IN / CP / QS"
        }
      }
    }
  },
};
