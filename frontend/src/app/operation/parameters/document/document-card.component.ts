/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnChanges, OnDestroy, OnInit } from "@angular/core";
import { OperationFile } from "../../../core/model/operation-file";
import { TranslateService } from "@ngx-translate/core";
import * as _ from "underscore";
import { Subscription } from "rxjs/Subscription";
import { ReadOnlyService } from "../../service/read-only.service";
import { HighlightedQuestionService } from './highlighted-question/services/highlighted-question.service';
import { OperationManagementService } from '../../service/operation-managment.service';

@Component({
  selector: 'document-card',
  templateUrl: './document-card.component.html'
})
export class DocumentCardComponent implements OnChanges, OnInit, OnDestroy {
  private _subscriptions: Subscription[] = [];
  private _readOnly = false;
  private questions;

  @Input() files: OperationFile[];
  fileCountByType;
  completed = false;
  inError = false;

  constructor(private readOnlyService: ReadOnlyService,
              private translateService: TranslateService,
              private operationManagementService: OperationManagementService,
              private highlightedQuestionService: HighlightedQuestionService) {

  }


  ngOnDestroy(): void {
    this._subscriptions.forEach(s => s.unsubscribe());
  }

  get readOnly() {
    return this._readOnly;
  }

  ngOnInit() {

    this.highlightedQuestionService.list(this.operationManagementService.operation.getValue().id)
      .subscribe(questions => this.questions = questions);


    this._subscriptions.push(
      this.operationManagementService.status.subscribe(status => {
        this.completed = status.configurationStatus.completedSections["document"];
        this.inError = status.configurationStatus.sectionsInError["document"];
      }),
      this.readOnlyService.isDocumentInReadOnly()
        .subscribe(
          readOnly => {
            this._readOnly = readOnly;
          }
        ),
    );
  }

  get partiallyComplete() {
    return this.files.length > 0
  }

  ngOnChanges(): void {
    this.fileCountByType = _.countBy(this.files, file => {
      return file.type;
    });
  }

  documentCount(type: string): string {
    let count: number = this.fileCountByType[type];
    if (count === undefined || count === 0) {
      return this.translateService.instant('parameters.document.card.no-document');
    }
    if (count === 1) {
      return this.translateService.instant('parameters.document.card.1-document');
    }
    return this.translateService.instant('parameters.document.card.n-document', {count: count});
  }

  questionCount() {

    if (!this.questions || this.questions.length === 0) {
      return this.translateService.instant('parameters.document.card.no-question');
    }
    if (this.questions.length === 1) {
      return this.translateService.instant('parameters.document.card.1-question');
    }
    return this.translateService.instant('parameters.document.card.n-question', {count: this.questions.length});
  }
}
