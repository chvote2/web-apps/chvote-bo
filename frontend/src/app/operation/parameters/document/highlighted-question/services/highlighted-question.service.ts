/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpParameters } from '../../../../../core/service/http/http.parameters.service';
import { HighlightedQuestion } from '../model/highlighted-question';
import { OperationManagementService } from '../../../../service/operation-managment.service';
import { tap } from 'rxjs/operators';

@Injectable()
export class HighlightedQuestionService {
  public serviceUrl: (number) => string;

  constructor(private http: HttpClient, httpParams: HttpParameters,
              private operationManagementService: OperationManagementService) {
    this.serviceUrl = (operationId) => httpParams.apiBaseURL + `/operation/${operationId}/highlighted-question`;
  }

  list(operationId: number): Observable<HighlightedQuestion[]> {
    return this.http.get<HighlightedQuestion[]>(this.serviceUrl(operationId));
  }

  download(operationId: number, localizedQuestionId: number): Observable<Blob> {
    return this.http.get(`${this.serviceUrl(operationId)}/download/${localizedQuestionId}`, {
      headers: new HttpHeaders({
        'Accept': 'text/pdf'
      }),
      responseType: 'blob'
    });
  }

  addQuestion(operationId: number) {
    return this.http.put<HighlightedQuestion>(this.serviceUrl(operationId), null)
      .pipe(tap(() => this.operationManagementService.shouldUpdateStatus(false)));
  }

  deleteQuestion(operationId: number, questionId) {
    return this.http.delete(`${this.serviceUrl(operationId)}/${questionId}`)
      .pipe(tap(() => this.operationManagementService.shouldUpdateStatus(false)));
  }

  deleteLocalizedQuestion(operationId: number, questionId: number, localizedQuestionId: number) {
    return this.http.delete<HighlightedQuestion>(`${this.serviceUrl(operationId)}/${questionId}/${localizedQuestionId}`)
      .pipe(tap(() => this.operationManagementService.shouldUpdateStatus(false)));
  }

}
