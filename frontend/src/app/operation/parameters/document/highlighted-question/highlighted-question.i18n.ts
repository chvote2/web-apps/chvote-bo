/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_highlighted_question = {
  fr: {
    "delete-localized-question-dialog": {
      "message": "Voulez-vous vraiment supprimer la traduction en {{lang}} ?",
    },
    "delete-question-dialog": {
      "message": "Voulez-vous vraiment supprimer la question numéro {{index}} ainsi que toutes ses traductions ?",
    },
    grid: {
      header: {
        "localized-question": "Intitulé de la question",
        "language": "Langue",
        "file": "Fichier",
        "date": "Date d'import",
        "delete": "Supprimer"
      }
    },
    title: "Questions mises en avant sur le site",
    "question-number": "Question numéro ",
    actions: {
      "add-question": "Ajouter une question",
      "close-edition-mode": "Revenir à la liste"
    },
    form: {
      fields: {
        "localized-question": {
          "placeholder": "Intitulé de la question"
        },
        "language": {
          "placeholder": "Choix de la langue",
          "label-key": {
            "FR": "Français",
            "DE": "Allemand",
            "IT": "Italien",
            "RM": "Romanche",
          }
        },
        "input-file": {
          "placeholder": "Sélectionner un document",
        },
      }

    }
  },
  de: {
    "delete-localized-question-dialog": {
      "message": "DE - Voulez-vous vraiment supprimer la traduction en {{lang}} ?",
    },
    "delete-question-dialog": {
      "message": "DE - Voulez-vous vraiment supprimer la question numéro {{index}} ainsi que toutes ses traductions ?",
    },
    grid: {
      header: {
        "localized-question": "DE - Intitulé de la question",
        "language": "DE - Langue",
        "file": "DE - Fichier",
        "date": "DE - Date d'import",
        "delete": "DE - Supprimer"
      }
    },
    title: "DE - Questions mises en avant sur le site",
    "question-number": "DE - Question numéro ",
    actions: {
      "add-question": "DE - Ajouter une question",
      "close-edition-mode": "DE - Revenir à la liste"
    },
    form: {
      fields: {
        "localized-question": {
          "placeholder": "DE - Intitulé de la question"
        },
        "language": {
          "placeholder": "DE - Choix de la langue",
          "label-key": {
            "FR": "DE - Français",
            "DE": "DE - Allemand",
            "IT": "DE - Italien",
            "RM": "DE - Romanche",
          }
        },
        "input-file": {
          "placeholder": "DE - Sélectionner un document",
        },
      }

    }
  }
};
