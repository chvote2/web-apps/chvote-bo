/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { OperationParametersModel } from "../model/operation-parameters.model";
import { OperationFile } from "../../../core/model/operation-file";
import { Operation } from "../../model/operation";
import { DocumentService } from "./service/document.service";
import { Observable } from "rxjs/Observable";
import { DataSource } from "@angular/cdk/collections";
import { MatSnackBar, MatSort, MatSortable } from "@angular/material";
import { sortOnProperty } from "../../../core/util/table-utils";
import { TranslateService } from "@ngx-translate/core";
import { TdDialogService } from "@covalent/core";
import * as _ from "underscore";
import { FileService } from "app/core/service/file-service/file-service";
import { map } from "rxjs/operators";
import { merge } from "rxjs/observable/merge";
import { AuthorizationService } from "../../../core/service/http/authorization.service";
import { ConfirmBeforeQuit } from "../../../core/confirm-before-quit";
import { UploadInputComponent } from "../../../shared/upload-input/upload-input.component";
import { Subscription } from "rxjs/Subscription";
import { ReadOnlyService } from "../../service/read-only.service";
import { HighlightedQuestionListComponent } from "./highlighted-question/components/highlighted-question-list.component";
import { isFormModifiedAndNotSaved } from '../../../core/util/form-utils';

const ALL_TYPES = [
  {value: 'DOCUMENT_FAQ', labelKey: 'parameters.document.edit.form.fields.type.label-key.DOCUMENT_FAQ'},
  {value: 'DOCUMENT_TERMS', labelKey: 'parameters.document.edit.form.fields.type.label-key.DOCUMENT_TERMS'},
  {value: 'DOCUMENT_CERTIFICATE', labelKey: 'parameters.document.edit.form.fields.type.label-key.DOCUMENT_CERTIFICATE'}
];

const ALL_LANGUAGES = [
  {value: 'DE', labelKey: 'parameters.document.edit.form.fields.language.label-key.DE'},
  {value: 'FR', labelKey: 'parameters.document.edit.form.fields.language.label-key.FR'},
  {value: 'IT', labelKey: 'parameters.document.edit.form.fields.language.label-key.IT'},
  {value: 'RM', labelKey: 'parameters.document.edit.form.fields.language.label-key.RM'}
];

@Component({
  templateUrl: './document-edit.component.html',
  styleUrls: ['../parameter-edit.scss', './document-edit.component.scss']
})
export class DocumentEditComponent implements OnInit, ConfirmBeforeQuit {
  operation: Operation;
  documents: OperationFile[];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(UploadInputComponent) uploadInputComponent: UploadInputComponent;
  @ViewChild(HighlightedQuestionListComponent) questionListComponent: HighlightedQuestionListComponent;
  documentTypes = ALL_TYPES;
  languages = ALL_LANGUAGES;
  documentForm: FormGroup;
  documentsDataSource: DocumentsDataSource;
  private _subscriptions: Subscription[] = [];
  private _readOnly = false;

  constructor(private formBuilder: FormBuilder,
              private parametersModel: OperationParametersModel,
              private documentService: DocumentService,
              private dialogService: TdDialogService,
              private translateService: TranslateService,
              private fileService: FileService,
              private authorizationService: AuthorizationService,
              private readOnlyService: ReadOnlyService,
              private snackBar: MatSnackBar) {
    this.createForm();
    this.documentForm.reset({documentType: null, documentLanguage: null});
  }

  ngOnInit() {

    this._subscriptions.push(
      this.readOnlyService.isDocumentInReadOnly()
        .subscribe(
          readOnly => {
            this._readOnly = readOnly;
          }
        ),
      this.parametersModel.currentOperation.subscribe(operation => this.operation = operation),
      this.parametersModel.currentDocumentFiles.subscribe(documents => this.documents = documents));

    this.documentsDataSource = new DocumentsDataSource(this.parametersModel, this.sort, this.translateService);
    this.updateTypes();
    this.updateLanguages();

    //The first sort is done on ballotId column
    // To conform to the SFD, the second column to be sorted is language but matsort doesn't support this feature yet.
    // An issue is opened for this purpose
    // TODO : https://github.com/angular/material2/issues/7226
    this.sort.sort(<MatSortable>{
        id: "type",
        start: "asc",
      }
    );
  }


  get readOnly(): boolean {
    return this._readOnly;
  }


  get allDocumentsAreImported() {
    return this.documents && this.documents.length == ALL_TYPES.length * ALL_LANGUAGES.length;
  }

  get documentsDisplayedColumns() {
    let readableColumn = ['type', 'language', 'file'];
    if (!this.readOnly && this.authorizationService.hasAtLeastOneRole(['DELETE_OPERATION_DOCUMENTATION'])) {
      return readableColumn.concat(['delete']);
    }
    return readableColumn;
  }

  get uploadUrl(): string {
    return `${this.documentService.serviceUrl}/${this.operation.id}/upload/${this.documentForm.value.documentType}/${this.documentForm.value.documentLanguage}`;
  }

  isFormModified(): boolean {
    return this.questionListComponent.inEdition ||
           this.uploadInputComponent &&
           (this.uploadInputComponent.isFormModified() ||
           isFormModifiedAndNotSaved(this.documentForm, false));
  }



  ngOnDestroy(): void {
    this._subscriptions.forEach(s => s.unsubscribe());
  }

  checkBeforeUpload() {
    return () => {
      this.documentForm.controls.documentType.markAsTouched();
      this.documentForm.controls.documentLanguage.markAsTouched();
      this.documentForm.updateValueAndValidity();
      return this.documentForm.valid;
    }
  }

  processUploadResult(document: OperationFile) {
    if (document) {
      let existingDocument = _.find(this.documents, doc => {
        return doc.businessKey === document.businessKey;
      });
      if (existingDocument) {
        this.removeDocument(existingDocument.id);
      }
      this.documents.push(document);
      this.parametersModel.updateDocumentFiles(this.documents);
      // UI-feedback: notify user that import was successful
      this.snackBar.open(
        this.translateService.instant(
          'parameters.document.edit.form.import-success',
          {fileName: document.fileName}
        ),
        '',
        {
          duration: 5000
        }
      );
      this.documentForm.reset({documentType: null, documentLanguage: null});
      this.updateTypes();
      this.updateLanguages();
    }
  }

  download(e, document: OperationFile) {
    this.documentService.download(document.id).subscribe(blob => {
      this.fileService.downloadBlob(document.fileName, blob)
    });
    e.preventDefault();
  }

  deleteFile(document: OperationFile) {
    this.dialogService.openConfirm({
      message: this.translateService.instant('parameters.document.edit.delete-dialog.message',
        {fileName: document.fileName}),
      disableClose: true,
      cancelButton: this.translateService.instant('global.actions.no'),
      acceptButton: this.translateService.instant('global.actions.yes'),
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.documentService.deleteFile(document.id).subscribe(() => {
          this.snackBar.open(
            this.translateService.instant(
              'parameters.document.edit.delete-success', {fileName: document.fileName}),
            '',
            {duration: 5000}
          );
          this.removeDocument(document.id);
          this.parametersModel.updateDocumentFiles(this.documents);
          this.updateTypes();
          this.updateLanguages();
        });
      }
    });
  }

  private updateTypes() {
    let documentLanguage = this.documentForm.controls.documentLanguage.value;
    if (documentLanguage) {
      let documentsByTypes = this.documents.filter(document => document.language == documentLanguage)
        .map(document => document.type);
      this.documentTypes = ALL_TYPES.filter(type => documentsByTypes.indexOf(type.value) == -1);
    } else {
      this.documentTypes = ALL_TYPES;
    }
  }

  private updateLanguages() {
    let documentType = this.documentForm.controls.documentType.value;
    if (documentType) {
      let documentsByLanguages = this.documents.filter(document => document.type == documentType)
        .map(document => document.language);
      this.languages = ALL_LANGUAGES.filter(language => documentsByLanguages.indexOf(language.value) == -1);
    } else {
      this.languages = ALL_LANGUAGES;
    }
  }

  private removeDocument(documentId: number) {
    this.documents = _.reject(this.documents, function (document: OperationFile) {
      return document.id === documentId;
    });
  }

  private createForm() {
    this.documentForm = this.formBuilder.group({
      documentType: [
        {value: null},
        Validators.required
      ],
      documentLanguage: [
        {value: null},
        Validators.required
      ]
    })
  }
}

class DocumentsDataSource extends DataSource<OperationFile> {

  private documents: OperationFile[];

  constructor(private parametersModel: OperationParametersModel,
              private sort: MatSort,
              private translateService: TranslateService) {
    super();
    this.parametersModel.currentDocumentFiles.subscribe(files => this.documents = files);
  }

  connect(): Observable<OperationFile[]> {
    return merge(this.parametersModel.currentDocumentFiles, this.sort.sortChange,
      this.translateService.onLangChange).pipe(
      map(() => this.translate(this.documents)),
      map((documents) => sortOnProperty(documents, this.sort.active, this.sort.direction))
    )
  }

  disconnect() {
  }

  private translate(documents: OperationFile[]) {
    return documents.map(document => Object.assign(
      {},
      document,
      {
        language: this.translateService.instant(
          `parameters.document.edit.form.fields.language.label-key.${document.language}`),
        type: this.translateService.instant(
          `parameters.document.edit.form.fields.type.label-key.${document.type}`)
      }));
  }
}
