/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_voting_material_printer = {
  fr: {
    "card": {
      "title": "Configuration imprimeur",
      "subtitle": "Modèle de configuration imprimeur",
      "printer-template-title": "Modèle sélectionné"
    },
    "edit": {
      "title": "Définir le modèle de configuration imprimeur",
      "printer": "Imprimeur",
      "form": {
        "fields": {
          "printer-template": {
            "placeholder": "Sélectionner un modèle"
          }
        }
      },
      "printer-municipality-mapping": {
        "title": "Associations des communes aux imprimeurs",
        "municipality-count": "Nombre de communes : "
      },
      "printer-swiss-abroad-mapping": {
        "title": "Imprimeur des électeurs Suisse de l'Étranger (SE)",
        "info": "Cas des SE rattachés au canton et n'ayant pas de commune politique"
      },
      "printer-testing-card-mapping": {
        "title": "Imprimeur des cartes de test et des cartes de contrôleur",
        "caption": "Tous les imprimeurs du modèle recevront des cartes de test imprimeur"
      }
    }
  },
  de: {
    "card": {
      "title": "DE - Configuration imprimeur",
      "subtitle": "DE - Modèle de configuration imprimeur",
      "printer-template-title": "DE - Modèle sélectionné"
    },
    "edit": {
      "title": "DE - Définir le modèle de configuration imprimeur",
      "printer": "DE - Imprimeur",
      "form": {
        "fields": {
          "printer-template": {
            "placeholder": "DE - Sélectionner un modèle"
          }
        }
      },
      "printer-municipality-mapping": {
        "title": "DE - Associations des communes aux imprimeurs",
        "municipality-count": "DE - Nombre de communes : "
      },
      "printer-swiss-abroad-mapping": {
        "title": "DE - Imprimeur des électeurs Suisse de l'Étranger (SE)",
        "info": "DE - Cas des SE rattachés au canton et n'ayant pas de commune politique"
      },
      "printer-testing-card-mapping": {
        "title": "DE - Imprimeur des cartes de test"
      }
    }
  },


};
