/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VotingMaterialComponent } from './voting-material.component';
import { TestingCardsLotResolver } from '../testing-cards-lot/service/testing-cards-lot.resolver';
import { AuthorizationGuard } from '../../core/service/http/authorization.service';
import { ConfirmBeforeQuitGuard } from '../../core/confirm-before-quit';

const routes: Routes = [
  {
    path: 'register/upload',
    component: VotingMaterialComponent,
    data: {
      mode: "upload-register"
    },
    canActivate: [AuthorizationGuard],
    canActivateChild: [AuthorizationGuard],
    canDeactivate: [ConfirmBeforeQuitGuard]
  },
  {
    path: 'register',
    component: VotingMaterialComponent,
    data: {
      mode: "list-register"
    },
    canActivate: [AuthorizationGuard],
    canActivateChild: [AuthorizationGuard]
  },
  {
    path: 'voting-card-title',
    component: VotingMaterialComponent,
    data: {
      mode: "voting-card-title"
    },
    canActivate: [AuthorizationGuard],
    canActivateChild: [AuthorizationGuard],
    canDeactivate: [ConfirmBeforeQuitGuard]
  },
  {
    path: 'printer',
    component: VotingMaterialComponent,
    data: {
      mode: "printer"
    },
    canActivate: [AuthorizationGuard],
    canActivateChild: [AuthorizationGuard],
    canDeactivate: [ConfirmBeforeQuitGuard]
  },

  {
    path: 'testing-card-lot/create',
    component: VotingMaterialComponent,
    data: {
      forConfiguration: false,
      mode: "testing-card-edit-or-create"
    },
    canActivate: [AuthorizationGuard],
    canActivateChild: [AuthorizationGuard],
    canDeactivate: [ConfirmBeforeQuitGuard]
  },
  {
    path: 'testing-card-lot/:voterTestingCardsLotId',
    component: VotingMaterialComponent,
    resolve: {
      voterTestingCardsLot: TestingCardsLotResolver,
    },
    data: {
      forConfiguration: false,
      mode: "testing-card-edit-or-create"
    },
    canActivate: [AuthorizationGuard],
    canActivateChild: [AuthorizationGuard],
    canDeactivate: [ConfirmBeforeQuitGuard]
  },
  {
    path: 'testing-card-lot',
    component: VotingMaterialComponent,
    data: {
      forConfiguration: false,
      mode: "testing-card-list"
    },
    canActivate: [AuthorizationGuard],
    canActivateChild: [AuthorizationGuard]
  },
  {
    path: '',
    component: VotingMaterialComponent,
    data: {
      mode: "view",
    },
    canActivate: [AuthorizationGuard],
    canActivateChild: [AuthorizationGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VotingMaterialRoutingModule {
}

