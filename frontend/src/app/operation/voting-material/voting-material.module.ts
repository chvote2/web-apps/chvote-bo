/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { RegisterModule } from './register/register.module';
import { PrinterTemplateService } from './printer/services/printer-template.service';
import { CardTitleCardComponent } from './card-title/card-title-card/card-title-card.component';
import { CardTitleEditComponent } from './card-title/card-title-edit/card-title-edit.component';
import { VotingMaterialRoutingModule } from './voting-material-routing.module';
import { VotingMaterialComponent } from './voting-material.component';
import { TestingCardsLotModule } from '../testing-cards-lot/testing-cards-lot.module';
import { PrinterCardComponent } from './printer/printer-card/printer-card.component';
import { PrinterEditComponent } from './printer/printer-edit/printer-edit.component';
import { PrinterViewComponent } from './printer/printer-view/printer-view.component';
import { VotingMaterialNotificationComponent } from './voting-material-notification/voting-material-notification.component';

@NgModule({
  imports: [
    SharedModule,
    RegisterModule,
    VotingMaterialRoutingModule,
    TestingCardsLotModule
  ],
  declarations: [
    VotingMaterialComponent,
    VotingMaterialNotificationComponent,
    CardTitleCardComponent,
    CardTitleEditComponent,
    PrinterCardComponent,
    PrinterEditComponent,
    PrinterViewComponent,
  ],
  providers: [
    PrinterTemplateService
  ]
})
export class VotingMaterialModule {
}

