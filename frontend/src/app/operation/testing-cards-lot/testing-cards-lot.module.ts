/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { NgModule } from '@angular/core';
import { CovalentDialogsModule } from '@covalent/core';
import { SharedModule } from '../../shared/shared.module';
import { TestingCardsLotSideBarItemComponent } from './side-bar-item/side-bar-item.component';
import { RouterModule } from '@angular/router';
import { OperationDataService } from '../service/operation.data.service';
import { TestingCardsLotListComponent } from './testing-cards-lot-list/testing-cards-lot-list.component';
import { TestingCardsLotService } from './service/testing-cards-lot.service';
import { TestingCardsLotResolver } from './service/testing-cards-lot.resolver';
import { TestingCardsLotDetailsComponent } from './testing-cards-lot-details/testing-cards-lot-details.component';
import { TestingCardsLotCardComponent } from './card/card.component';


@NgModule({
  imports: [
    SharedModule,
    RouterModule,
    CovalentDialogsModule,
  ],
  declarations: [
    TestingCardsLotDetailsComponent,
    TestingCardsLotListComponent,
    TestingCardsLotCardComponent,
    TestingCardsLotSideBarItemComponent,
  ],
  providers: [
    OperationDataService,
    TestingCardsLotService,
    TestingCardsLotResolver,
  ],
  exports: [
    TestingCardsLotCardComponent,
    TestingCardsLotDetailsComponent,
    TestingCardsLotListComponent,
    TestingCardsLotSideBarItemComponent
  ]
})
export class TestingCardsLotModule {
}
