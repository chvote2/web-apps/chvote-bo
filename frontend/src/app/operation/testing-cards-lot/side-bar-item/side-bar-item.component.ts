/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Operation } from '../../model/operation';
import { OperationManagementService } from '../../service/operation-managment.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'testing-cards-lot-side-bar-item',
  templateUrl: './side-bar-item.component.html'
})
export class TestingCardsLotSideBarItemComponent implements OnInit, OnDestroy {

  @Input() operation: Operation;
  @Input() inConfiguration;
  @Input() routerLink: string[];
  @Input() locked: boolean;
  completed = false;
  inError = false;
  private _subscriptions: Subscription[] = [];

  constructor(private operationManagementService: OperationManagementService) {
  }

  get i18nSubSection() {
    return this.inConfiguration ? "for-configuration" : "for-voting-material";
  }

  ngOnInit(): void {
    this._subscriptions.push(
      this.operationManagementService.status.subscribe(status => {
        this.completed = (this.inConfiguration ? status.configurationStatus :
          status.votingMaterialStatus).completedSections["testing-card-lot"];
        this.inError = (this.inConfiguration ? status.configurationStatus :
          status.votingMaterialStatus).sectionsInError["testing-card-lot"];
      })
    )
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach(s => s.unsubscribe());
  }
}
