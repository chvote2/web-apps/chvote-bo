/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { NgModule } from "@angular/core";
import { SharedModule } from '../shared/shared.module';
import { OperationStatusService } from './service/operation.status.service';
import { CovalentStepsModule } from '@covalent/core';
import { OperationResolver } from './service/operation-resolver';
import { MatTabsModule } from '@angular/material';
import { OperationManagementService } from './service/operation-managment.service';
import { GeneratedVotingMaterialService } from './service/generated-voting-material.service';
import { OperationManagementRoutingModule } from './operation-management-routing.module';
import { OperationManagementComponent } from './operation-management.component';
import { DeploymentSummaryComponent } from './deployment-summary/deployment-summary.component';
import { VotingMaterialModule } from './voting-material/voting-material.module';
import { TestSiteCardsDownloadButtonComponent } from './deployment-summary/test-site-cards-download-button.component';
import { ReadOnlyService } from './service/read-only.service';
import { VotingMaterialCreationProgressComponent } from './deployment-summary/voting-material-creation-progress/voting-material-creation-progress.component';
import { VotingPeriodModule } from './voting-period/voting-period.module';
import { ProductionSummaryComponent } from './deployment-summary/production-summary/production-summary.component';
import { TestSummaryComponent } from './deployment-summary/test-summary/test-summary.component';
import { TallyComponent } from './tally/tally.component';

@NgModule({
  imports: [
    SharedModule,
    OperationManagementRoutingModule,
    CovalentStepsModule,
    MatTabsModule,
    VotingMaterialModule,
    VotingPeriodModule
  ],
  declarations: [
    OperationManagementComponent,
    DeploymentSummaryComponent,
    TestSiteCardsDownloadButtonComponent,
    VotingMaterialCreationProgressComponent,
    ProductionSummaryComponent,
    TestSummaryComponent,
    TallyComponent,
  ],
  providers: [
    OperationStatusService,
    OperationManagementService,
    ReadOnlyService,
    OperationResolver,
    GeneratedVotingMaterialService
  ]
})
export class OperationManagementModule {
}
