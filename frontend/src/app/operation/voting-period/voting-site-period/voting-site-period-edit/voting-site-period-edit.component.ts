/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Operation } from '../../../model/operation';
import { VotingSitePeriod } from '../voting-site-period';
import { Subject } from 'rxjs/Subject';
import { VotingSitePeriodService } from '../voting-site-period.service';
import { ActivatedRoute, Router } from '@angular/router';
import { OperationManagementService } from '../../../service/operation-managment.service';
import { enableFormComponents, focusOnError, isFormModifiedAndNotSaved } from '../../../../core/util/form-utils';
import { ConfirmBeforeQuit } from '../../../../core/confirm-before-quit';
import { convertMomentToDate } from '../../../../core/util/utils';

@Component({
  selector: 'voting-site-period-edit',
  templateUrl: './voting-site-period-edit.component.html',
  styleUrls: ['./voting-site-period-edit.component.scss']
})
export class VotingSitePeriodEditComponent implements OnInit, ConfirmBeforeQuit {
  now = new Date();
  form: FormGroup;
  @Input()
  readOnly: boolean;
  @Input()
  operation: Operation;
  @Input()
  period: VotingSitePeriod;
  @Output()
  periodChange = new Subject<VotingSitePeriod>();
  private saved = false;

  constructor(private service: VotingSitePeriodService,
              formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private operationManagementService: OperationManagementService) {
    this.form = formBuilder.group({
      dateOpen: [null, Validators.required],
      dateClose: [null, Validators.required],
      gracePeriod: [null, [Validators.required, Validators.min(0), Validators.max(60)]]
    });
  }


  isFormModified(): boolean {
    return isFormModifiedAndNotSaved(this.form, this.saved);
  }

  ngOnInit() {
  }

  ngOnChanges(): void {
    this.form.reset({
      dateOpen: this.period ? this.period.dateOpen : new Date(),
      dateClose: this.period ? this.period.dateClose : null,
      gracePeriod: this.period ? this.period.gracePeriod : null,
    });
    this.updateForReadonly();
  }

  save() {
    this.form.controls.dateOpen.markAsTouched();
    this.form.controls.dateClose.markAsTouched();
    this.form.controls.gracePeriod.markAsTouched();
    this.form.updateValueAndValidity();

    if (this.form.valid) {
      let period = this.form.getRawValue();
      convertMomentToDate(period, "dateOpen");
      convertMomentToDate(period, "dateClose");

      this.service.saveOrUpdate(this.operation.id, period)
        .subscribe(() => {
          this.periodChange.next(period);
          this.operationManagementService.shouldUpdateStatus();
          this.saved = true;
          this.router.navigate(['..'], {relativeTo: this.route})
        });
    } else {
      focusOnError(this.form);
    }
  }

  private updateForReadonly() {
    enableFormComponents(this.form.controls, !this.readOnly);
  }


}
