/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ElectoralAuthorityKeyCardComponent } from './electoral-authority-key/card/card.component';
import { ElectoralAuthorityKeyListComponent } from './electoral-authority-key/list/list.component';
import { ElectoralAuthorityKeyService } from './electoral-authority-key/electoral-authority-key.service';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { SharedModule } from '../shared/shared.module';
import { AdminCardsViewComponent } from './cards-view/cards-view.component';
import { AdminEditViewComponent } from './edit-view/edit-view.component';
import { EditLabelComponent } from './electoral-authority-key/edit-label/edit-label.component';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    AdminRoutingModule
  ],
  declarations: [
    ElectoralAuthorityKeyCardComponent,
    ElectoralAuthorityKeyListComponent,
    AdminComponent,
    AdminCardsViewComponent,
    AdminEditViewComponent,
    EditLabelComponent],
  providers: [ElectoralAuthorityKeyService],
  entryComponents: [EditLabelComponent],
})
export class AdminModule {
}
