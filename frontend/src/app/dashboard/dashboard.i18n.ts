/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { i18n_operation_list } from './operation-list/operation-list.i18n';

export const i18n_dashboard = {
  fr: {
    "sidenav": "Tableau de bord",
    "header": {
      "title": "Tableau de bord des opérations de vote",
      "actions": {
        "create-operation": "Créer une opération"
      }
    },
    "operation-list": i18n_operation_list.fr
  },
  de: {
    "sidenav": "DE - Tableau de bord",
    "header": {
      "title": "DE - Tableau de bord des opérations de vote",
      "actions": {
        "create-operation": "DE - Créer une opération"
      }
    },
    "operation-list": i18n_operation_list.de
  }
};
