/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'completable-card',
  templateUrl: './completable-card.component.html',
  styleUrls: ['./completable-card.component.scss']
})
export class CompletableCardComponent {

  @Input() required: boolean;
  @Input() title: string;
  @Input() subtitle: string;
  @Input() partiallyCompleted: boolean = false;
  @Input() completed: boolean;
  @Input() editSection: string[];
  @Input() readOnly = false;
  @Input() inError = false;

  constructor(private router: Router,
              private route: ActivatedRoute) {
  }


  get shouldDisplayContent() {
    return this.completed || this.partiallyCompleted
  }

  edit() {
    this.router.navigate(this.editSection, {relativeTo: this.route});
  }

}
