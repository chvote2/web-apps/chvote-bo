/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { INCOMPLETE, NOT_REQUESTED, OperationStatus } from '../../operation/model/operation-status';

export function operationStatus(): OperationStatus {
  return {
    configurationStatus: {
      state: INCOMPLETE,
      readOnlySections: [],
      completedSections: {},
      sectionsInError: {},
      modificationMode: 'FULLY_MODIFIABLE',
      allSectionsInReadOnly: false
    },
    votingMaterialStatus: null,
    votingPeriodStatus: {
      state: INCOMPLETE,
      readOnly: false,
      completedSections: {}
    },
    tallyArchiveStatus: {
      state: NOT_REQUESTED
    },
    deploymentTarget: "NOT_DEFINED",
    inTestStatusMessage: {
      message: "configStatusMessageKey",
      parameters: []
    }
    ,
    inProductionStatusMessage: {
      message: "vmStatusMessageKey",
      parameters: []
    },
    consistencyComputationInProgress: false,
    configurationConsistencyErrors: [],
    votingMaterialConsistencyErrors: [],
    configurationCompleteAndConsistent: true,
    votingMaterialCompleteAndConsistent: true,
  }

}
