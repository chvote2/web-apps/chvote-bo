/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import {
  ApplicationRef, Component, ComponentFactoryResolver, ComponentRef, EmbeddedViewRef, Injectable, Injector
} from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { finalize } from 'rxjs/operators';


export const BACKGROUND_PROCESSING = "background-processing";

@Injectable()
export class InProgressInterceptor implements HttpInterceptor {
  private nbCalls = 0;
  private componentRef: ComponentRef<WaitingCover>;


  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private appRef: ApplicationRef,
              private injector: Injector) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.headers.has(BACKGROUND_PROCESSING)) {
      req.headers.delete(BACKGROUND_PROCESSING);
      return next.handle(req);
    }
    if (this.nbCalls++ == 0) {
      this.showCover();
    }
    return next.handle(req).pipe(
      finalize(() => {
        if (--this.nbCalls == 0) {
          this.hideCover();
        }
      })
    );
  }

  protected showCover() {

    if (!localStorage.getItem("_e2e")) {
      setTimeout(() => {
          this.componentRef = this.componentFactoryResolver
            .resolveComponentFactory(WaitingCover)
            .create(this.injector);
          this.appRef.attachView(this.componentRef.hostView);
          const domElem = (this.componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
          document.body.appendChild(domElem);
        }
      );
    }
  }


  protected hideCover() {
    if (!localStorage.getItem("_e2e")) {
      setTimeout(() => {
        this.appRef.detachView(this.componentRef.hostView);
        this.componentRef.destroy();
      });
    }
  }

}


@Component({
  template: `
    <div id='cover'>
      <mat-spinner></mat-spinner>
    </div>
  `,
  styles: [`
    @keyframes fadeIn {
      0% {
        opacity: 0;
      }
      30% {
        opacity: 0;
      }
      100% {
        opacity: 1;
      }
    }

    #cover {
      position: fixed;
      top: 0;
      left: 0;
      background: rgba(0, 0, 0, 0.6);
      z-index: 100;
      width: 100%;
      height: 100%;
      transition: all;
      animation: fadeIn 1s;
    }

    mat-spinner {
      margin-top: calc(50vh - 65px);
      margin-left: auto;
      margin-right: auto;

    }
  `]
})
export class WaitingCover {


}




